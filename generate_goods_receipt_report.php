<?php
require_once __DIR__ . '/vendor/autoload.php';
include("include/config.php");
require_once 'func/Receipts.php';
require_once 'func/Product.php';

// Get the goods receipt ID from the URL parameter
$goodsReceiptId = $_GET['id'];

// Create instances of Receipts and Product classes
$receiptsObj = new Receipts($con);
$productObj = new Product($con);

// Fetch the goods receipt details from the database
$receiptDetails = $receiptsObj->getReceiptDetails($goodsReceiptId);

// Fetch the data store details from the database
$dataStoreQuery = "SELECT * FROM data_store LIMIT 1";
$dataStoreResult = mysqli_query($con, $dataStoreQuery);
$dataStoreRow = mysqli_fetch_assoc($dataStoreResult);

// Create a new mPDF instance
$mpdf = new \Mpdf\Mpdf();

// Generate the HTML content for the report
$html = '
   <html>
       <head>
           <style>
               body { font-family: DejaVu Sans, sans-serif; }
               table { border-collapse: collapse; width: 100%; }
               th, td { border: 1px solid black; padding: 8px; text-align: left; }
           </style>
       </head>
       <body>
           <h1>Накладна №' . $receiptDetails['number'] . ' від ' . $receiptDetails['date'] . '</h1>
           <p><strong>Постачальник:</strong> ' . $receiptDetails['supplier_name'] . '</p>
           <p><strong>Покупець:</strong></p>
           <ul>
               <li>Назва: ' . $dataStoreRow['full_name'] . '</li>
               <li>Адреса: ' . $dataStoreRow['legal_address'] . '</li>
               <li>IBAN: ' . $dataStoreRow['iban'] . '</li>
               <li>Банк: ' . $dataStoreRow['bank'] . '</li>
           </ul>
           <p><strong>Склад:</strong> ' . $receiptDetails['warehouse_address'] . '</p>
           
           <table>
               <thead>
                   <tr>
                       <th>№</th>
                       <th>Товар</th>
                       <th>Кількість</th>
                       <th>Одиниці виміру</th>
                       <th>Ціна з ПДВ</th>
                       <th>Сума з ПДВ</th>
                   </tr>
               </thead>
               <tbody>';

// Parse the JSON products data
$products = json_decode($receiptDetails['products'], true);

$totalQuantity = 0;
$itemCount = 1;
foreach ($products as $product) {
    $productId = $product['id'];
    $price = $product['price'];
    $quantity = $product['quantity'];

    // Fetch the product details from the database
    $productData = $productObj->getProductById($productId);
    $productName = $productData['productName'];
    $unitName = $productObj->getUnitName($productData['units_id']);

    $totalPrice = $price * $quantity;

    $html .= '
       <tr>
           <td>' . $itemCount . '</td>
           <td>' . $productName . '</td>
           <td>' . $quantity . '</td>
           <td>' . $unitName . '</td>
           <td>' . $price . '</td>
           <td>' . $totalPrice . '</td>
       </tr>';

    $totalQuantity += $quantity;
    $itemCount++;
}

$html .= '
               </tbody>
           </table>
           
           <p><strong>Всього позицій:</strong> ' . count($products) . ' на суму ' . $receiptDetails['sum'] . ' грн.</p>
           <p><strong>У т.ч. ПДВ:</strong> нуль гривень 00 копійок</p>
           <p>Від постачальника _________________________________________</p>
           <p>Від покупця _________________________________________</p>
       </body>
   </html>';

// Write the HTML content to the PDF
$mpdf->WriteHTML($html);

// Output the PDF
$mpdf->Output('goods_receipt_report.pdf', 'I');
?>