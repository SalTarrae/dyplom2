<?php
session_start();
require_once 'func/login.php';
include("include/config.php");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$user = new User($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}

// Get user information
$userId = $_SESSION['user_id'];
$userInfo = $user->getUserInfo($userId);

if (isset($_POST['updateProfile'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];

    if ($user->updateProfile($userId, $name, $email)) {
        $_SESSION['success_msg'] = "Profile updated successfully.";
        header("Location: user.php");
        exit();
    } else {
        $_SESSION['error_msg'] = "Error updating profile.";
    }
}

if (isset($_POST['changePassword'])) {
    $email = $userInfo['email'];
    $token = bin2hex(random_bytes(32));

    if ($user->storeResetToken($email, $token)) {
        $resetLink = "http://" . $_SERVER['HTTP_HOST'] . "/change-password.php?token=$token";

        $to = 'lizaeta01@gmail.com';
        $subject = "Password Reset Request";
        $message = "Hi,\n\nYou recently requested to reset your password. Click the link below to reset your password:\n\n$resetLink\n\nIf you did not make this request, you can safely ignore this email.\n\nBest regards,\nYour Application Team";
        $headers = "From: elizavetadanylko@gmail.com";
        $result2 = mail($to, $subject, $message, $headers);

        if ($result2) {
            $_SESSION['success_msg'] = "A password reset link has been sent to your email.";
        } else {
            $_SESSION['error_msg'] = "Failed to send password reset email.";
        }
    } else {
        $_SESSION['error_msg'] = "Failed to store password reset token.";
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Профіль</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module-head">
                <h3>User Profile</h3>
            </div>

            <?php if (isset($_SESSION['success_msg'])) { ?>
                <div class="alert alert-success"><?php echo $_SESSION['success_msg']; ?></div>
                <?php unset($_SESSION['success_msg']); ?>
            <?php } ?>

            <?php if (isset($_SESSION['error_msg'])) { ?>
                <div class="alert alert-danger"><?php echo $_SESSION['error_msg']; ?></div>
                <?php unset($_SESSION['error_msg']); ?>
            <?php } ?>

            <form method="post">
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $userInfo['name']; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" name="email" value="<?php echo $userInfo['email']; ?>">
                </div>
                <div class="form-group">
                    <label for="role">Role:</label>
                    <input type="text" class="form-control" id="role" name="role" value="<?php echo $userInfo['role_name']; ?>" disabled>
                </div>
                <button type="submit" class="btn btn-primary" name="updateProfile">Update Profile</button>
                <button type="submit" class="btn btn-danger" name="changePassword">Change Password</button>
            </form>
        </div>
    </div>
</div>
</body>
</html>