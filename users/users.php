<?php
session_start();
require_once 'func/login.php';
include("include/config.php");

$user = new User($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
} else {
    $currentUserId = $_SESSION['user_id'];

    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    if (isset($_GET['del'])) {
        $userId = $_GET['id'];
        if ($user->deleteUser($userId)) {
            $_SESSION['delmsg'] = "User deleted successfully";
        } else {
            $_SESSION['delmsg'] = "Failed to delete user";
        }
    }

    if (isset($_POST['updateRole'])) {
        $userId = $_POST['userId'];
        $newRole = $_POST['role'];
        if ($user->updateUserRole($userId, $newRole)) {
            $_SESSION['updatemsg'] = "User role updated successfully";
        } else {
            $_SESSION['updatemsg'] = "Failed to update user role";
        }
    }

    if (isset($_POST['addUser'])) {
        $email = $_POST['email'];
        $role = $_POST['role'];

        // Check if a user with the same email already exists
        if ($user->checkExistingUser($email)) {
            $_SESSION['errmsg'] = "Користувач з такою ж адресою вже існує.";
        } else {
            // Generate a unique registration token
            $token = bin2hex(random_bytes(16));

            // Store the user details and token in the database
            if ($user->addUser($email, $role, $token)) {
                // Send registration email to the user
                $registrationLink = "http://dyplom3/register.php?token=$token";
                $subject = "Registration Invitation";
                $message = "You have been invited to register. Click the link below to complete your registration:\n\n$registrationLink";
                $headers = "From: noreply@example.com";

                if (mail($email, $subject, $message, $headers)) {
                    $_SESSION['successmsg'] = "Користувача успішно додано та надіслано реєстраційний email.";
                } else {
                    $_SESSION['errmsg'] = "Failed to send registration email.";
                }
            } else {
                $_SESSION['errmsg'] = "Failed to add user.";
            }
        }
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Користувачі</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module-head">
                    <h3>Користувачі</h3>
                </div>
                <div class="module-body table">
                    <?php if (isset($_SESSION['delmsg'])) { ?>
                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <?php echo htmlentities($_SESSION['delmsg']); ?>
                            <?php unset($_SESSION['delmsg']); ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($_SESSION['updatemsg'])) { ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <?php echo htmlentities($_SESSION['updatemsg']); ?>
                            <?php unset($_SESSION['updatemsg']); ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($_SESSION['successmsg'])) { ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <?php echo htmlentities($_SESSION['successmsg']); ?>
                            <?php unset($_SESSION['successmsg']); ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($_SESSION['errmsg'])) { ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <?php echo htmlentities($_SESSION['errmsg']); ?>
                            <?php unset($_SESSION['errmsg']); ?>
                        </div>
                    <?php } ?>
                    <br/>
                    <div class="control-group">
                        <div class="controls">
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUserModal">Додати користувача</button>
                        </div>
                    </div>
                    <br/>
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Ім'я</th>
                            <th>Email</th>
                            <th>Роль</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $userQuery = $user->getAllUsers($currentUserId);
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($userQuery)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['name']); ?></td>
                                <td><?php echo htmlentities($row['email']); ?></td>
                                <td><?php echo htmlentities($row['role_name']); ?></td>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#editModal<?php echo $row['id']; ?>"><i class="icon-edit"></i></a>
                                    <a href="users.php?id=<?php echo $row['id'] ?>&del=delete" onClick="return confirm('Are you sure you want to delete?')"><i class="icon-remove-sign"></i></a>
                                </td>
                            </tr>
                            <?php $cnt = $cnt + 1;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Add User Modal -->
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="addUserModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addUserModalLabel">Add User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post">
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" class="form-control" id="email" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="role">Role</label>
                            <select class="form-control" id="role" name="role">
                                <?php
                                $roleQuery = $user->getAllRoles();
                                while ($roleRow = mysqli_fetch_array($roleQuery)) {
                                    echo "<option value='" . $roleRow['id'] . "'>" . $roleRow['name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary" name="addUser">Add User</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Edit User Modal -->
    <?php
    $userQuery = $user->getAllUsers($currentUserId);
    while ($row = mysqli_fetch_array($userQuery)) {
        ?>
        <div class="modal fade" id="editModal<?php echo $row['id']; ?>" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">Edit User Role</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="post">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" id="name" value="<?php echo $row['name']; ?>" disabled>
                            </div>
                            <div class="form-group">
                                <label for="role">Role</label>
                                <select class="form-control" id="role" name="role">
                                    <?php
                                    $roleQuery = $user->getAllRoles();
                                    while ($roleRow = mysqli_fetch_array($roleQuery)) {
                                        $selected = ($roleRow['id'] == $row['role']) ? 'selected' : '';
                                        echo "<option value='" . $roleRow['id'] . "' " . $selected . ">" . $roleRow['name'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <input type="hidden" name="userId" value="<?php echo $row['id']; ?>">
                            <button type="submit" class="btn btn-primary" name="updateRole">Update Role</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>

    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        });
    </script>
    </body>
    </html>
<?php } ?>