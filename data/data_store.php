<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
}
else {

date_default_timezone_set('Europe/Kiev');
$currentTime = date( 'd-m-Y h:i:s A', time () );

if(isset($_POST['submit'])) {

    $name = $_POST['name'];
    $persona = $_POST['persona'];
    $bank = $_POST['bank'];
    $iban = $_POST['iban'];
    $mobile = $_POST['mobile'];
    $email = $_POST['email'];
    $country = $_POST['country'];
    $registerСode = $_POST['registerСode'];
    $payerCode = $_POST['payerCode'];
    $legalAddress = $_POST['legalAddress'];
    $postalAddress = $_POST['postalAddress'];
    $locationAddress = $_POST['locationAddress'];


    $sql_legal = "UPDATE data_store
                  SET full_name = '$name',
                      mobile_phone = '$mobile',
                      email = '$email',
                      country = '$country',
                      register_code = '$registerСode',
                      payer_code = '$payerCode',
                      legal_address = '$legalAddress',
                      postal_address = '$postalAddress',
                      location_address = '$locationAddress',
                      iban = '$iban',
                      bank = '$bank',
                      persona = '$persona'
                  WHERE data_store.id = 1"; // змінити умову WHERE за необхідності

    if (mysqli_query($con, $sql_legal)) {
        if (mysqli_affected_rows($con) > 0) {
            $_SESSION['msg'] = "Дані успішно оновлені!";
        } else {
            $_SESSION['msg'] = "Нічого не оновлено.";
        }
    } else {
        $_SESSION['msg'] = "Помилка оновлення: " . mysqli_error($con);
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Товари</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">

                <?php if(isset($_POST['submit']))
                {?>
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Все добре!</strong>	<?php echo htmlentities($_SESSION['msg']);?><?php echo htmlentities($_SESSION['msg']="");?>
                    </div>
                <?php } ?>


            <br />
    <?php $query=mysqli_query($con,"select * from data_store where id=1");
    $cnt=1;
    while($row=mysqli_fetch_array($query))
    {
        ?>
        <div id="legalFields" >
                <form id="legalForm" class="form-horizontal row-fluid" name="counterparties" method="post" enctype="multipart/form-data">
                    <div class="module">
                        <div class="module-head">
                            <h3>Загальні</h3>
                        </div>
                        <div class="module-body">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Повна назва</label>
                                <div class="controls">
                                    <input type="text"  value="<?php echo htmlentities($row['full_name']);?>"  name="name"  placeholder="Напишіть найменування" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Телефон</label>
                                <div class="controls">
                                    <input type="text"    name="mobile" value="<?php echo htmlentities($row['mobile_phone']);?>"  placeholder="Напишіть моб. телефон" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Email</label>
                                <div class="controls">
                                    <input type="text"    name="email" value="<?php echo htmlentities($row['email']);?>"  placeholder="Напишіть email" class="span8 tip" required>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="module">
                        <div class="module-head">
                            <h3>Юридична особа</h3>
                        </div>
                        <div class="module-body">

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Відповідальна особа</label>
                                <div class="controls">
                                    <input type="text"    name="persona" value="<?php echo htmlentities($row['persona']);?>"   placeholder="Напишіть банк" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Країна</label>
                                <div class="controls">
                                    <input type="text"    name="country" value="<?php echo htmlentities($row['country']);?>"   placeholder="Напишіть країну" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Код ЄРДПОУ</label>
                                <div class="controls">
                                    <input type="text"  name="registerСode" value="<?php echo htmlentities($row['register_code']);?>"   placeholder="Напишіть код" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Код платника ПДВ</label>
                                <div class="controls">
                                    <input type="text"    name="payerCode"  value="<?php echo htmlentities($row['payer_code']);?>"  placeholder="Напишіть код УЗНР" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="basicinput">Юридична адреса</label>
                                <div class="controls">
                                    <input type="text" placeholder="Напишіть адресу" value="<?php echo htmlentities($row['legal_address']);?>"  name="legalAddress" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Поштова адреса</label>
                                <div class="controls">
                                    <input type="text"    name="postalAddress" value="<?php echo htmlentities($row['postal_address']);?>"   placeholder="Напишіть адресу" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Адреса місцезнаходження</label>
                                <div class="controls">
                                    <input type="text"    name="locationAddress"  value="<?php echo htmlentities($row['location_address']);?>"  placeholder="Напишіть адресу" class="span8 tip" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="module">
                        <div class="module-head">
                            <h3>Банківські рахунки</h3>
                        </div>
                        <div class="module-body">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">IBAN</label>
                                <div class="controls">
                                    <input type="text" value="<?php echo htmlentities($row['iban']);?>"    name="iban"  placeholder="Напишіть iban" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Банк</label>
                                <div class="controls">
                                    <input type="text"    name="bank"  value="<?php echo htmlentities($row['bank']);?>"  placeholder="Напишіть банк" class="span8 tip" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" name="submit" class="btn btn-dark">Оновити</button>
                        </div>
                    </div>

                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
</body>
<?php } ?>