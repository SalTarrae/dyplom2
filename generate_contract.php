<?php
require_once 'vendor/autoload.php';

use \Mpdf\Mpdf;

include("include/config.php");

if (isset($_GET['inventoryId'])) {
    $inventoryId = $_GET['inventoryId'];

    // Отримуємо інформацію про інвентаризацію з бази даних
    $query = mysqli_query($con, "SELECT * FROM inventory WHERE id = $inventoryId");
    $inventory = mysqli_fetch_assoc($query);

    // Парсимо JSON-дані про товари
    $products = json_decode($inventory['products'], true);

    // Створюємо HTML-розмітку для звіту
    $html = '
        <h1 style="text-align: center; font-weight: bold;">Звіт з інвентаризації</h1>
        <p>Відомість по інвентаризації №' . $inventoryId . ' від ' . date('d.m.Y', strtotime($inventory['date'])) . '</p>
        <table border="1" cellpadding="5" cellspacing="0" style="border-collapse: collapse; width: 100%;">
            <thead>
                <tr>
                    <th>Товар</th>
                    <th>Артикул</th>
                    <th>Одиниця вимірювання</th>
                    <th>Облікова кількість</th>
                    <th>Фактична кількість</th>
                    <th>Розходження</th>
                    <th>Собівартість</th>
                </tr>
            </thead>
            <tbody>
    ';

    // Отримуємо інформацію про товари
    $productQuery = mysqli_query($con, "SELECT p.id, p.article, p.productName, u.name AS unitName FROM products p JOIN units u ON p.units_id = u.id");

    foreach ($products as $product) {
        $productInfo = mysqli_fetch_assoc($productQuery);
        $difference = $product['accounting_quantity'] - $product['actual_quantity'];
        $html .= '
            <tr>
                <td>' . $productInfo['productName'] . '</td>
                <td>' . $productInfo['article'] . '</td>
                <td>' . $productInfo['unitName'] . '</td>
                <td>' . $product['accounting_quantity'] . '</td>
                <td>' . $product['actual_quantity'] . '</td>
                <td>' . $difference . '</td>
                <td>' . $product['purchase_price'] . '</td>
            </tr>
        ';
    }

    $html .= '
            </tbody>
        </table>
    ';

    // Створюємо PDF-файл за допомогою mPDF
    $mpdf = new Mpdf();
    $mpdf->WriteHTML($html);

    // Виводимо PDF-файл у новому вікні
    $mpdf->Output('inventory_report.pdf', 'I');
}
?>