<?php
session_start();
require_once 'func/login.php';
include("include/config.php");
require_once 'func/counterparties.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());
    $counterpartiesObj = new Counterparties($con);

    if (isset($_POST['submitLegal'])) {
        $name = $_POST['name'];
        $person = $_POST['person'];
        $bank = $_POST['bank'];
        $iban = $_POST['iban'];
        $mobile = $_POST['mobile'];
        $email = $_POST['email'];
        $another = $_POST['another'];
        $note = $_POST['note'];
        $country = $_POST['country'];
        $registerCode = $_POST['registerСode'];
        $payerCode = $_POST['payerCode'];
        $legalAddress = $_POST['legalAddress'];
        $postalAddress = $_POST['postalAddress'];
        $locationAddress = $_POST['locationAddress'];

        if ($counterpartiesObj->createCounterparty($name, $mobile, $email, $another, $note, $person, $country, $registerCode, $payerCode, $legalAddress, $postalAddress, $locationAddress, $iban, $bank)) {
            $_SESSION['msg'] = "Контрагента створено";
        } else {
            echo "Помилка при створенні контрагента: " . mysqli_error($con);
        }
    }
    ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Товари</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">

</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">


                <?php if(isset($_POST['submit']))
                {?>
                    <div class="alert alert-success">
                        <button type="buftton" class="close" data-dismiss="alert">×</button>
                        <strong>Все добре!</strong>	<?php echo htmlentities($_SESSION['msg']);?><?php echo htmlentities($_SESSION['msg']="");?>
                    </div>
                <?php } ?>


                <?php if(isset($_GET['del']))
                {?>
                    <div class="alert alert-error">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>О ні!</strong> 	<?php echo htmlentities($_SESSION['delmsg']);?><?php echo htmlentities($_SESSION['delmsg']="");?>
                    </div>
                <?php } ?>


            <br />
            <div id="legalFields" >
                <form id="legalForm" class="form-horizontal row-fluid" name="counterparties" method="post" enctype="multipart/form-data">
                    <div class="module">
                        <div class="module-head">
                            <h3>Загальні</h3>
                        </div>

                        <div class="module-body">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Повна назва</label>
                                <div class="controls">
                                    <input type="text"    name="name"  placeholder="Напишіть найменування" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Телефон</label>
                                <div class="controls">
                                    <input type="text"    name="mobile"  placeholder="Напишіть моб. телефон" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Email</label>
                                <div class="controls">
                                    <input type="text"    name="email"  placeholder="Напишіть email" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Інше</label>
                                <div class="controls">
                                    <textarea class="form-control" name="another" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Примітка</label>
                                <div class="controls">
                                    <textarea class="form-control" name="note" rows="3"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="module">
                        <div class="module-head">
                            <h3>Юридична особа</h3>
                        </div>
                        <div class="module-body">

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Відповідальна особа</label>
                                <div class="controls">
                                    <input type="text"    name="person"  placeholder="Напишіть банк" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Країна</label>
                                <div class="controls">
                                    <input type="text"    name="country"  placeholder="Напишіть країну" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Код ЄРДПОУ</label>
                                <div class="controls">
                                    <input type="text"  name="registerСode"  placeholder="Напишіть код" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Код платника ПДВ</label>
                                <div class="controls">
                                    <input type="text"    name="payerCode"  placeholder="Напишіть код УЗНР" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label"  for="basicinput">Юридична адреса</label>
                                <div class="controls">
                                    <input type="text" placeholder="Напишіть адресу" name="legalAddress" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Поштова адреса</label>
                                <div class="controls">
                                    <input type="text"    name="postalAddress"  placeholder="Напишіть адресу" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Адреса місцезнаходження</label>
                                <div class="controls">
                                    <input type="text"    name="locationAddress"  placeholder="Напишіть адресу" class="span8 tip" required>
                                </div>
                            </div>

                            <input type="hidden" name="selected_kveds" id="selected_kveds" value='[]'>
                            <div class="control-group">
                                <div class="controls">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="module">
                        <div class="module-head">
                            <h3>Банківські рахунки</h3>
                        </div>
                        <div class="module-body">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">IBAN</label>
                                <div class="controls">
                                    <input type="text"    name="iban"  placeholder="Напишіть iban" class="span8 tip" required>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Банк</label>
                                <div class="controls">
                                    <input type="text"    name="bank"  placeholder="Напишіть банк" class="span8 tip" required>
                                </div>
                            </div>
                            <button type="submit" name="submitLegal" class="btn btn-dark">Вставити</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


<script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    });
</script>
</body>
<?php } ?>