<?php
session_start();
require_once 'func/login.php';
require_once 'func/order.php';
include("include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}

if (isset($_GET['id'])) {
    $orderId = $_GET['id'];

    $order = new Order($con);
    $orderData = $order->getOrderById($orderId);

    // Отримання даних покупця
    $buyerQuery = mysqli_query($con, "SELECT * FROM data_store");
    $buyer = mysqli_fetch_assoc($buyerQuery);

    // Отримання даних товарів замовлення з JSON
    $productsJson = $orderData['products'];
    $products = json_decode($productsJson, true);

    // Генерація HTML коду для звіту
    $html = '<html>
                <head>
                    <style>
                        body { font-family: DejaVu Sans, sans-serif; }
                        table { border-collapse: collapse; width: 100%; }
                        th, td { border: 1px solid black; padding: 8px; text-align: left; }
                    </style>
                </head>
                <body>
                    <h1>Замовлення постачальнику №' . $orderData['number'] . ' від ' . $orderData['date'] . '</h1>
                    <p><strong>Постачальник:</strong> ' . $orderData['counterparty_name'] . '</p>
                    <p><strong>Покупець:</strong> ' . $buyer['full_name'] . ', ' . $buyer['legal_address'] . ', IBAN: ' . $buyer['iban'] . ', Банк: ' . $buyer['bank'] . '</p>
                    <p><strong>Склад:</strong> ' . $orderData['warehouse_address'] . '</p>
                    <table>
                        <thead>
                            <tr>
                                <th>№</th>
                                <th>Товар</th>
                                <th>Кількість</th>
                                <th>Одиниці виміру</th>
                                <th>Ціна з ПДВ</th>
                                <th>Сума з ПДВ</th>
                            </tr>
                        </thead>
                        <tbody>';

    $totalItems = 0;
    $totalSum = 0;
    $cnt = 1;
    foreach ($products as $product) {
        $productId = $product['id'];
        $price = $product['price'];
        $quantity = $product['quantity'];

        // Отримання даних товару з таблиці products
        $productQuery = mysqli_query($con, "SELECT products.productName, units.name AS unit_name
                                             FROM products 
                                             JOIN units ON units.id = products.units_id
                                             WHERE products.id = $productId");
        $productData = mysqli_fetch_assoc($productQuery);

        $productName = $productData['productName'];
        $unitName = $productData['unit_name'];
        $sum = $price * $quantity;

        $html .= '<tr>
                      <td>' . $cnt . '</td>
                      <td>' . $productName . '</td>
                      <td>' . $quantity . '</td>
                      <td>' . $unitName . '</td>
                      <td>' . $price . '</td>
                      <td>' . $sum . '</td>
                  </tr>';
        $totalItems += $quantity;
        $totalSum += $sum;
        $cnt++;
    }

    $html .= '</tbody>
              </table>
              <p><strong>Всього позицій:</strong> ' . $totalItems . ' на суму ' . $totalSum . ' грн.</p>
              <p><strong>У т.ч. ПДВ:</strong> нуль гривень 00 копійок</p>
              <p>Від постачальника _________________________________________</p>
              <p>Від покупця _________________________________________</p>
          </body>
      </html>';

    // Генерація PDF за допомогою mpdf
    require_once __DIR__ . '/vendor/autoload.php';

    $mpdf = new \Mpdf\Mpdf();
    $mpdf->WriteHTML($html);
    $mpdf->Output('order_' . $orderData['number'] . '.pdf', 'I');
    exit;
}
?>