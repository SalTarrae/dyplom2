<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/product.php';

$product = new Product($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    header("Location: index.php");
    exit();
} else {
date_default_timezone_set('Europe/Kiev');
$currentTime = date('d-m-Y h:i:s A', time());

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $product->createUnit($name);
    $_SESSION['msg'] = "Created !!";
    header("Location: units.php");
    exit();
}

if (isset($_GET['del'])) {
    $id = mysqli_real_escape_string($con, $_GET['id']);
    $result = $product->deleteUnit($id);

    if ($result) {
        $_SESSION['delmsg'] = "Видалено!";
    } else {
        $error_message = "Помилка: " . mysqli_error($con);
        $_SESSION['error'] = $error_message;
    }
    header("Location: units.php");
    exit();
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $result = $product->updateUnit($id, $name);

    if ($result) {
        $_SESSION['updatemsg'] = "Оновлено!";
    } else {
        $error_message = "Помилка: " . mysqli_error($con);
        $_SESSION['error'] = $error_message;
    }
    header("Location: units.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Одиниці виміру </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Одиниці виміру</h3>
                </div>
                <div class="module-body">
                    <?php if(isset($_SESSION['msg'])) { ?>
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Все добре!</strong> <?php echo htmlentities($_SESSION['msg']);?>
                        </div>
                        <?php unset($_SESSION['msg']); ?>
                    <?php } ?>
                           <?php if(isset($_SESSION['delmsg'])) { ?>
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong></strong> <?php echo htmlentities($_SESSION['delmsg']);?>
                        </div>
                        <?php unset($_SESSION['delmsg']); ?>
                    <?php } ?>

                    <?php if(isset($_SESSION['updatemsg'])) { ?>
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Оновлено!</strong> <?php echo htmlentities($_SESSION['updatemsg']);?>
                        </div>
                        <?php unset($_SESSION['updatemsg']); ?>
                    <?php } ?>

                    <?php if(isset($_SESSION['error'])) { ?>
                        <div class="alert alert-danger alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong></strong> <?php echo htmlentities($_SESSION['error']);?>
                        </div>
                        <?php unset($_SESSION['error']); ?>
                    <?php } ?>

                    <br />

                    <form class="form-horizontal row-fluid" name="Units" method="post">
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Назва одиниці виміру</label>
                            <div class="controls">
                                <input type="text" placeholder="Назва" name="name" class="span8 tip" required>
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <button type="submit" name="submit" class="btn btn-dark">Створити</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="module">
                <div class="module-head">
                    <h3>Одиниці виміру</h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Одиниця виміру</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $query = $product->getAllUnits();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['name']); ?></td>
                                <td>
                                    <a href="#" class="edit-link" data-id="<?php echo $row['id']; ?>" data-name="<?php echo $row['name']; ?>" data-toggle="modal" data-target="#editModal"><i class="icon-edit"></i></a>
                                    <a href="units.php?id=<?php echo $row['id']?>&del=delete" onClick="return confirm('Ви дійсно хочете видалити запис?')"><i class="icon-remove-sign"></i></a>
                                </td>
                            </tr>
                            <?php $cnt=$cnt+1; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Модальне вікно для редагування -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Редагувати одиницю виміру</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="modal-body">
                    <input type="hidden" id="editId" name="id">
                    <div class="form-group">
                        <label for="editName">Назва одиниці виміру</label>
                        <input type="text" class="form-control" id="editName" name="name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" class="btn btn-primary" name="update">Зберегти зміни</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="../scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.edit-link').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');
            $('#editId').val(id);
            $('#editName').val(name);
        });
    });
</script>
</body>
<?php } ?>
</html>