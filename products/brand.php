<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/product.php';

$product = new Product($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    header("Location: index.php");
    exit();
} else {
date_default_timezone_set('Europe/Kiev');
$currentTime = date('d-m-Y h:i:s A', time());

if (isset($_POST['submit'])) {
    $trade_name = $_POST['trade_name'];
    $description = $_POST['description'];
    $country = $_POST['country'];
    $product->createTradeMark($trade_name, $description, $country);
    $_SESSION['msg'] = "Created !!";
    header("Location: brand.php");
    exit();
}

if (isset($_GET['del'])) {
    $id = mysqli_real_escape_string($con, $_GET['id']);
    $result = $product->deleteTradeMark($id);

    if ($result) {
        $_SESSION['delmsg'] = "Видалено!";
    } else {
        $error_message = "Помилка: " . mysqli_error($con);
        $_SESSION['error'] = $error_message;
    }
    header("Location: brand.php");
    exit();
}

if (isset($_POST['update'])) {
    $id = $_POST['id'];
    $trade_name = $_POST['trade_name'];
    $description = $_POST['description'];
    $country = $_POST['country'];
    $result = $product->updateTradeMark($id, $trade_name, $description, $country);

    if ($result) {
        $_SESSION['updatemsg'] = "Оновлено!";
    } else {
        $error_message = "Помилка: " . mysqli_error($con);
        $_SESSION['error'] = $error_message;
    }
    header("Location: brand.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Торгова марка </title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Торгова марка</h3>
                </div>
                <div class="module-body">
                    <?php if(isset($_SESSION['msg'])) { ?>
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Все добре!</strong> <?php echo htmlentities($_SESSION['msg']);?>
                        </div>
                        <?php unset($_SESSION['msg']); ?>
                    <?php } ?>

                    <?php if(isset($_SESSION['delmsg'])) { ?>
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong></strong> <?php echo htmlentities($_SESSION['delmsg']);?>
                        </div>
                        <?php unset($_SESSION['delmsg']); ?>
                    <?php } ?>

                    <?php if(isset($_SESSION['updatemsg'])) { ?>
                        <div class="alert alert-success alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong>Оновлено!</strong> <?php echo htmlentities($_SESSION['updatemsg']);?>
                        </div>
                        <?php unset($_SESSION['updatemsg']); ?>
                    <?php } ?>

                    <?php if(isset($_SESSION['error'])) { ?>
                        <div class="alert alert-danger alert-dismissible fade show">
                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                            <strong></strong> <?php echo htmlentities($_SESSION['error']);?>
                        </div>
                        <?php unset($_SESSION['error']); ?>
                    <?php } ?>

                    <br />

                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addModal">Додати Торгову марку</button>

                    <br /><br />

                    <div class="module-body table">
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Опис</th>
                                <th>Країна</th>
                                <th>Дія</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $query = $product->getAllTradeMarks();
                            $cnt = 1;
                            while ($row = mysqli_fetch_array($query)) {
                                ?>
                                <tr>
                                    <td><?php echo htmlentities($cnt); ?></td>
                                    <td><?php echo htmlentities($row['trade_name']); ?></td>
                                    <td><?php echo htmlentities($row['description']); ?></td>
                                    <td><?php echo htmlentities($row['country']); ?></td>
                                    <td>
                                        <a href="#" class="edit-link" data-id="<?php echo $row['id']; ?>" data-trade_name="<?php echo $row['trade_name']; ?>" data-description="<?php echo $row['description']; ?>" data-country="<?php echo $row['country']; ?>" data-toggle="modal" data-target="#editModal"><i class="icon-edit"></i></a>
                                        <a href="brand.php?id=<?php echo $row['id']?>&del=delete" onClick="return confirm('Ви дійсно хочете видалити запис?')"><i class="icon-remove-sign"></i></a>
                                    </td>
                                </tr>
                                <?php $cnt=$cnt+1; } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Модальне вікно для додавання -->
<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Додати Торгову марку</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="trade_name">Назва</label>
                        <input type="text" class="form-control" id="trade_name" name="trade_name" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Опис</label>
                        <textarea class="form-control" id="description" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="country">Країна</label>
                        <input type="text" class="form-control" id="country" name="country">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" class="btn btn-primary" name="submit">Додати</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Модальне вікно для редагування -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editModalLabel">Редагувати Торгову марку</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editForm" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                <div class="modal-body">
                    <input type="hidden" id="editId" name="id">
                    <div class="form-group">
                        <label for="editTradeName">Назва</label>
                        <input type="text" class="form-control" id="editTradeName" name="trade_name" required>
                    </div>
                    <div class="form-group">
                        <label for="editDescription">Опис</label>
                        <textarea class="form-control" id="editDescription" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editCountry">Країна</label>
                        <input type="text" class="form-control" id="editCountry" name="country">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" class="btn btn-primary" name="update">Зберегти зміни</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="../scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.edit-link').click(function() {
            var id = $(this).data('id');
            var trade_name = $(this).data('trade_name');
            var description = $(this).data('description');
            var country = $(this).data('country');
            $('#editId').val(id);
            $('#editTradeName').val(trade_name);
            $('#editDescription').val(description);
            $('#editCountry').val(country);
        });
    });
</script>
</body>
<?php } ?>
</html>