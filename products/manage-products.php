<?php
session_start();
require_once '../func/login.php';
require_once '../func/product.php';
include("../include/config.php");

$productManager = new Product($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    if (isset($_GET['del'])) {
        $productManager->deleteProduct($_GET['id']);
        $_SESSION['delmsg'] = "Товар видалено!";
    }

    $products = $productManager->getProducts();
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Товари</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Товари</h3>
                    </div>
                    <?php if (!$user->hasRole('manager')) { ?>
                        <div class="module-body">
                            <div class="control-group">
                                <div class="controls">
                                    <a href="../index.php" class="btn btn-dark">Створити новий товар</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>

                <div class="module-body table">
                    <?php if (isset($_GET['del'])) { ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Увага!</strong> <?php echo htmlentities($_SESSION['delmsg']); ?>
                            <?php echo htmlentities($_SESSION['delmsg'] = ""); ?>
                        </div>
                    <?php } ?>

                    <br/>

                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Назва</th>
                            <th>Категорія</th>
                            <th>Підкатегорія</th>
                            <th>Компанія</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($products)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['productName']); ?></td>
                                <td><?php echo htmlentities($row['categoryName']); ?></td>
                                <td><?php echo htmlentities($row['subcategory']); ?></td>
                                <td><?php echo htmlentities($row['trade_name']); ?></td>
                                <td>
                                    <a href="../edit-products.php?id=<?php echo $row['id']; ?>"><i class="icon-edit"></i></a>
                                    <a href="manage-products.php?id=<?php echo $row['id']; ?>&del=delete" onClick="return confirm('Ви впевнені, що хочете видалити цей товар?')"><i class="icon-remove-sign"></i></a>
                                </td>
                            </tr>
                            <?php
                            $cnt++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        });
    </script>
    </body>
    </html>
    <?php
}
?>