<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/product.php';

$product = new Product($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    if (isset($_POST['submit'])) {
        $category = $_POST['category'];
        $description = $_POST['description'];
        $product->createCategory($category);
        $_SESSION['msg'] = "Category Created !!";
    }

    if (isset($_GET['del'])) {
        $product->deleteCategory($_GET['id']);
        $_SESSION['delmsg'] = "Category deleted !!";
    }

    if (isset($_POST['update'])) {
        $categoryId = $_POST['category_id'];
        $categoryName = $_POST['category_name'];
        $product->updateCategory($categoryId, $categoryName);
        $_SESSION['updatemsg'] = "Category updated !!";
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Категорії</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">

                <div class="module">
                    <div class="module-head">
                        <h3>Категорії</h3>
                    </div>
                    <div class="module-body">

                        <?php if (isset($_POST['submit'])) { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Все добре!</strong> <?php echo htmlentities($_SESSION['msg']); ?><?php echo htmlentities($_SESSION['msg'] = ""); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>

                        <?php if (isset($_GET['del'])) { ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Видалено!</strong> <?php echo htmlentities($_SESSION['delmsg']); ?><?php echo htmlentities($_SESSION['delmsg'] = ""); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>

                        <?php if (isset($_POST['update'])) { ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>Оновлено!</strong> <?php echo htmlentities($_SESSION['updatemsg']); ?><?php echo htmlentities($_SESSION['updatemsg'] = ""); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>

                        <br/>

                        <form class="form-horizontal row-fluid" name="Category" method="post">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Назва категорії</label>
                                <div class="controls">
                                    <input type="text" placeholder="Назва категорії" name="category" class="span8 tip" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" name="submit" class="btn btn-dark">Створити</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="module">
                    <div class="module-head">
                        <h3>Категорії</h3>
                    </div>
                    <div class="module-body table">
                        <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Категорії</th>
                                <th>Дія</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            $query = $product->getAllCategories();
                            $cnt = 1;
                            while ($row = mysqli_fetch_array($query)) {
                                ?>
                                <tr>
                                    <td><?php echo htmlentities($cnt); ?></td>
                                    <td><?php echo htmlentities($row['categoryName']); ?></td>
                                    <td>
                                        <a href="#editModal" data-toggle="modal" data-id="<?php echo $row['id']; ?>" data-name="<?php echo $row['categoryName']; ?>"><i class="icon-edit"></i></a>
                                                                   <a href="category.php?id=<?php echo $row['id']?>&del=delete" onClick="return confirm('Ви дійсно хочете видалити запис?')"><i class="icon-remove-sign"></i></a>
                                    </td>
                                </tr>
                                <?php $cnt = $cnt + 1;
                            } ?>

                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editModalLabel">Редагувати категорію</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <input type="hidden" id="category_id" name="category_id">
                        <div class="form-group">
                            <label for="category_name">Назва категорії</label>
                            <input type="text" class="form-control" id="category_name" name="category_name" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                        <button type="submit" name="update" class="btn btn-primary">Зберегти зміни</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');

            $('#confirm-delete').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });

            $('a[data-toggle="modal"]').on('click', function(e) {
                var modal = $($(this).attr('href'));
                modal.find('#category_id').val($(this).data('id'));
                modal.find('#category_name').val($(this).data('name'));
            });
        });
    </script>
    </body>
    </html>
<?php } ?>