<?php
session_start();
require_once 'func/login.php';
include("include/config.php");

$user = new User($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}

if (isset($_GET['token'])) {
    $token = $_GET['token'];

    // Check if the token is valid
    $userInfo = $user->getUserByResetToken($token);

    if ($userInfo) {
        $email = $userInfo['email'];

        if (isset($_POST['changePassword'])) {
            $newPassword = $_POST['newPassword'];
            $confirmPassword = $_POST['confirmPassword'];

            if ($newPassword === $confirmPassword) {
                if ($user->updatePassword($email, $newPassword)) {
                    $_SESSION['success_msg'] = "Password changed successfully.";
                    header("Location: user.php");
                    exit();
                } else {
                    $_SESSION['error_msg'] = "Failed to update password.";
                }
            } else {
                $_SESSION['error_msg'] = "Passwords do not match.";
            }
        }

        ?>

        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Change Password</title>
            <link type="text/css" href="css/theme.css" rel="stylesheet">
            <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
            <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
            <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        </head>
        <body>
        <div class="container-fluid">
            <div class="row flex-nowrap sticky-lg-top">
                <?php include('include/side.php'); ?>
                <div class="col py-3">
                    <div class="module-head">
                        <h3>Change Password</h3>
                    </div>

                    <?php if (isset($_SESSION['success_msg'])) { ?>
                        <div class="alert alert-success"><?php echo $_SESSION['success_msg']; ?></div>
                        <?php unset($_SESSION['success_msg']); ?>
                    <?php } ?>

                    <?php if (isset($_SESSION['error_msg'])) { ?>
                        <div class="alert alert-danger"><?php echo $_SESSION['error_msg']; ?></div>
                        <?php unset($_SESSION['error_msg']); ?>
                    <?php } ?>

                    <form method="post">
                        <div class="form-group">
                            <label for="newPassword">New Password:</label>
                            <input type="password" class="form-control" id="newPassword" name="newPassword" required>
                        </div>
                        <div class="form-group">
                            <label for="confirmPassword">Confirm Password:</label>
                            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" required>
                        </div>
                        <button type="submit" class="btn btn-primary" name="changePassword">Change Password</button>
                    </form>
                </div>
            </div>
        </div>
        </body>
        </html>

        <?php
    } else {
        $_SESSION['error_msg'] = "Invalid or expired password reset token.";
        header("Location: user.php");
        exit();
    }
} else {
    $_SESSION['error_msg'] = "No password reset token provided.";
    header("Location: user.php");
    exit();
}
?>