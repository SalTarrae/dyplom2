$(document).ready(function() {
    $('#createWarehouseModal').on('shown.bs.modal', function () {
        $('#createWarehouseModal').trigger('focus');
    });
});

$(document).ready(function() {
    $('.datatable-1').dataTable();
    $('.dataTables_paginate').addClass("btn-group datatable-pagination");
    $('.dataTables_paginate > a').wrapInner('<span />');
    $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
    $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
} );

$(document).ready(function() {
    $('#createWarehouseModal').modal('hide');
});

$(document).ready(function() {
    $('.edit-warehouse').click(function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var address = $(this).data('address');

        $('#edit_warehouse_id').val(id);
        $('#edit_warehouse_name').val(name);
        $('#edit_warehouse_address').val(address);
    });
});#createWarehouseModal