<?php
session_start();
require_once 'func/login.php';
require_once 'include/config.php';
require_once 'func/stocks.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}

$stocks = new Stocks($con);
date_default_timezone_set('Europe/Kiev');
$currentTime = date('d-m-Y h:i:s A', time());

// Обробка AJAX-запиту для отримання товарів для вибраного складу
if (isset($_GET['action']) && $_GET['action'] === 'getStockProducts') {
    $warehouseId = $_GET['warehouseId'];

    $stockProducts = $stocks->getStockProducts($warehouseId);
    $products = array();
    while ($stockRow = mysqli_fetch_array($stockProducts)) {
        $products[] = array(
            'id' => $stockRow['id'],
            'stock_id' => $stockRow['stock_id'],
            'productName' => $stockRow['productName'],
            'quantity' => $stockRow['quantity'],
            'purchase_price' => $stockRow['purchase_price'],
            'date_of_receipt' => $stockRow['date_of_receipt'],
            'unitName' => $stockRow['unitName']
        );
    }

    echo json_encode($products);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Списання товару</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <?php
            if (isset($_SESSION['msg']) && $_SESSION['msg'] !== "") {
                if ($_SESSION['msg_type'] === 'success') {
                    echo '<div class="alert alert-success">';
                } else {
                    echo '<div class="alert alert-danger">';
                }
                echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                echo '<strong>' . ($_SESSION['msg_type'] === 'success' ? 'Успішно!' : 'Помилка!') . '</strong> ' . htmlentities($_SESSION['msg']);
                echo '</div>';
                $_SESSION['msg'] = "";
                $_SESSION['msg_type'] = "";
            }
            ?>
            <div class="module-head">
                <h3>Списання товару</h3>
            </div>
            <div class="module-body">
                <form method="post" action="">
                    <div class="form-group">
                        <label for="date">Дата:</label>
                        <input type="date" class="form-control" id="date" name="date" required>
                    </div>
                    <div class="form-group">
                        <label for="warehouse">Склад:</label>
                        <select class="form-control" id="warehouse" name="warehouse" required>
                            <option value="">Виберіть склад</option>
                            <?php
                            $warehouses = $stocks->getAllWarehouses();
                            while ($warehouseRow = mysqli_fetch_array($warehouses)) {
                                ?>
                                <option value="<?php echo $warehouseRow['id']; ?>"><?php echo $warehouseRow['name']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="reason">Причина списання:</label>
                        <select class="form-control" id="reason" name="reason" required>
                            <option value="">Виберіть причину</option>
                            <option value="Брак">Брак</option>
                            <option value="Якість при зберіганні">Якість при зберіганні</option>
                            <option value="Інвентеризація">Інвентеризація</option>
                            <option value="Псування">Псування</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="products">Товари:</label>
                        <table class="table" id="productsTable">
                            <thead>
                            <tr>
                                <th>Назва товару</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                                <th>Дії</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#productModal">Додати товар</button>
                    </div>
                    <button type="submit" class="btn btn-primary" name="submit">Зберегти</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Product Modal -->
<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productModalLabel">Виберіть товар</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table" id="stockTable">
                    <thead>
                    <tr>
                        <th>Назва товару</th>
                        <th>Кількість</th>
                        <th>Ціна</th>
                        <th>Партія від</th>
                        <th>Одиниці</th>
                        <th>Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var products = [];
        // Обробник зміни вибраного складу
        $('#warehouse').change(function () {
            var warehouseId = $(this).val();
            loadStockProducts(warehouseId);
        });

        // Завантаження товарів для вибраного складу по AJAX
        function loadStockProducts(warehouseId) {
            $.ajax({
                url: '',
                method: 'GET',
                data: {action: 'getStockProducts', warehouseId: warehouseId},
                dataType: 'json',
                success: function (response) {
                    var tableBody = $('#stockTable tbody');
                    tableBody.empty();

                    response.forEach(function (product) {
                        var row = $('<tr>');
                        row.append($('<td>').text(product.productName));
                        row.append($('<td>').text(product.quantity));
                        row.append($('<td>').text(product.purchase_price));
                        row.append($('<td>').text('Партія від ' + product.date_of_receipt));
                        row.append($('<td>').text(product.unitName));
                        row.append($('<td>').html('<button type="button" class="btn btn-primary btn-sm add-product" data-id="' + product.id + '" data-stock-id="' + product.stock_id + '" data-name="' + product.productName + '" data-quantity="' + product.quantity + '" data-price="' + product.purchase_price + '">Додати</button>'));
                        tableBody.append(row);
                    });
                }
            });
        }

        // Обробник події click для кнопки "Додати товар"
        $(document).on('click', '.add-product', function () {
            var productId = $(this).data('id');
            var stockId = $(this).data('stock-id');
            var productName = $(this).data('name');
            var maxQuantity = parseInt($(this).data('quantity'));
            var purchasePrice = parseFloat($(this).data('price'));

            // Перевірка, чи товар вже доданий до масиву
            var existingProduct = products.find(function (product) {
                return product.stock_id === stockId;
            });

            if (existingProduct) {
                alert("Товар вже доданий до списку.");
            } else {
                var quantity = prompt(`Введіть кількість для "${productName}"`, 1);

                if (quantity !== null) {
                    quantity = parseInt(quantity);

                    if (quantity > 0 && quantity <= maxQuantity) {
                        products.push({
                            id: productId,
                            stock_id: stockId,
                            quantity: quantity,
                            purchasePrice: purchasePrice
                        });

                        updateProductsTable();
                        $('#productModal').modal('hide');
                    } else {
                        alert("Введена кількість некоректна або перевищує доступну кількість.");
                    }
                }
            }
        });

        function updateProductsTable() {
            var tableBody = $('#productsTable tbody');
            tableBody.empty();

            products.forEach(function (product, index) {
                var row = $('<tr>');
                row.append($('<td>').text(product.name));
                var quantityInput = $('<input>').attr({
                    type: 'number',
                    class: 'form-control product-quantity',
                    'data-index': index,
                    min: 1,
                    max: product.maxQuantity
                }).val(product.quantity);
                row.append($('<td>').append(quantityInput));
                row.append($('<td>').text(product.purchasePrice));
                row.append($('<td>').html('<button type="button" class="btn btn-danger btn-sm remove-product" data-stock-id="' + product.stock_id + '">Видалити</button>'));
                tableBody.append(row);
            });

            $('.remove-product').click(function () {
                var stockId = $(this).data('stock-id');
                products = products.filter(function (product) {
                    return product.stock_id !== stockId;
                });
                updateProductsTable();
            });

            $('.product-quantity').change(function () {
                var index = $(this).data('index');
                var newQuantity = parseInt($(this).val());
                if (newQuantity <= products[index].maxQuantity) {
                    products[index].quantity = newQuantity;
                } else {
                    alert(`Кількість не може перевищувати ${products[index].maxQuantity}.`);
                    $(this).val(products[index].quantity);
                }
            });
        }

        $('form').submit(function (event) {
            event.preventDefault(); // Запобігти перезавантаженню сторінки

            var date = $('#date').val();
            var warehouseId = $('#warehouse').val();
            var reason = $('#reason').val();

            if (products.length === 0) {
                alert("Виберіть принаймні один товар для списання.");
                return; // Зупинити відправку форми
            }

            $.ajax({
                url: '',
                method: 'POST',
                data: {
                    submit: true,
                    date: date,
                    warehouse: warehouseId,
                    reason: reason,
                    products: JSON.stringify(products)
                },
                success: function (response) {
                    // Обробка успішної відповіді від сервера
                    alert("Списання товару успішно збережено.");
                    // Очищення форми та масиву products
                    $('#date').val('');
                    $('#warehouse').val('');
                    $('#reason').val('');
                    products = [];
                    updateProductsTable();
                },
                error: function (xhr, status, error) {
                    // Обробка помилки
                    alert("Помилка при збереженні списання товару.");
                }
            });
        });
    });
</script>
<?php
if (isset($_POST['submit'])) {
    $date = $_POST['date'];
    $warehouseId = $_POST['warehouse'];
    $reason = $_POST['reason'];
    $products = json_decode($_POST['products'], true); // Декодуємо дані про товари з форми
    if (empty($products)) {
        $_SESSION['msg'] = "Помилка: Не вибрано жодного товару.";
        $_SESSION['msg_type'] = "error";
    } else {
        try {
            if ($stocks->createWriteOff($date, $warehouseId, $reason, $products)) {
                $_SESSION['msg'] = "Списання товару успішно збережено.";
                $_SESSION['msg_type'] = "success";
                // Перенаправлення на сторінку write-off.php
                header("Location: write-off.php");
                exit();
            } else {
                throw new Exception("Помилка при збереженні списання товару.");
            }
        } catch (Exception $e) {
            $_SESSION['msg'] = $e->getMessage();
            $_SESSION['msg_type'] = "error";
        }
    }
}
?>
</body>
</html>