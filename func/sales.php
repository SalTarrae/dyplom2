<?php
class Sales {
    private $con;

    public function __construct($con) {
        $this->con = $con;
    }

    public function addSale($date, $products, $payment) {
        $products_json = json_encode($products);
        $sql = mysqli_query($this->con, "INSERT INTO sales (date, products, payment) VALUES ('$date', '$products_json', '$payment')");
        return $sql;
    }

    public function deleteSale($id) {
        mysqli_query($this->con, "DELETE FROM sales WHERE id = '$id'");
    }

    public function getSale($id) {
        $query = mysqli_query($this->con, "SELECT * FROM sales WHERE id = '$id'");
        return mysqli_fetch_assoc($query);
    }

    public function getAllSales() {
        $query = mysqli_query($this->con, "SELECT * FROM sales");
        return $query;
    }

    public function updateStocks($products) {
        foreach ($products as $product) {
            $productId = $product['id'];
            $quantity = $product['quantity'];

            $query = mysqli_query($this->con, "SELECT * FROM stocks WHERE id_product = '$productId' AND id_warehouse = (SELECT id FROM warehouse WHERE name = 'Магазин') ORDER BY data_expiry ASC");

            while ($row = mysqli_fetch_array($query)) {
                $stockId = $row['id'];
                $stockQuantity = $row['quantity'];

                if ($stockQuantity >= $quantity) {
                    $newQuantity = $stockQuantity - $quantity;
                    if ($newQuantity == 0) {
                        mysqli_query($this->con, "DELETE FROM stocks WHERE id = '$stockId'");
                    } else {
                        mysqli_query($this->con, "UPDATE stocks SET quantity = '$newQuantity' WHERE id = '$stockId'");
                    }
                    $quantity = 0;
                } else {
                    mysqli_query($this->con, "DELETE FROM stocks WHERE id = '$stockId'");
                    $quantity -= $stockQuantity;
                }

                if ($quantity == 0) {
                    break;
                }
            }
        }
    }

    public function getMonthlySalesCount() {
        $query = mysqli_query($this->con, "SELECT COUNT(*) AS count FROM sales WHERE MONTH(date) = MONTH(CURDATE()) AND YEAR(date) = YEAR(CURDATE())");
        $row = mysqli_fetch_assoc($query);
        return $row['count'];
    }

    public function getMonthlyProductsSold() {
        $query = mysqli_query($this->con, "SELECT SUM(JSON_LENGTH(products)) AS total_sold FROM sales WHERE MONTH(date) = MONTH(CURDATE()) AND YEAR(date) = YEAR(CURDATE())");
        $row = mysqli_fetch_assoc($query);
        return $row['total_sold'];
    }

    public function getDailyProductsSold() {
        $query = mysqli_query($this->con, "SELECT SUM(JSON_LENGTH(products)) AS total_sold FROM sales WHERE DATE(date) = CURDATE()");
        $row = mysqli_fetch_assoc($query);
        return $row['total_sold'] ?? 0;
    }

    public function getPopularProduct() {
        $query = mysqli_query($this->con, "SELECT JSON_EXTRACT(products, '$[*].id_product') AS product_ids
                                            FROM sales
                                            WHERE MONTH(date) = MONTH(CURDATE()) AND YEAR(date) = YEAR(CURDATE())");

        $productCounts = array();

        while ($row = mysqli_fetch_assoc($query)) {
            $productIds = json_decode($row['product_ids']);

            foreach ($productIds as $productId) {
                if (isset($productCounts[$productId])) {
                    $productCounts[$productId]++;
                } else {
                    $productCounts[$productId] = 1;
                }
            }
        }

        if (!empty($productCounts)) {
            $popularProductId = array_search(max($productCounts), $productCounts);

            $productQuery = mysqli_query($this->con, "SELECT * FROM products WHERE id = $popularProductId");
            $productRow = mysqli_fetch_assoc($productQuery);
            return $productRow['productName'];
        }

        return "";
    }

    public function getSalesByDay() {
        $query = mysqli_query($this->con, "SELECT DATE(date) AS day, COUNT(*) AS count 
                                            FROM sales
                                            WHERE MONTH(date) = MONTH(CURDATE()) AND YEAR(date) = YEAR(CURDATE())
                                            GROUP BY DATE(date)");

        $salesByDay = array();

        while ($row = mysqli_fetch_assoc($query)) {
            $day = $row['day'];
            $count = $row['count'];
            $salesByDay[$day] = $count;
        }

        return $salesByDay;
    }

    public function getSalesReport($date) {
        $query = "SELECT p.article, p.productName, p.units_id, s.products, s.payment
                  FROM sales s
                  INNER JOIN products p ON JSON_EXTRACT(s.products, '$[*].id_product') LIKE CONCAT('%', p.id, '%')
                  WHERE DATE(s.date) = '$date'";
        $result = mysqli_query($this->con, $query);
        return $result;
    }
}
?>