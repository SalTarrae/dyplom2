<?php
class Order {
    private $con;

    public function __construct($con) {
        $this->con = $con;
    }
    public function getAllOrders() {
        $query = mysqli_query($this->con, "SELECT * FROM order_to_supplier");
        return $query;
    }
    public function getOrderById($orderId) {
        $query = mysqli_query($this->con, "SELECT order_to_supplier.*, legal_entity.full_name AS counterparty_name, warehouse.address AS warehouse_address, warehouse.name AS warehouse_name
                                            FROM order_to_supplier
                                            JOIN legal_entity ON legal_entity.id = order_to_supplier.id_counterparties
                                            JOIN warehouse ON warehouse.id = order_to_supplier.id_warehouse
                                            WHERE order_to_supplier.id = $orderId");
        return mysqli_fetch_assoc($query);
    }
    public function getOrders($period, $year, $month) {
        $query = "SELECT o.id, o.id_counterparties, le.full_name AS supplier_name, o.number AS order_number, o.date AS order_date, o.sum AS order_sum,
                         gr.date AS delivery_date, gr.sum AS delivery_sum
                  FROM order_to_supplier o
                  LEFT JOIN goods_receipt gr ON o.id = gr.id_order
                  LEFT JOIN legal_entity le ON o.id_counterparties = le.id";

        if ($period === 'year' && $year !== null) {
            $query .= " WHERE YEAR(o.date) = $year";
        } elseif ($period === 'month' && $year !== null && $month !== null) {
            $query .= " WHERE YEAR(o.date) = $year AND MONTH(o.date) = $month";
        } elseif ($period === 'week') {
            $query .= " WHERE o.date >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK)";
        } elseif ($period === 'day') {
            $query .= " WHERE DATE(o.date) = CURDATE()";
        }

        $result = mysqli_query($this->con, $query);
        $orders = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $orders[] = $row;
        }
        return $orders;
    }
    public function deleteOrder($orderId) {
        $query = mysqli_query($this->con, "DELETE FROM order_to_supplier WHERE id = $orderId");
        return $query;
    }

    public function getCounterpartyNameById($counterpartyId) {
        $query = mysqli_query($this->con, "SELECT full_name FROM legal_entity WHERE id = $counterpartyId");
        $row = mysqli_fetch_assoc($query);
        return $row['full_name'];
    }

    public function getWarehouseNameById($warehouseId) {
        $query = mysqli_query($this->con, "SELECT name FROM warehouse WHERE id = $warehouseId");
        $row = mysqli_fetch_assoc($query);
        return $row['name'];
    }
    public function generateOrdersTable($orders) {
        $tableHtml = '<table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Постачальник</th>
                                <th>Дата</th>
                                <th>Номер замовлення</th>
                                <th>Дата постачання</th>
                                <th>Замовлення на суму</th>
                                <th>Отримано на суму</th>
                                <th>Не отримано на суму</th>
                            </tr>
                        </thead>
                        <tbody>';

        $totalOrderSum = 0;
        $totalDeliverySum = 0;
        $totalNotDeliveredSum = 0;

        foreach ($orders as $row) {
            $orderSum = $row['order_sum'];
            $deliverySum = $row['delivery_sum'] !== null ? $row['delivery_sum'] : 0;
            $notDeliveredSum = $orderSum - $deliverySum;

            $tableHtml .= "<tr>
                            <td>" . $row['supplier_name'] . "</td>
                            <td>" . $row['order_date'] . "</td>
                            <td>" . $row['order_number'] . "</td>
                            <td>" . ($row['delivery_date'] !== null ? $row['delivery_date'] : '-') . "</td>
                            <td>" . number_format($orderSum, 2) . "</td>
                            <td>" . number_format($deliverySum, 2) . "</td>
                            <td>" . number_format($notDeliveredSum, 2) . "</td>
                          </tr>";

            $totalOrderSum += $orderSum;
            $totalDeliverySum += $deliverySum;
            $totalNotDeliveredSum += $notDeliveredSum;
        }

        $tableHtml .= '</tbody></table>';

        return $tableHtml;
    }

    public function generateTotalHtml($orders) {
        $totalOrderSum = 0;
        $totalDeliverySum = 0;
        $totalNotDeliveredSum = 0;

        foreach ($orders as $row) {
            $orderSum = $row['order_sum'];
            $deliverySum = $row['delivery_sum'] !== null ? $row['delivery_sum'] : 0;
            $notDeliveredSum = $orderSum - $deliverySum;

            $totalOrderSum += $orderSum;
            $totalDeliverySum += $deliverySum;
            $totalNotDeliveredSum += $notDeliveredSum;
        }

        $totalHtml = '<p><strong>Разом по замовленням:</strong> ' . number_format($totalOrderSum, 2) . '</p>';
        $totalHtml .= '<p><strong>Разом отримано:</strong> ' . number_format($totalDeliverySum, 2) . '</p>';
        $totalHtml .= '<p><strong>Разом не отримано:</strong> ' . number_format($totalNotDeliveredSum, 2) . '</p>';

        return $totalHtml;
    }

    public function generatePdfContent($orders) {
        $html = '<html>
                    <head>
                        <style>
                            body { font-family: Arial, sans-serif; }
                            table { border-collapse: collapse; width: 100%; }
                            th, td { border: 1px solid black; padding: 8px; text-align: left; }
                            th { background-color: #f2f2f2; }
                        </style>
                    </head>
                    <body>
                        <h1 style="text-align: center;">Контроль поставок</h1>
                        <table>
                            <thead>
                                <tr>
                                    <th>Постачальник</th>
                                    <th>Дата</th>
                                    <th>Номер замовлення</th>
                                    <th>Дата постачання</th>
                                    <th>Замовлення на суму</th>
                                    <th>Отримано на суму</th>
                                    <th>Не отримано на суму</th>
                                </tr>
                            </thead>
                            <tbody>';

        $totalOrderSum = 0;
        $totalDeliverySum = 0;
        $totalNotDeliveredSum = 0;

        foreach ($orders as $row) {
            $orderSum = $row['order_sum'];
            $deliverySum = $row['delivery_sum'] !== null ? $row['delivery_sum'] : 0;
            $notDeliveredSum = $orderSum - $deliverySum;

            $html .= "<tr>
                        <td>" . $row['supplier_name'] . "</td>
                        <td>" . $row['order_date'] . "</td>
                        <td>" . $row['order_number'] . "</td>
                        <td>" . ($row['delivery_date'] !== null ? $row['delivery_date'] : '-') . "</td>
                        <td>" . number_format($orderSum, 2) . "</td>
                        <td>" . number_format($deliverySum, 2) . "</td>
                        <td>" . number_format($notDeliveredSum, 2) . "</td>
                      </tr>";

            $totalOrderSum += $orderSum;
            $totalDeliverySum += $deliverySum;
            $totalNotDeliveredSum += $notDeliveredSum;
        }

        $html .= '</tbody>
                </table>
                <p><strong>Разом по замовленням:</strong> ' . number_format($totalOrderSum, 2) . '</p>
                <p><strong>Разом отримано:</strong> ' . number_format($totalDeliverySum, 2) . '</p>
                <p><strong>Разом не отримано:</strong> ' . number_format($totalNotDeliveredSum, 2) . '</p>
            </body>
        </html>';

        return $html;
    }
    public function createOrder($number, $data, $counterparties, $warehouse, $selectedProducts) {
        if (!empty($selectedProducts)) {
            $products = array();
            $totalSum = 0;

            foreach ($selectedProducts as $product) {
                $productId = $product['id'];
                $quantity = $product['quantity'];
                $price = $product['price'];

                $products[] = array(
                    'id' => $productId,
                    'quantity' => $quantity,
                    'price' => $price
                );

                $totalSum += $quantity * $price;
            }

            $productsJson = json_encode($products);

            // Use prepared statements to prevent SQL injection
            $stmt = mysqli_prepare($this->con, "INSERT INTO order_to_supplier (number, date, id_counterparties, id_warehouse, products, sum) VALUES (?, ?, ?, ?, ?, ?)");
            mysqli_stmt_bind_param($stmt, "ississ", $number, $data, $counterparties, $warehouse, $productsJson, $totalSum);

            if (mysqli_stmt_execute($stmt)) {
                return true;
            } else {
                return false;
            }

            mysqli_stmt_close($stmt);
        } else {
            return false;
        }
    }
}
?>