<?php
class Counterparties
{
    private $con;

    public function __construct($connection)
    {
        $this->con = $connection;
    }

    public function getAllCounterparties()
    {
        $query = "SELECT * FROM legal_entity";
        return mysqli_query($this->con, $query);
    }

    public function getCounterpartyById($id)
    {
        $query = "SELECT * FROM legal_entity WHERE id = '$id'";
        $result = mysqli_query($this->con, $query);
        return mysqli_fetch_assoc($result);
    }

    public function updateCounterparty($id, $fullName, $mobilePhone, $email, $other, $note, $country, $registerCode, $payerCode, $legalAddress, $postalAddress, $locationAddress)
    {
        $query = "UPDATE legal_entity SET full_name = '$fullName', mobile_phone = '$mobilePhone', email = '$email', other = '$other', note = '$note', country = '$country', register_code = '$registerCode', payer_code = '$payerCode', legal_address = '$legalAddress', postal_address = '$postalAddress', location_address = '$locationAddress' WHERE id = '$id'";
        return mysqli_query($this->con, $query);
    }

    public function deleteCounterparty($id)
    {
        $query = "DELETE FROM legal_entity WHERE id = '$id'";
        return mysqli_query($this->con, $query);
    }

    public function createCounterparty($fullName, $mobilePhone, $email, $other, $note, $person, $country, $registerCode, $payerCode, $legalAddress, $postalAddress, $locationAddress, $iban, $bank)
    {
        $query = "INSERT INTO legal_entity (full_name, mobile_phone, email, other, note, persona, country, register_code, payer_code, legal_address, postal_address, location_address, iban, bank) VALUES ('$fullName', '$mobilePhone', '$email', '$other', '$note', '$person', '$country', '$registerCode', '$payerCode', '$legalAddress', '$postalAddress', '$locationAddress', '$iban', '$bank')";
        return mysqli_query($this->con, $query);
    }

    public function addPriceListItem($counterpartyId, $productName, $unit, $price)
    {
        global $con;

        // Перевірка на існування в базі даних
        $checkQuery = "SELECT COUNT(*) FROM price_list WHERE name_product = '$productName' AND units = '$unit' AND id_counterparties = '$counterpartyId'";
        $result = mysqli_query($con, $checkQuery);
        $count = mysqli_fetch_row($result)[0];

        if ($count == 0) {
            // Додавання в базу даних
            $insertQuery = "INSERT INTO price_list (name_product, price, units, id_counterparties) VALUES ('$productName', '$price', '$unit', '$counterpartyId')";
            mysqli_query($con, $insertQuery);
        } else {
            // Запис повідомлення про наявність даних у системі
            $_SESSION['duplicate_error'][] = "Товар '$productName' з ціною $price і одиницею виміру '$unit' вже наявний у системі для цього постачальника.";
        }
    }

    public function getPriceList($counterpartyId)
    {
        global $con;

        $query = "SELECT pl.id, pl.name_product, pl.price, pl.units, le.full_name AS counterparty_name
                  FROM price_list pl
                  JOIN legal_entity le ON pl.id_counterparties = le.id
                  WHERE pl.id_counterparties = '$counterpartyId'";

        $result = mysqli_query($con, $query);
        $priceList = array();

        while ($row = mysqli_fetch_assoc($result)) {
            $priceList[] = $row;
        }

        return $priceList;
    }
}
?>