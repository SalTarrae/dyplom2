<?php
class Product {
    private $con;

    public function __construct($con) {
        $this->con = $con;
    }

    public function deleteProduct($id) {
        mysqli_query($this->con, "DELETE FROM products WHERE id = '$id'");
    }

    public function getAllProductsRaw() {
        $query = mysqli_query($this->con, "SELECT * FROM products");
        return $query;
    }

    public function getProducts() {
        $query = mysqli_query($this->con, "SELECT products.*, category.categoryName, subcategory.subcategory, trade_mark.trade_name
                                            FROM products
                                            JOIN category ON category.id = products.category
                                            JOIN subcategory ON subcategory.id = products.subCategory
                                            JOIN trade_mark ON trade_mark.id = products.trade_mark");
        return $query;
    }
    public function insertProduct($category, $subcat, $articul, $productname, $trade_mark, $units, $productdescription) {
        $stmt = mysqli_prepare($this->con, "INSERT INTO products (category, subCategory, article, productName, productDescription, trade_mark, units_id) VALUES (?, ?, ?, ?, ?, ?, ?)");
        mysqli_stmt_bind_param($stmt, "iissisi", $category, $subcat, $articul, $productname, $productdescription, $trade_mark, $units);
        $result = mysqli_stmt_execute($stmt);
        mysqli_stmt_close($stmt);
        return $result;
    }
    public function getMaxProductId() {
        $query = mysqli_query($this->con, "SELECT MAX(id) as pid FROM products");
        $result = mysqli_fetch_array($query);
        return $result['pid'];
    }
    public function getProductsWithDiscounts() {
        $query = mysqli_query($this->con, "SELECT p.id, p.productName
                                            FROM products p
                                            INNER JOIN stocks s ON p.id = s.id_product
                                            WHERE s.id_warehouse = (SELECT id FROM warehouse WHERE name = 'Магазин')
                                            GROUP BY p.id");
        return $query;
    }
    public function updateCategory($id, $categoryName) {
        $sql = "UPDATE category SET categoryName='$categoryName' WHERE id='$id'";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    public function getProductById($productId) {
        $query = mysqli_query($this->con, "SELECT * FROM products WHERE id = '$productId'");
        return mysqli_fetch_assoc($query);
    }

    public function getUnitName($unitsId) {
        $unitsQuery = "SELECT name FROM units WHERE id = '$unitsId'";
        $unitsResult = mysqli_query($this->con, $unitsQuery);
        $unitsRow = mysqli_fetch_assoc($unitsResult);
        return $unitsRow['name'];
    }

    public function getProductName($productId) {
        $query = "SELECT productName FROM products WHERE id = '$productId'";
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['productName'];
    }
    public function createCategory($categoryName) {
        $sql = mysqli_query($this->con, "INSERT INTO category (categoryName) VALUES ('$categoryName')");
        return $sql;
    }

    public function deleteCategory($id) {
        mysqli_query($this->con, "DELETE FROM category WHERE id = '$id'");
    }

    public function getAllCategories() {
        $query = mysqli_query($this->con, "SELECT * FROM category");
        return $query;
    }
    public function createSubcategory($categoryId, $subcategory) {
        $sql = mysqli_query($this->con, "INSERT INTO subcategory (categoryid, subcategory) VALUES ('$categoryId', '$subcategory')");
        return $sql;
    }

    public function deleteSubcategory($id) {
        mysqli_query($this->con, "DELETE FROM subcategory WHERE id = '$id'");
    }

    public function getAllSubcategories() {
        $query = mysqli_query($this->con, "SELECT subcategory.id, category.categoryName, subcategory.subcategory 
                                            FROM subcategory 
                                            JOIN category ON category.id = subcategory.categoryid");
        return $query;
    }
    public function updateSubcategory($subcategoryId, $subcategoryName)
    {
        $sql = "UPDATE subcategory SET subcategory = '$subcategoryName' WHERE id = '$subcategoryId'";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }
    public function createUnit($name) {
        $sql = mysqli_query($this->con, "INSERT INTO units(name) VALUES('$name')");
        return $sql;
    }

    public function deleteUnit($id) {
        $sql = "DELETE FROM units WHERE id = '$id'";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    public function updateUnit($id, $name) {
        $sql = "UPDATE units SET name='$name' WHERE id='$id'";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    public function getAllUnits() {
        $query = mysqli_query($this->con, "SELECT * FROM units");
        return $query;
    }
    public function getSubcategoriesByCategoryId($categoryId) {
        $subcategoryQuery = mysqli_query($this->con, "SELECT * FROM subcategory WHERE categoryid = '$categoryId'");
        return $subcategoryQuery;
    }
    public function getProductsWithExpiryDate($expiry) {
        if ($expiry === 'expired') {
            $query = mysqli_query($this->con, "
                SELECT p.productName, c.categoryName, w.name AS warehouseName, s.data_expiry
                FROM stocks s
                JOIN products p ON s.id_product = p.id
                JOIN category c ON p.category = c.id
                JOIN warehouse w ON s.id_warehouse = w.id
                WHERE s.data_expiry < CURDATE()
            ");
        } else {
            $expiryDate = $expiry === 'today' ? date('Y-m-d') : date('Y-m-d', strtotime('+1 day'));
            $query = mysqli_query($this->con, "
                SELECT p.productName, c.categoryName, w.name AS warehouseName, s.data_expiry
                FROM stocks s
                JOIN products p ON s.id_product = p.id
                JOIN category c ON p.category = c.id
                JOIN warehouse w ON s.id_warehouse = w.id
                WHERE s.data_expiry = '$expiryDate'
            ");
        }
        return $query;
    }

    public function createTradeMark($trade_name, $description, $country) {
        $sql = mysqli_query($this->con, "INSERT INTO trade_mark (trade_name, description, country) VALUES ('$trade_name', '$description', '$country')");
        return $sql;
    }

    public function deleteTradeMark($id) {
        $sql = "DELETE FROM trade_mark WHERE id = '$id'";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    public function updateTradeMark($id, $trade_name, $description, $country) {
        $sql = "UPDATE trade_mark SET trade_name='$trade_name', description='$description', country='$country' WHERE id='$id'";
        $result = mysqli_query($this->con, $sql);
        return $result;
    }

    public function getAllTradeMarks() {
        $query = mysqli_query($this->con, "SELECT * FROM trade_mark");
        return $query;
    }
    public function updateProduct($pid, $category, $subcat, $articul, $productname, $trade_mark, $units, $productdescription) {
        $stmt = mysqli_prepare($this->con, "UPDATE products SET 
                                        category = ?,
                                        subCategory = ?,
                                        article = ?,
                                        productName = ?,
                                        productDescription = ?,
                                        trade_mark = ?,
                                        units_id = ?
                                        WHERE id = ?");

        mysqli_stmt_bind_param($stmt, "iisssiis", $category, $subcat, $articul, $productname, $productdescription, $trade_mark, $units, $pid);

        $result = mysqli_stmt_execute($stmt);

        mysqli_stmt_close($stmt);

        return $result;
    }

}
?>