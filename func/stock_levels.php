<?php
class StockLevels {
    private $con;

    public function __construct($con) {
        $this->con = $con;
    }

    public function createStockLevel($idProduct, $quantity, $idWarehouse) {
        $checkQuery = mysqli_query($this->con, "SELECT * FROM stock_levels WHERE id_product = '$idProduct' AND id_warehouse = '$idWarehouse'");
        if (mysqli_num_rows($checkQuery) > 0) {
            return false;
        } else {
            $createQuery = mysqli_query($this->con, "INSERT INTO stock_levels (id_product, quantity, id_warehouse) VALUES ('$idProduct', '$quantity', '$idWarehouse')");
            return $createQuery;
        }
    }

    public function updateStockLevel($stockLevelId, $idProduct, $quantity, $idWarehouse) {
        $updateQuery = mysqli_query($this->con, "UPDATE stock_levels SET id_product = '$idProduct', quantity = '$quantity', id_warehouse = '$idWarehouse' WHERE id = '$stockLevelId'");
        return $updateQuery;
    }

    public function deleteStockLevel($id) {
        mysqli_query($this->con, "DELETE FROM stock_levels WHERE id = '$id'");
    }

    public function getStockLevels() {
        $query = mysqli_query($this->con, "SELECT stock_levels.*, products.productName, units.name AS unit_name, warehouse.name 
                                             FROM stock_levels
                                             JOIN products ON products.id = stock_levels.id_product  
                                             JOIN units ON units.id = products.units_id
                                             JOIN warehouse ON warehouse.id = stock_levels.id_warehouse");
        return $query;
    }
}
?>
