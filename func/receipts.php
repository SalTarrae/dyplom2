<?php

require_once 'price.php';

class Receipts
{
    private $con;
    private $price;

    public function __construct($connection)
    {
        $this->con = $connection;
        $this->price = new Price($connection);
    }

    public function addReceipt($date, $orderId, $warehouseId, $counterpartyId, $products)
    {
        $number = "GR" . rand(10000, 99999);
        $totalSum = 0;

        foreach ($products as $product) {
            $totalSum += $product['quantity'] * $product['price'];
        }

        $productsJson = json_encode($products);

        $query = "INSERT INTO goods_receipt (id_order, number, date, products, sum, id_warehouse)
                  VALUES ('$orderId', '$number', '$date', '$productsJson', '$totalSum', '$warehouseId')";

        if (mysqli_query($this->con, $query)) {
            $goodsReceiptId = mysqli_insert_id($this->con);

            foreach ($products as $product) {
                $this->updateStocks($product, $date, $warehouseId, $counterpartyId);
                $this->price->updatePrice($product['id'], $product['price']);
            }

            return $goodsReceiptId;
        } else {
            return false;
        }
    }

    private function updateStocks($product, $date, $warehouseId, $counterpartyId)
    {
        $productId = $product['id'];
        $quantity = $product['quantity'];
        $price = $product['price'];
        $expiryDate = $product['expiry_date'];

        $query = "INSERT INTO stocks (id_product, quantity, id_warehouse, id_counterparties, date_of_receipt, data_expiry, purchase_price)
                  VALUES ('$productId', '$quantity', '$warehouseId', '$counterpartyId', '$date', '$expiryDate', '$price')";

        mysqli_query($this->con, $query);
    }

    public function getReceipts()
    {
        $query = "SELECT goods_receipt.*, order_to_supplier.number as order_number, warehouse.name as warehouse_name
                  FROM goods_receipt
                  JOIN order_to_supplier ON order_to_supplier.id = goods_receipt.id_order
                  JOIN warehouse ON warehouse.id = goods_receipt.id_warehouse";

        return mysqli_query($this->con, $query);
    }

    public function deleteReceipt($id)
    {
        $query = "DELETE FROM goods_receipt WHERE id = '$id'";
        return mysqli_query($this->con, $query);
    }

    public function getProductName($productId)
    {
        $query = "SELECT productName FROM products WHERE id = '$productId'";
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['productName'];
    }
    public function getReceiptDetails($receiptId)
    {
        $query = "SELECT goods_receipt.*, order_to_supplier.id_counterparties, order_to_supplier.sum, legal_entity.full_name as supplier_name, warehouse.address as warehouse_address
                  FROM goods_receipt 
                  JOIN order_to_supplier ON goods_receipt.id_order = order_to_supplier.id
                  JOIN legal_entity ON order_to_supplier.id_counterparties = legal_entity.id
                  JOIN warehouse ON goods_receipt.id_warehouse = warehouse.id
                  WHERE goods_receipt.id = $receiptId";

        $result = mysqli_query($this->con, $query);
        return mysqli_fetch_assoc($result);
    }
    public function roundPrice($price)
    {
        $fractional = $price - floor($price);

        if ($fractional < 0.2) {
            $roundedPrice = floor($price) + 0.2;
        } elseif ($fractional < 0.25) {
            $roundedPrice = floor($price) + 0.25;
        } elseif ($fractional < 0.5) {
            $roundedPrice = floor($price) + 0.5;
        } elseif ($fractional < 0.75) {
            $roundedPrice = floor($price) + 0.75;
        } elseif ($fractional < 0.99) {
            $roundedPrice = floor($price) + 0.99;
        } else {
            $roundedPrice = ceil($price);
        }

        return $roundedPrice;
    }

}

?>
