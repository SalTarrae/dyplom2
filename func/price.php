<?php
class Price
{
    private $con;

    public function __construct($con)
    {
        $this->con = $con;
    }

    public function getOldPrice($productId)
    {
        $query = "SELECT margin_price, pdv_price FROM price WHERE id_product = $productId";
        $result = mysqli_query($this->con, $query);
        $priceData = mysqli_fetch_assoc($result);
        return $priceData;
    }

    public function createDiscount($startDate, $endDate, $percentage, $productId)
    {
        // Отримати стару ціну з таблиці price для вибраного продукту
        $priceData = $this->getOldPrice($productId);
        $oldPrice = $priceData['margin_price'];
        $pdvPrice = $priceData['pdv_price'];

        // Обчислити нову ціну зі знижкою та ПДВ
        $newPrice = $oldPrice * (1 - $percentage / 100) * 1.2;

        // Вставити дані в таблицю discounts
        $insertQuery = "INSERT INTO discounts (old_price, percentage, new_price, date_start, date_finish, id_product) VALUES ($oldPrice, $percentage, $newPrice, '$startDate', '$endDate', $productId)";
        mysqli_query($this->con, $insertQuery);
    }
    public function isDiscountExists($productId)
    {
        $query = "SELECT COUNT(*) AS count FROM discounts WHERE id_product = $productId AND CURDATE() BETWEEN date_start AND date_finish";
        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);
        return $row['count'] > 0;
    }
    public function getAllDiscounts()
    {
        $query = "SELECT p.productName, d.old_price, d.new_price, d.percentage, d.date_start, d.date_finish
              FROM discounts d
              INNER JOIN products p ON d.id_product = p.id
              WHERE CURDATE() BETWEEN d.date_start AND d.date_finish";
        $result = mysqli_query($this->con, $query);
        $discounts = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $discounts[] = $row;
        }
        return $discounts;
    }

    public function getProductPriceData($productId)
    {
        $query = "SELECT pr.pdv_price AS price, pr.margin_price, d.new_price AS discount_price, d.percentage
                  FROM products p
                  INNER JOIN price pr ON p.id = pr.id_product
                  LEFT JOIN discounts d ON p.id = d.id_product AND CURDATE() BETWEEN d.date_start AND d.date_finish
                  WHERE p.id = '$productId'";
        $result = mysqli_query($this->con, $query);
        $priceData = mysqli_fetch_assoc($result);
        return $priceData;
    }

    public function getProductPrice($productId)
    {
        $priceData = $this->getProductPriceData($productId);
        return $priceData['discount_price'] !== null ? $priceData['discount_price'] : $priceData['price'];
    }

    public function deleteExpiredDiscounts()
    {
        $currentDate = date('Y-m-d');
        mysqli_query($this->con, "DELETE FROM discounts WHERE date_finish < '$currentDate'");
    }

    public function getProductCostPrice($productId)
    {
        $costQuery = "SELECT cost_price, margin_price FROM price WHERE id_product = '$productId'";
        $costResult = mysqli_query($this->con, $costQuery);
        $costRow = mysqli_fetch_assoc($costResult);
        return $costRow;
    }
    public function checkPriceExistence($productName, $unit, $counterpartyId)
    {
        $checkQuery = "SELECT COUNT(*) FROM price_list WHERE name_product = '$productName' AND units = '$unit' AND id_counterparties = '$counterpartyId'";
        $result = mysqli_query($this->con, $checkQuery);
        $count = mysqli_fetch_row($result)[0];
        return $count > 0;
    }

    public function addPriceListItem($productName, $price, $unit, $counterpartyId)
    {
        $insertQuery = "INSERT INTO price_list (name_product, price, units, id_counterparties) VALUES ('$productName', '$price', '$unit', '$counterpartyId')";
        mysqli_query($this->con, $insertQuery);
    }
    public function getPriceListByCounterparty($counterpartyId)
    {
        $query = "SELECT pl.id, pl.name_product, pl.price, pl.units, le.full_name AS counterparty_name
              FROM price_list pl
              JOIN legal_entity le ON pl.id_counterparties = le.id
              WHERE pl.id_counterparties = '$counterpartyId'";

        $result = mysqli_query($this->con, $query);
        $priceList = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $priceList[] = $row;
        }
        return $priceList;
    }

    public function processPriceListData($data, $counterpartyId)
    {
        $duplicateErrors = array();
        foreach ($data as $item) {
            $productName = mysqli_real_escape_string($this->con, $item['product_name']);
            $unit = mysqli_real_escape_string($this->con, $item['unit']);
            $price = mysqli_real_escape_string($this->con, $item['price']);

            if (!$this->checkPriceExistence($productName, $unit, $counterpartyId)) {
                $this->addPriceListItem($productName, $price, $unit, $counterpartyId);
            } else {
                $duplicateErrors[] = "Товар '$productName' з ціною $price і одиницею виміру '$unit' вже наявний у системі для цього постачальника.";
            }
        }
        return $duplicateErrors;
    }
    public function updatePrice($productId, $costPrice) {
        $marginPrice = $this->calculateMarginPrice($costPrice);
        $pdvPrice = $this->calculatePdvPrice($marginPrice);

        $query = "INSERT INTO price (id_product, cost_price, margin_price, pdv_price)
                  VALUES ('$productId', '$costPrice', '$marginPrice', '$pdvPrice')
                  ON DUPLICATE KEY UPDATE
                  cost_price = VALUES(cost_price),
                  margin_price = VALUES(margin_price),
                  pdv_price = VALUES(pdv_price)";

        mysqli_query($this->con, $query);
    }

    private function calculateMarginPrice($costPrice, $margin = 0.2) {
        return $costPrice * (1 + $margin);
    }

    private function calculatePdvPrice($marginPrice, $pdv = 0.2) {
        return $marginPrice * (1 + $pdv);
    }
}
?>