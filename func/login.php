<?php
session_start();
error_reporting(0);
include("../include/config.php");
include("./include/config.php");

class User
{
    private $con;

    public function __construct($con)
    {
        if (!$con) {
            die("Database connection is required.");
        }
        $this->con = $con;
    }

    public function login($username, $password)
    {
        $password = md5($password);
        $query = mysqli_query($this->con, "SELECT users.*, role.name AS role_name
                                   FROM users
                                   JOIN role ON users.role = role.id
                                   WHERE users.username = '$username' AND users.password = '$password'");


        $user = mysqli_fetch_array($query);

        if ($user) {
            $_SESSION['user_name'] = $user['username'];
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['user_role'] = $user['role_name'];
            return true;
        } else {
            return false;
        }
    }

    public function isLoggedIn()
    {
        return isset($_SESSION['user_name']);
    }

    public function getRole()
    {
        return $_SESSION['user_role'];
    }

    public function hasRole($role)
    {
        return $_SESSION['user_role'] === $role;
    }

    public function logout()
    {
        $_SESSION['login'] = "";
        $ldate = date('d-m-Y h:i:s A', time());

        session_unset();
        $_SESSION['errmsg'] = "You have successfully logged out";

        $this->redirectToIndex();
    }

    private function redirectToIndex()
    {
        header("Location: index.php");
        exit();
    }

    public function getUserById($userId)
    {
        $query = mysqli_query($this->con, "SELECT users.*, role.name AS role_name 
                                   FROM users 
                                   JOIN role ON users.role = role.id
                                   WHERE users.id = $userId");
        return mysqli_fetch_array($query);
    }
    public function getUserInfo($userId) {
        $sql = "SELECT users.name, users.email, role.name AS role_name 
                FROM users 
                JOIN role ON users.role = role.id
                WHERE users.id = $userId";
        $result = mysqli_query($this->con, $sql);
        return mysqli_fetch_assoc($result);
    }

    public function updateProfile($userId, $name, $email) {
        $sql = "UPDATE users SET name = '$name', email = '$email' WHERE id = $userId";
        return mysqli_query($this->con, $sql);
    }
    public function getAllUsers($currentUserId)
    {
        $query = mysqli_query($this->con, "SELECT users.id, users.name, users.email, role.name AS role_name 
                                   FROM users 
                                   JOIN role ON users.role = role.id
                                   WHERE users.id != $currentUserId");
        return $query;
    }

    public function deleteUser($userId)
    {
        $query = mysqli_query($this->con, "DELETE FROM users WHERE id = $userId");
        return $query;
    }

    public function updateUserRole($userId, $newRole)
    {
        $query = mysqli_query($this->con, "UPDATE users SET role = $newRole WHERE id = $userId");
        return $query;
    }

    public function addUser($email, $role, $token)
    {
        $query = mysqli_query($this->con, "INSERT INTO users (email, role, token) VALUES ('$email', '$role', '$token')");
        return $query;
    }

    public function checkExistingUser($email)
    {
        $checkQuery = mysqli_query($this->con, "SELECT * FROM users WHERE email = '$email'");
        return mysqli_num_rows($checkQuery) > 0;
    }
    public function getAllRoles() {
        $query = mysqli_query($this->con, "SELECT * FROM role");
        return $query;
    }
    public function getUserByToken($token)
    {
        $query = mysqli_query($this->con, "SELECT * FROM users WHERE token = '$token'");
        return mysqli_fetch_assoc($query);
    }

    public function updateUserRegistration($email, $name, $username, $password)
    {
        $hashedPassword = md5($password);
        $query = mysqli_query($this->con, "UPDATE users SET name = '$name', username = '$username', password = '$hashedPassword', token = NULL WHERE email = '$email'");
        return $query;
    }
    public function storeResetToken($email, $token)
    {
        $sql = "UPDATE users SET reset_token = '$token', reset_token_created_at = NOW() WHERE email = '$email'";
        return mysqli_query($this->con, $sql);
    }
    public function getUserByResetToken($token)
    {
        $sql = "SELECT * FROM users WHERE reset_token = '$token' AND reset_token_created_at >= DATE_SUB(NOW(), INTERVAL 24 HOUR)";
        $result = mysqli_query($this->con, $sql);
        return mysqli_fetch_assoc($result);
    }

    public function updatePassword($email, $newPassword)
    {
        $hashedPassword = md5($newPassword);
        $sql = "UPDATE users SET password = '$hashedPassword', reset_token = NULL, reset_token_created_at = NULL WHERE email = '$email'";
        return mysqli_query($this->con, $sql);
    }
}

$user = new User($con);

if (isset($_POST['submitLogin'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if ($user->login($username, $password)) {
        header("Location: dashboard.php");
        exit();
    } else {
        $_SESSION['errmsg'] = "Invalid username or password";
        header("Location: index.php");
        exit();
    }
}

if (isset($_GET['logout'])) {
    $user->logout();
}
?>