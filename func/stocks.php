<?php
class Stocks {
    private $con;

    public function __construct($con) {
        $this->con = $con;
    }

    public function getAllWarehouses() {
        $query = "SELECT * FROM warehouse";
        return mysqli_query($this->con, $query);
    }

    public function getWarehouseById($id) {
        $query = "SELECT * FROM warehouse WHERE id = '$id'";
        $result = mysqli_query($this->con, $query);
        return mysqli_fetch_assoc($result);
    }

    public function createWriteOff($date, $warehouseId, $reason, $products) {
        // Початок транзакції
        mysqli_begin_transaction($this->con);

        try {
            // Використовуємо prepared statements для запобігання SQL-ін'єкцій
            $insertQuery = mysqli_prepare($this->con, "INSERT INTO write_off (date, id_warehouse, reason, products) VALUES (?, ?, ?, ?)");
            $productsJson = json_encode($products, JSON_UNESCAPED_UNICODE);
            mysqli_stmt_bind_param($insertQuery, "siss", $date, $warehouseId, $reason, $productsJson);

            if (mysqli_stmt_execute($insertQuery)) {
                $writeOffId = mysqli_insert_id($this->con);

                // Update stock for each product
                foreach ($products as $product) {
                    $productId = $product['id'];
                    $quantity = $product['quantity'];

                    $updateQuery = mysqli_prepare($this->con, "UPDATE stocks SET quantity = quantity - ? WHERE id_product = ?");
                    mysqli_stmt_bind_param($updateQuery, "ii", $quantity, $productId);

                    if (mysqli_stmt_execute($updateQuery)) {
                        // Перевірка, чи кількість товару стала рівною 0
                        $checkQuantityQuery = mysqli_prepare($this->con, "SELECT quantity FROM stocks WHERE id_product = ?");
                        mysqli_stmt_bind_param($checkQuantityQuery, "i", $productId);
                        mysqli_stmt_execute($checkQuantityQuery);
                        mysqli_stmt_bind_result($checkQuantityQuery, $remainingQuantity);
                        mysqli_stmt_fetch($checkQuantityQuery);
                        mysqli_stmt_close($checkQuantityQuery);

                        if ($remainingQuantity === 0) {
                            // Видалення запису про товар, якщо кількість стала рівною 0
                            $deleteQuery = mysqli_prepare($this->con, "DELETE FROM stocks WHERE id_product = ?");
                            mysqli_stmt_bind_param($deleteQuery, "i", $productId);
                            mysqli_stmt_execute($deleteQuery);
                            mysqli_stmt_close($deleteQuery);
                        }
                    } else {
                        throw new Exception("Помилка при оновленні залишку для товару з ID {$productId}: " . mysqli_error($this->con));
                    }
                    mysqli_stmt_close($updateQuery);
                }

                // Коміт транзакції
                mysqli_commit($this->con);

                return true;
            } else {
                throw new Exception("Помилка при вставці даних у таблицю write_off: " . mysqli_error($this->con));
            }

            mysqli_stmt_close($insertQuery);
        } catch (Exception $e) {
            // Відкат транзакції в разі помилки
            mysqli_rollback($this->con);
            throw $e;
        }
    }
    public function getWriteOffById($id) {
        $query = "SELECT write_off.*, warehouse.name AS warehouse_name 
                  FROM write_off 
                  JOIN warehouse ON write_off.id_warehouse = warehouse.id
                  WHERE write_off.id = $id";
        $result = mysqli_query($this->con, $query);
        return mysqli_fetch_assoc($result);
    }
    public function deleteWriteOff($id) {
        // Перевіряємо, чи запис списання не пов'язаний з інвентаризацією
        $checkQuery = mysqli_prepare($this->con, "SELECT COUNT(*) FROM inventory WHERE id_write_off = ?");
        mysqli_stmt_bind_param($checkQuery, "i", $id);
        mysqli_stmt_execute($checkQuery);
        mysqli_stmt_bind_result($checkQuery, $count);
        mysqli_stmt_fetch($checkQuery);
        mysqli_stmt_close($checkQuery);

        if ($count > 0) {
            return false;
        }

        $deleteQuery = mysqli_prepare($this->con, "DELETE FROM write_off WHERE id = ?");
        mysqli_stmt_bind_param($deleteQuery, "i", $id);
        return mysqli_stmt_execute($deleteQuery);
    }
    public function getAllWriteOffs() {
        $query = "SELECT write_off.*, warehouse.name
                  FROM write_off
                  JOIN warehouse ON write_off.id_warehouse = warehouse.id";
        return mysqli_query($this->con, $query);
    }

    public function getStockProducts($warehouseId, $categories = null) {
        $query = "SELECT p.id, p.productName, s.quantity, s.id as stock_id, s.purchase_price, s.date_of_receipt, u.name AS unitName
              FROM stocks s
              JOIN products p ON s.id_product = p.id
              JOIN units u ON p.units_id = u.id
              WHERE s.id_warehouse = ?";

        if (!empty($categories)) {
            $query .= " AND p.category IN ($categories)";
        }

        $query .= " ORDER BY p.productName";

        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'i', $warehouseId);
        mysqli_stmt_execute($stmt);
        return mysqli_stmt_get_result($stmt);
    }


    public function updateStockQuantity($productId, $quantity) {
        $updateQuery = mysqli_prepare($this->con, "UPDATE stocks SET quantity = quantity - ? WHERE id_product = ?");
        mysqli_stmt_bind_param($updateQuery, "ii", $quantity, $productId);
        return mysqli_stmt_execute($updateQuery);
    }

    public function getStockQuantity($productId) {
        $checkQuantityQuery = mysqli_prepare($this->con, "SELECT quantity FROM stocks WHERE id_product = ?");
        mysqli_stmt_bind_param($checkQuantityQuery, "i", $productId);
        mysqli_stmt_execute($checkQuantityQuery);
        mysqli_stmt_bind_result($checkQuantityQuery, $remainingQuantity);
        mysqli_stmt_fetch($checkQuantityQuery);
        mysqli_stmt_close($checkQuantityQuery);
        return $remainingQuantity;
    }
    public function getAllSurpluses() {
        $query = "SELECT surpluses.*, warehouse.name
                  FROM surpluses
                  JOIN warehouse ON surpluses.id_warehouse = warehouse.id";
        return mysqli_query($this->con, $query);
    }
    public function deleteStock($productId) {
        $deleteQuery = mysqli_prepare($this->con, "DELETE FROM stocks WHERE id_product = ?");
        mysqli_stmt_bind_param($deleteQuery, "i", $productId);
        return mysqli_stmt_execute($deleteQuery);
    }
    public function createSurplus($date, $warehouseId, $products) {
        // Початок транзакції
        mysqli_begin_transaction($this->con);

        try {
            // Використовуємо prepared statements для запобігання SQL-ін'єкцій
            $insertQuery = mysqli_prepare($this->con, "INSERT INTO surpluses (date, id_warehouse, products) VALUES (?, ?, ?)");
            mysqli_stmt_bind_param($insertQuery, "sis", $date, $warehouseId, json_encode($products, JSON_UNESCAPED_UNICODE));

            if (mysqli_stmt_execute($insertQuery)) {
                // Update stock for each product
                foreach ($products as $product) {
                    $productId = $product['id'];
                    $quantity = $product['quantity'];
                    $price = $product['price'];
                    $expiryDate = $product['expiryDate'];

                    // Check if the product already exists in the stocks table
                    $checkStockQuery = mysqli_prepare($this->con, "SELECT id FROM stocks WHERE id_product = ? AND id_warehouse = ?");
                    mysqli_stmt_bind_param($checkStockQuery, "ii", $productId, $warehouseId);
                    mysqli_stmt_execute($checkStockQuery);
                    mysqli_stmt_store_result($checkStockQuery);
                    $stockExists = mysqli_stmt_num_rows($checkStockQuery) > 0;
                    mysqli_stmt_close($checkStockQuery);

                    if ($stockExists) {
                        // If the product exists, update the quantity
                        $updateQuery = mysqli_prepare($this->con, "UPDATE stocks SET quantity = quantity + ? WHERE id_product = ? AND id_warehouse = ?");
                        mysqli_stmt_bind_param($updateQuery, "iii", $quantity, $productId, $warehouseId);
                        mysqli_stmt_execute($updateQuery);
                        mysqli_stmt_close($updateQuery);
                    } else {
                        // If the product doesn't exist, insert a new record
                        $insertStockQuery = mysqli_prepare($this->con, "INSERT INTO stocks (id_product, quantity, date_of_receipt, data_expiry, id_warehouse) VALUES (?, ?, ?, ?, ?)");
                        mysqli_stmt_bind_param($insertStockQuery, "iissi", $productId, $quantity, $date, $expiryDate, $warehouseId);
                        mysqli_stmt_execute($insertStockQuery);
                        mysqli_stmt_close($insertStockQuery);
                    }
                }
                // Коміт транзакції
                mysqli_commit($this->con);

                return true;
            } else {
                throw new Exception("Error inserting data into surpluses table: " . mysqli_error($this->con));
            }

            mysqli_stmt_close($insertQuery);
        } catch (Exception $e) {
            // Відкат транзакції в разі помилки
            mysqli_rollback($this->con);
            throw $e;
        }
    }
    public function getAllInventories() {
        $query = "SELECT inventory.*, warehouse.name as warehouse_name
                  FROM inventory
                  JOIN warehouse ON warehouse.id = inventory.id_warehouse";
        return mysqli_query($this->con, $query);
    }

    public function createInventory($date, $products, $idWriteOff, $idSurplus, $idWarehouse) {
        $query = "INSERT INTO inventory (date, products, id_write_off, id_surpluses, id_warehouse) VALUES (?, ?, ?, ?, ?)";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'ssiii', $date, $products, $idWriteOff, $idSurplus, $idWarehouse);
        mysqli_stmt_execute($stmt);
        return mysqli_insert_id($this->con);
    }
    public function getHighestStockProduct() {
        $query = "SELECT stocks.*, products.productName 
                  FROM stocks
                  JOIN products ON stocks.id_product = products.id
                  ORDER BY stocks.quantity DESC
                  LIMIT 1";

        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);

        return $row ? $row['productName'] : "";
    }

    public function getLowestStockProduct() {
        $query = "SELECT stocks.*, products.productName 
                  FROM stocks
                  JOIN products ON stocks.id_product = products.id
                  ORDER BY stocks.quantity ASC
                  LIMIT 1";

        $result = mysqli_query($this->con, $query);
        $row = mysqli_fetch_assoc($result);

        return $row ? $row['productName'] : "";
    }
    public function getInventoryById($id) {
        $query = "SELECT * FROM inventory WHERE id = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        return mysqli_fetch_assoc($result);
    }

    public function updateInventory($id, $date, $products, $idWriteOff, $idSurplus, $idWarehouse) {
        $query = "UPDATE inventory SET date = ?, products = ?, id_write_off = ?, id_surpluses = ?, id_warehouse = ? WHERE id = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'ssiiii', $date, $products, $idWriteOff, $idSurplus, $idWarehouse, $id);
        return mysqli_stmt_execute($stmt);
    }

    public function deleteInventory($id) {
        $query = "DELETE FROM inventory WHERE id = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return mysqli_stmt_execute($stmt);
    }
    public function getStockAnalysis($warehouseId) {
        $query = "SELECT p.productName, p.article, SUM(s.quantity) AS total_quantity, MIN(s.purchase_price) AS min_price, SUM(s.quantity * s.purchase_price) AS cost
                  FROM stocks s
                  JOIN products p ON s.id_product = p.id";

        if ($warehouseId !== 'all') {
            $query .= " WHERE s.id_warehouse = $warehouseId";
        }

        $query .= " GROUP BY p.id";

        return mysqli_query($this->con, $query);
    }
    public function getStockById($stockId) {
        $query = "SELECT * FROM stocks WHERE id = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'i', $stockId);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);
        return mysqli_fetch_assoc($result);
    }

    public function updateStockWarehouse($stockId, $warehouseId) {
        $query = "UPDATE stocks SET id_warehouse = ? WHERE id = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'ii', $warehouseId, $stockId);
        return mysqli_stmt_execute($stmt);
    }

    public function insertStock($productId, $quantity, $purchasePrice, $idCounterparties, $dateOfReceipt, $dataExpiry, $warehouseId) {
        $query = "INSERT INTO stocks (id_product, quantity, purchase_price, id_counterparties, date_of_receipt, data_expiry, id_warehouse) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'iidsissi', $productId, $quantity, $purchasePrice, $idCounterparties, $dateOfReceipt, $dataExpiry, $warehouseId);
        return mysqli_stmt_execute($stmt);
    }



    public function getStockProductsByWarehouse($warehouseId) {
        $query = "SELECT s.id AS stockId, p.id, p.productName, s.quantity, s.date_of_receipt 
                  FROM stocks s
                  INNER JOIN products p ON s.id_product = p.id
                  WHERE s.id_warehouse = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'i', $warehouseId);
        mysqli_stmt_execute($stmt);
        $result = mysqli_stmt_get_result($stmt);

        $products = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $products[] = $row;
        }

        return $products;
    }
    public function createWarehouse($name, $address) {
        $query = "INSERT INTO warehouse (name, address) VALUES (?, ?)";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'ss', $name, $address);
        return mysqli_stmt_execute($stmt);
    }
    public function deleteWarehouse($id) {
        $query = "DELETE FROM warehouse WHERE id = ?";
        $stmt = mysqli_prepare($this->con, $query);
        mysqli_stmt_bind_param($stmt, 'i', $id);
        return mysqli_stmt_execute($stmt);
    }
}
?>
