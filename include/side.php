<div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
    <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
        <a href="dashboard.php" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
            <img src="images/logo.svg" alt="Logo" class="img-fluid" style="max-width: 100px;">
        </a>
        <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
            <li class="nav-item">
                <a href="dashboard.php" class="nav-link align-middle px-0 text-white">
                    <i class="bi bi-bar-chart-fill"></i> <span class="ms-1 d-none d-sm-inline">Dashboard</span>
                </a>
            </li>
            <li>
                <a href="#submenu3" data-toggle="collapse" class="nav-link px-0 align-middle text-white">
                    <i class="fs-4 bi-table"></i> <span class="ms-1 d-none d-sm-inline">Продажі</span>
                </a>
                <ul class="collapse nav flex-column ms-1" id="submenu3" data-parent="#menu">
                    <li class="w-100">
                        <a href="/sales/sales.php" class="nav-link px-0"><span class="d-none d-sm-inline text-muted">Продажі</span></a>
                    </li>
                    <?php if (!$user->hasRole('manager')) {?>
                    <li class="w-100">
                        <a href="/sales/discounts.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Знижки</span></a>
                    </li>
                        <?php
                    }
                    ?>
                    <li class="w-100">
                        <a href="/sales/sales-report.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Звіт по продажам</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#submenu2" data-toggle="collapse" class="nav-link px-0 align-middle text-white">
                    <i class="bi bi-bag"></i> <span class="ms-1 d-none d-sm-inline">Закупівлі</span>
                </a>
                <ul class="collapse nav flex-column ms-1" id="submenu2" data-parent="#menu">
                    <?php if (!$user->hasRole('manager')) {?>
                    <li>
                        <a href="/purchasing/order_to_supplier.php" class="nav-link px-0"><span class="d-none
                        d-sm-inline
                        text-muted">Замовлення постачальнику</span></a>
                    </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="/purchasing/all_goods_receipt.php" class="nav-link px-0"><span class="d-none d-sm-inline text-muted">Надходження товарів</span></a>
                    </li>
                    <li class="w-100">
                        <a href="/purchasing/supply_control.php" class="nav-link px-0"><span class="d-none d-sm-inline text-muted">Контроль поставок</span></a>
                    </li>
                    <?php if (!$user->hasRole('manager')) {?>
                    <li>
                        <a href="/purchasing/counterparties.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Постачальники</span></a>
                    </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="/purchasing/price-list.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Прайс постачальника</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#submenu1" data-toggle="collapse" class="nav-link px-0 align-middle text-white">
                    <i class="bi bi-archive"></i> <span class="ms-1 d-none d-sm-inline">Склад</span>
                </a>
                <ul class="collapse nav flex-column ms-1" id="submenu1" data-parent="#menu">
                    <?php if (!$user->hasRole('manager')) {?>
                    <li>
                        <a href="/warehouse/warehouse.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Склади</span></a>
                    </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="/warehouse/write-off.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Списання</span></a>
                    </li>
                    <li>
                        <a href="/warehouse/surpluses.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Оприбуткування надлишків</span></a>
                    </li>
                    <?php if (!$user->hasRole('manager')) {?>
                    <li>
                        <a href="/warehouse/all_inventory.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Інвентаризація</span></a>
                    </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="/warehouse/report_warehouse.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Аналіз запасів</span></a>
                    </li>
                    <?php if (!$user->hasRole('manager')) {?>
                    <li>
                        <a href="/warehouse/stock_levels.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Норми запасу</span></a>
                    </li>
                        <?php
                    }
                    ?>
                    <li>
                        <a href="/warehouse/movement.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Переміщення</span></a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#submenu4" data-toggle="collapse" class="nav-link px-0 align-middle text-white">
                    <i class="bi bi-basket3"></i> <span class="ms-1 d-none d-sm-inline">Товари</span>
                </a>
                <ul class="collapse nav flex-column ms-1" id="submenu4" data-parent="#menu">
                    <?php if (!$user->hasRole('manager')) {?>
                    <li class="w-100">
                        <a href="/products/manage-products.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Товари</span></a>
                    </li>
                    <li>
                        <a href="/products/category.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Категорії</span></a>
                    </li>
                    <li>
                        <a href="/products/subcategory.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Субкатегорії</span></a>
                    </li>
                    <li class="w-100">
                        <a href="/products/units.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Одиниці виміру</span></a>
                    </li>
                        <li class="w-100">
                            <a href="/products/brand.php" class="nav-link px-0"><span class="d-none d-sm-inline
                            text-muted">Торгова марка</span></a>
                        </li>
                        <?php
                    }
                    ?>
                    <li class="w-100">
                        <a href="/products/spoilage.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Товари з терміном продатності</span></a>
                    </li>
                </ul>
            </li>
            <?php if (!$user->hasRole('manager')) {?>
            <li>
                <a href="#submenu5" data-toggle="collapse" class="nav-link px-0 align-middle text-white">
                    <i class="bi bi-file-bar-graph-fill"></i>  <span class="ms-1 d-none d-sm-inline">Загальні</span>
                </a>
                <ul class="collapse nav flex-column ms-1" id="submenu5" data-parent="#menu">
                    <li>
                        <a href="/data/data_store.php" class="nav-link px-0"><span class="d-none d-sm-inline
                        text-muted">Дані</span></a>
                    </li>
                </ul>
            </li>
                <?php
            }
            ?>
        </ul>
        <hr>
        <div class="dropdown pb-4">
            <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-toggle="dropdown" aria-expanded="false">
                <img src="/images/user.png" alt="User Avatar" width="30" height="30" class="rounded-circle">
                <span class="d-none d-sm-inline mx-1"><?php echo $_SESSION['user_name']; ?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                <li><a class="dropdown-item" href="/users/user.php">Профіль</a></li>
                <?php if (!$user->hasRole('manager')) {?>
                <li><a class="dropdown-item" href="/users/users.php">Користувачі</a></li>
                    <?php
                }
                ?>
                <li>
                    <hr class="dropdown-divider">
                </li>
                <li><a class="dropdown-item" href="/users/logout.php">Вийти</a></li>
            </ul>
        </div>
    </div>
</div>