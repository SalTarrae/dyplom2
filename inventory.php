<?php
session_start();
require_once 'func/login.php';
include("include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
}
else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('Y-m-d H:i:s', time());

    if (isset($_POST['submit_inventory'])) {
        $date = $_POST['date'];
        $id_warehouse = $_POST['id_warehouse'];
        $products = json_decode($_POST['products'], true);

        $newProducts = [];
        foreach ($products as $product) {
            $productId = $product['product_id'];
            $accountingQuantity = $product['accounting_quantity'];
            $actualQuantity = $product['actual_quantity'];
            $purchasePrice = $product['purchase_price'];
            $dateOfReceipt = $product['date_of_receipt'];

            $newProduct = [
                'id' => $productId,
                'accounting_quantity' => $accountingQuantity,
                'actual_quantity' => $actualQuantity,
                'purchase_price' => $purchasePrice,
                'date_of_receipt' => $dateOfReceipt
            ];
            $newProducts[] = $newProduct;

            if ($actualQuantity < $accountingQuantity) {
                $writeOffQuantity = $accountingQuantity - $actualQuantity;
                $updateQuery = mysqli_query($con, "UPDATE stocks SET quantity = quantity - $writeOffQuantity WHERE id_product = $productId");
                $checkQuantityQuery = mysqli_query($con, "SELECT quantity FROM stocks WHERE id_product = $productId");
                $quantityRow = mysqli_fetch_array($checkQuantityQuery);
                $remainingQuantity = $quantityRow['quantity'];
                if ($remainingQuantity <= 0) {
                    $deleteQuery = mysqli_query($con, "DELETE FROM stocks WHERE id_product = $productId");
                }
            }
        }

        // Списання товарів
        $writeOffProducts = [];
        foreach ($products as $product) {
            $productId = $product['product_id'];
            $accountingQuantity = $product['accounting_quantity'];
            $actualQuantity = $product['actual_quantity'];
            $purchasePrice = $product['purchase_price'];

            if ($actualQuantity < $accountingQuantity) {
                $writeOffQuantity = $accountingQuantity - $actualQuantity;

                $writeOffProduct = [
                    'id' => $productId,
                    'quantity' => $writeOffQuantity,
                    'purchasePrice' => $purchasePrice
                ];
                $writeOffProducts[] = $writeOffProduct;
            }
        }

        $id_write_off = null;
        if (!empty($writeOffProducts)) {
            $insertWriteOffQuery = mysqli_query($con, "INSERT INTO write_off (date, reason, products, id_warehouse) VALUES ('$date', 'Інвентаризація', '" . json_encode($writeOffProducts) . "', '$id_warehouse')");
            if ($insertWriteOffQuery) {
                $id_write_off = mysqli_insert_id($con);
            }
        }

        // Оприбуткування надлишків
        $surplusProducts = [];
        foreach ($products as $product) {
            $productId = $product['product_id'];
            $accountingQuantity = $product['accounting_quantity'];
            $actualQuantity = $product['actual_quantity'];
            $purchasePrice = $product['purchase_price'];
            $dateOfReceipt = $product['date_of_receipt'];

            if ($actualQuantity > $accountingQuantity) {
                $surplusQuantity = $actualQuantity - $accountingQuantity;

                $surplusProduct = [
                    'id' => $productId,
                    'quantity' => $surplusQuantity,
                    'purchasePrice' => $purchasePrice,
                    'dateOfReceipt' => $dateOfReceipt
                ];
                $surplusProducts[] = $surplusProduct;

                // Збільшення кількості товарів у stocks
                $updateStocksQuery = mysqli_query($con, "UPDATE stocks SET quantity = quantity + $surplusQuantity WHERE id_product = $productId");
            }
        }

        $id_surplus = null;
        if (!empty($surplusProducts)) {
            $insertSurplusQuery = mysqli_query($con, "INSERT INTO surpluses (date, id_warehouse, products) VALUES ('$date', '$id_warehouse', '" . json_encode($surplusProducts) . "')");
            if ($insertSurplusQuery) {
                $id_surplus = mysqli_insert_id($con);
            }
        }

        if (!empty($newProducts)) {
            $id_write_off_value = $id_write_off !== null ? $id_write_off : 'NULL';
            $id_surplus_value = $id_surplus !== null ? $id_surplus : 'NULL';

            $insertQuery = mysqli_query($con, "INSERT INTO inventory (date, products, id_write_off, id_surpluses, id_warehouse) VALUES ('$date', '" . json_encode($newProducts) . "', $id_write_off_value, $id_surplus_value, '$id_warehouse')");
            if ($insertQuery) {
                $id_inventory = mysqli_insert_id($con);
                echo "Запис успішно створено! ID запису: " . $id_inventory;
            } else {
                echo "Помилка при створенні запису!";
            }
        } else {
            echo "Немає нових товарів для додавання.";
        }
        exit();
    }

    if (isset($_POST['get_inventory_products'])) {
        $id_warehouse = $_POST['id_warehouse'];
        $categories = implode(',', $_POST['categories']);

        $query = mysqli_query($con, "SELECT stocks.id, stocks.id_product, stocks.quantity, stocks.purchase_price, stocks.date_of_receipt, products.id as product_id, products.productName, products.category, units.name AS unitName
        FROM stocks
        JOIN products ON stocks.id_product = products.id
        JOIN units ON products.units_id = units.id
        WHERE stocks.id_warehouse = $id_warehouse AND products.category IN ($categories)
        ORDER BY products.productName");

        $previousProductId = null;
        $output = '';
        while ($row = mysqli_fetch_array($query)) {
            $productId = $row['product_id'];
            if ($productId !== $previousProductId) {
                if ($previousProductId !== null) {
                    $output .= '</tbody></table></div>';
                }
                $output .= '<div class="product-section" data-product-id="' . $productId . '"><h4>' . $row['productName'] . '</h4>';
                $output .= '<table class="table table-bordered">';
                $output .= '<thead><tr><th>Партія</th><th>Облікова кількість</th><th>Фактична кількість</th><th>Ціна</th><th>Сума</th><th>Од. виміру</th></tr></thead>';
                $output .= '<tbody>';
            }
            $sum = $row['quantity'] * $row['purchase_price'];
            $output .= '<tr data-id="' . $row['id'] . '" data-product-id="' . $row['product_id'] . '" data-purchase-price="' . $row['purchase_price'] . '" data-date-of-receipt="' . $row['date_of_receipt'] . '">';
            $output .= '<td>Партія ' . date('d.m.Y', strtotime($row['date_of_receipt'])) . '</td>';
            $output .= '<td class="accounting-quantity">' . $row['quantity'] . '</td>';
            $output .= '<td><input type="number" class="actual-quantity" min="0"></td>';
            $output .= '<td class="purchase-price">' . $row['purchase_price'] . '</td>';
            $output .= '<td>' . $sum . '</td>';
            $output .= '<td>' . $row['unitName'] . '</td>';
            $output .= '</tr>';
            $previousProductId = $productId;
        }
        if ($previousProductId !== null) {
            $output .= '</tbody></table></div>';
        }
        echo $output;
        exit();
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Інвентаризації</title>
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <style>
            .category-list {
                display: flex;
                flex-wrap: wrap;
            }
            .category-item {
                margin-right: 10px;
                margin-bottom: 10px;
            }
            .product-section {
                margin-bottom: 20px;
            }
        </style>
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Інвентаризації</h3>
                    </div>
                    <div class="module-body">
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Склад</label>
                            <div class="controls">
                                <select name="id_warehouse" id="id_warehouse" class="span8 tip" required>
                                    <option value="">Виберіть склад</option>
                                    <?php
                                    $warehouseQuery = mysqli_query($con, "SELECT * FROM warehouse");
                                    while ($warehouseRow = mysqli_fetch_array($warehouseQuery)) {
                                        echo '<option value="' . $warehouseRow['id'] . '">' . $warehouseRow['name'] . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Категорії</label>
                            <div class="controls category-list">
                                <?php
                                $categoryQuery = mysqli_query($con, "SELECT * FROM category");
                                while ($categoryRow = mysqli_fetch_array($categoryQuery)) {
                                    echo '<div class="category-item">';
                                    echo '<label class="checkbox-inline">';
                                    echo '<input type="checkbox" name="categories[]" value="' . $categoryRow['id'] . '"> ' . $categoryRow['categoryName'];
                                    echo '</label>';
                                    echo '</div>';
                                }
                                ?>
                            </div>
                        </div>

                        <div class="control-group">
                            <div id="inventory_table"></div>
                        </div>

                        <div class="control-group">
                            <button type="button" id="submit_inventory" class="btn btn-dark">Зберегти</button>
                            <button type="button" id="write_off_button" class="btn btn-warning" data-toggle="modal" data-target="#write_off_modal">Списання</button>
                            <button type="button" id="surplus_button" class="btn btn-success" data-toggle="modal" data-target="#surplus_modal">Оприбуткування</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="write_off_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Перегляд списання</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Товари для списання:</label>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Артикул</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                            </tr>
                            </thead>
                            <tbody id="write_off_products"></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                </div>
            </div>
        </div>
    </div>
    <div id="surplus_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Перегляд оприбуткування</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Товари для оприбуткування:</label>
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Артикул</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                                <th>Дата надходження</th>
                            </tr>
                            </thead>
                            <tbody id="surplus_products"></tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"data-dismiss="modal">Закрити</button>
                </div>
            </div>
        </div>
    </div>
    <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            $('#id_warehouse, input[name="categories[]"]').change(function() {
                var id_warehouse = $('#id_warehouse').val();
                var categories = $('input[name="categories[]"]:checked').map(function() {
                    return this.value;
                }).get();
                $.ajax({
                    url: '',
                    type: 'POST',
                    data: {get_inventory_products: true, id_warehouse: id_warehouse, categories: categories},
                    success: function(response) {
                        $('#inventory_table').html(response);
                    }
                });
            });

            $('#submit_inventory').click(function() {
                var products = [];
                $('#inventory_table .product-section').each(function() {
                    var productId = $(this).data('product-id');
                    $(this).find('tbody tr').each(function() {
                        var product = {
                            product_id: productId,
                            accounting_quantity: $(this).find('.accounting-quantity').text(),
                            actual_quantity: $(this).find('.actual-quantity').val(),
                            purchase_price: $(this).find('.purchase-price').text(),
                            date_of_receipt: $(this).data('date-of-receipt')
                        };
                        products.push(product);
                    });
                });

                var id_warehouse = $('#id_warehouse').val();

                $.ajax({
                    url: '',
                    type: 'POST',
                    data: {submit_inventory: true, date: '<?php echo $currentTime; ?>', id_warehouse: id_warehouse, products: JSON.stringify(products)},
                    success: function(response) {
                        alert(response);
                        $('input[name="categories[]"]:checked').trigger('change');
                    }
                });
            });

            $('#write_off_button').click(function() {
                var products = [];
                $('#inventory_table .product-section').each(function() {
                    var productId = $(this).data('product-id');
                    $(this).find('tbody tr').each(function() {
                        var accountingQuantity = parseFloat($(this).find('.accounting-quantity').text());
                        var actualQuantity = parseFloat($(this).find('.actual-quantity').val());
                        var purchasePrice = parseFloat($(this).find('.purchase-price').text());
                        if (actualQuantity < accountingQuantity) {
                            var product = {
                                id: productId,
                                quantity: accountingQuantity - actualQuantity,
                                purchasePrice: purchasePrice
                            };
                            products.push(product);
                        }
                    });
                });

                var writeOffProductsHtml = '';
                products.forEach(function(product) {
                    writeOffProductsHtml += '<tr data-product-id="' + product.id + '">';
                    writeOffProductsHtml += '<td>' + product.id + '</td>'; // Тут виводиться артикул товару
                    writeOffProductsHtml += '<td>' + product.quantity + '</td>';
                    writeOffProductsHtml += '<td>' + product.purchasePrice + '</td>';
                    writeOffProductsHtml += '</tr>';
                });
                $('#write_off_products').html(writeOffProductsHtml);
            });

            $('#surplus_button').click(function() {
                var products = [];
                $('#inventory_table .product-section').each(function() {
                    var productId = $(this).data('product-id');
                    $(this).find('tbody tr').each(function() {
                        var accountingQuantity = parseFloat($(this).find('.accounting-quantity').text());
                        var actualQuantity = parseFloat($(this).find('.actual-quantity').val());
                        var purchasePrice = parseFloat($(this).find('.purchase-price').text());
                        var dateOfReceipt = $(this).data('date-of-receipt');
                        if (actualQuantity > accountingQuantity) {
                            var product = {
                                id: productId,
                                quantity: actualQuantity - accountingQuantity,
                                purchasePrice: purchasePrice,
                                dateOfReceipt: dateOfReceipt
                            };
                            products.push(product);
                        }
                    });
                });

                var surplusProductsHtml = '';
                products.forEach(function(product) {
                    surplusProductsHtml += '<tr data-product-id="' + product.id + '">';
                    surplusProductsHtml += '<td>' + product.id + '</td>'; // Тут виводиться артикул товару
                    surplusProductsHtml += '<td>' + product.quantity + '</td>';
                    surplusProductsHtml += '<td>' + product.purchasePrice + '</td>';
                    surplusProductsHtml += '<td>' + product.dateOfReceipt + '</td>';
                    surplusProductsHtml += '</tr>';
                });
                $('#surplus_products').html(surplusProductsHtml);
            });
        });
    </script>
    </body>
    </html>
<?php }?>