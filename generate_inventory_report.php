<?php
require_once 'include/config.php';
require_once __DIR__ . '/vendor/autoload.php';

$inventory_id = $_GET['id'];

// Отримуємо дані про інвентаризацію
$inventory_query = mysqli_query($con, "SELECT inventory.*, warehouse.name as warehouse_name, warehouse.address as warehouse_address 
                                 FROM inventory
                                 JOIN warehouse ON warehouse.id = inventory.id_warehouse
                                 WHERE inventory.id = $inventory_id");
$inventory = mysqli_fetch_array($inventory_query);

// Отримуємо товари з JSON
$products = json_decode($inventory['products'], true);

// Створюємо новий PDF документ
$mpdf = new \Mpdf\Mpdf();

// Генеруємо HTML для звіту
$html = '<h1>Звіт з інвентаризації</h1>';
$html .= '<p>Відомість інвентаризації №' . $inventory['id'] . ' від ' . $inventory['date'] . '</p>';
$html .= '<p>Склад: ' . $inventory['warehouse_name'] . ' - ' . $inventory['warehouse_address'] . '</p>';

$html .= '<table border="1" cellpadding="5">
            <thead>
                <tr>
                    <th>№</th>
                    <th>Товар</th>
                    <th>Партія</th>
                    <th>Облікова кількість</th>
                    <th>Фактична кількість</th>
                    <th>Одиниця вимірювання</th>
                    <th>Ціна</th>
                </tr>
            </thead>
            <tbody>';

$cnt = 1;
foreach ($products as $product) {
    $product_query = mysqli_query($con, "SELECT products.productName, units.name as unit_name 
                                      FROM products
                                      JOIN units ON units.id = products.units_id
                                      WHERE products.id = " . $product['id']);
    $product_data = mysqli_fetch_array($product_query);

    $html .= '<tr>
                <td>' . $cnt . '</td>
                <td>' . $product_data['productName'] . '</td>
                <td>Партія від ' . date('d.m.Y', strtotime($product['date_of_receipt'])) . '</td>
                <td>' . $product['accounting_quantity'] . '</td>
                <td>' . $product['actual_quantity'] . '</td>
                <td>' . $product_data['unit_name'] . '</td>
                <td>' . $product['purchase_price'] . '</td>
              </tr>';

    $cnt++;
}

$html .= '</tbody></table>';

$html .= '<p>Директор </p>_________________________________________</p>';
$html .= '<p>Члени комісії _________________________________________</p>';
$html .= '<p>_________________________________________</p>';
$html .= '<p>_________________________________________</p>';

// Записуємо HTML в PDF документ
$mpdf->WriteHTML($html);

// Виводимо PDF документ в браузер
$mpdf->Output('inventory_report_' . $inventory_id . '.pdf', 'I');
?>