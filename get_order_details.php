<?php
include("include/config.php");
require_once 'func/Order.php';
require_once 'func/Product.php';
$orderObj = new Order($con);
$productObj = new Product($con);
if (isset($_POST['orderId'])) {
    $orderId = $_POST['orderId'];

    // Get the order details
    $order = $orderObj->getOrderById($orderId);

    // Get the products from the order
    $products = json_decode($order['products'], true);

    ?>
    <div class="form-group">
        <label>Постачальник:</label>
        <input type="text" class="form-control" value="<?php echo $order['counterparty_name']; ?>" readonly>
        <input type="hidden" name="counterparty_id" value="<?php echo $order['id_counterparties']; ?>">
    </div>
    <div class="form-group">
        <label>Склад:</label>
        <input type="text" class="form-control" value="<?php echo $order['warehouse_name']; ?>" readonly>
        <input type="hidden" name="warehouse_id" value="<?php echo $order['id_warehouse']; ?>">
    </div>
    <table class="table">
        <thead>
        <tr>
            <th>Товар</th>
            <th>Кількість</th>
            <th>Ціна</th>
            <th>Термін придатності</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($products as $product) {
            $productName = $productObj->getProductName($product['id']);
            ?>
            <tr>
                <td>
                    <?php echo $productName; ?>
                    <input type="hidden" name="product_id[<?php echo $product['id']; ?>]" value="<?php echo $product['id']; ?>">
                </td>
                <td>
                    <input type="number" class="form-control" name="quantity[<?php echo $product['id']; ?>]" value="<?php echo $product['quantity']; ?>" required>
                </td>
                <td>
                    <input type="number" step="0.01" class="form-control" name="price[<?php echo $product['id']; ?>]" value="<?php echo $product['price']; ?>" required>
                </td>
                <td>
                    <input type="date" class="form-control" name="expiry_date[<?php echo $product['id']; ?>]" required>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <?php
}
?>