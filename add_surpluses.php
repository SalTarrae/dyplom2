<?php
session_start();
require_once 'func/login.php';
include("include/config.php");
require_once 'func/stocks.php';
require_once 'func/product.php';

$stocks = new Stocks($con);
$product = new Product($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
date_default_timezone_set('Europe/Kiev');
$currentTime = date('d-m-Y h:i:s A', time());

// Обробка AJAX-запиту для отримання всіх товарів
if (isset($_GET['action']) && $_GET['action'] === 'getAllProducts') {
    $productsQuery = $product->getAllProductsRaw();

    $products = array();
    while ($productRow = mysqli_fetch_array($productsQuery)) {
        $products[] = array(
            'id' => $productRow['id'],
            'productName' => $productRow['productName']
        );
    }

    echo json_encode($products);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Оприбуткування надлишків</title>
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
          rel='stylesheet'>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link type="text/css" href="css/theme.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <?php
            if (isset($_SESSION['msg']) && $_SESSION['msg'] !== "") {
                if ($_SESSION['msg_type'] === 'success') {
                    echo '<div class="alert alert-success">';
                } else {
                    echo '<div class="alert alert-danger">';
                }
                echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                echo '<strong>' . ($_SESSION['msg_type'] === 'success' ? 'Success!' : 'Error!') . '</strong> ' . htmlentities($_SESSION['msg']);
                echo '</div>';
                $_SESSION['msg'] = "";
                $_SESSION['msg_type'] = "";
            }
            ?>
            <div class="module-head">
                <h3>Оприбуткування надлишків</h3>
            </div>
            <div class="module-body">
                <form method="post" action="">
                    <div class="form-group">
                        <label for="date">Дата:</label>
                        <input type="date" class="form-control" id="date" name="date" required>
                    </div>
                    <div class="form-group">
                        <label for="warehouse">Склад:</label>
                        <select class="form-control" id="warehouse" name="warehouse" required>
                            <option value="">Виберіть склад</option>
                            <?php
                            $warehouseQuery = $stocks->getAllWarehouses();
                            while ($warehouseRow = mysqli_fetch_array($warehouseQuery)) {
                                ?>
                                <option value="<?php echo $warehouseRow['id']; ?>"><?php echo $warehouseRow['name']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="products">Товари:</label>
                        <table class="table" id="productsTable">
                            <thead>
                            <tr>
                                <th>Назва товару</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                                <th>Термін придатності</th>
                                <th>Дії</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#productModal">Додати товар</button>
                    </div>
                    <input type="hidden" name="products" id="productsInput">
                    <button type="submit" class="btn btn-primary" name="submit">Зберегти</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Product Modal -->
<div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productModalLabel">Виберіть товар</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table" id="allProductsTable">
                    <thead>
                    <tr>
                        <th>Назва товару</th>
                        <th>Дії</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        var products = [];

        // Завантаження всіх товарів по AJAX
        function loadAllProducts() {
            $.ajax({
                url: '',
                method: 'GET',
                data: {action: 'getAllProducts'},
                dataType: 'json',
                success: function (response) {
                    var tableBody = $('#allProductsTable tbody');
                    tableBody.empty();

                    response.forEach(function (product) {
                        var row = $('<tr>');
                        row.append($('<td>').text(product.productName));
                        row.append($('<td>').html('<button type="button" class="btn btn-primary btn-sm add-product" data-id="' + product.id + '" data-name="' + product.productName + '">Додати</button>'));
                        tableBody.append(row);
                    });
                }
            });
        }

        // Обробник події click для кнопки "Додати товар"
        $(document).on('click', '.add-product', function () {
            var productId = $(this).data('id');
            var productName = $(this).data('name');

            // Check if the product already exists in the array
            var existingProduct = products.find(function (product) {
                return product.id === productId;
            });

            if (existingProduct) {
                alert("Товар вже доданий до списку.");
            } else {
                var quantity = prompt(`Введіть кількість для "${productName}"`, 1);

                if (quantity !== null) {
                    quantity = parseInt(quantity);

                    if (quantity > 0) {
                        var price = prompt(`Введіть ціну для "${productName}"`);

                        if (price !== null) {
                            price = parseFloat(price);

                            if (price >= 0) {
                                var expiryDate = prompt(`Введіть термін придатності для "${productName}" (у форматі РРРР-ММ-ДД)`);

                                if (expiryDate !== null) {
                                    products.push({
                                        id: productId,
                                        name: productName,
                                        quantity: quantity,
                                        price: price,
                                        expiryDate: expiryDate
                                    });

                                    updateProductsTable();
                                    $('#productModal').modal('hide');
                                }
                            } else {
                                alert("Введена ціна некоректна.");
                            }
                        }
                    } else {
                        alert("Введена кількість некоректна.");
                    }
                }
            }
        });

        function updateProductsTable() {
            var tableBody = $('#productsTable tbody');
            tableBody.empty();

            products.forEach(function (product, index) {
                var row = $('<tr>');
                row.append($('<td>').text(product.name));
                var quantityInput = $('<input>').attr({
                    type: 'number',
                    class: 'form-control product-quantity',
                    'data-index': index,
                    min: 1
                }).val(product.quantity);
                row.append($('<td>').append(quantityInput));
                var priceInput = $('<input>').attr({
                    type: 'number',
                    class: 'form-control product-price',
                    'data-index': index,
                    min: 0,
                    step: 0.01
                }).val(product.price);
                row.append($('<td>').append(priceInput));
                var expiryDateInput = $('<input>').attr({
                    type: 'date',
                    class: 'form-control product-expiry-date',
                    'data-index': index
                }).val(product.expiryDate);
                row.append($('<td>').append(expiryDateInput));
                row.append($('<td>').html('<button type="button" class="btn btn-danger btn-sm remove-product" data-id="' + product.id + '">Видалити</button>'));
                tableBody.append(row);
            });

            $('.remove-product').click(function () {
                var productId = $(this).data('id');
                products = products.filter(function (product) {
                    return product.id !== productId;
                });
                updateProductsTable();
            });

            $('.product-quantity').change(function () {
                var index = $(this).data('index');
                var newQuantity = parseInt($(this).val());
                if (newQuantity > 0) {
                    products[index].quantity = newQuantity;
                } else {
                    alert("Кількість має бути більше 0.");
                    $(this).val(products[index].quantity);
                }
            });

            $('.product-price').change(function () {
                var index = $(this).data('index');
                var newPrice = parseFloat($(this).val());
                if (newPrice >= 0) {
                    products[index].price = newPrice;
                } else {
                    alert("Ціна не може бути від'ємною.");
                    $(this).val(products[index].price);
                }
            });

            $('.product-expiry-date').change(function () {
                var index = $(this).data('index');
                var newExpiryDate = $(this).val();
                products[index].expiryDate = newExpiryDate;
            });
        }

        $('form').submit(function (event) {
            event.preventDefault(); // Запобігти перезавантаженню сторінки

            var date = $('#date').val();
            var warehouseId = $('#warehouse').val();

            if (products.length === 0) {
                alert("Виберіть принаймні один товар для оприбуткування.");
                return; // Зупинити відправку форми
            }

            $('#productsInput').val(JSON.stringify(products));
            $('form').off('submit').submit(); // Відправити форму
        });

        // Завантаження всіх товарів при відкритті модального вікна
        $('#productModal').on('show.bs.modal', function () {
            loadAllProducts();
        });
    });
</script>
<?php
if (isset($_POST['submit'])) {
    $date = $_POST['date'];
    $warehouseId = $_POST['warehouse'];
    $products = json_decode($_POST['products'], true); // Декодуємо дані про товари з форми

    if (empty($products)) {
        $_SESSION['msg'] = "Error: No products selected.";
        $_SESSION['msg_type'] = "error";
    } else {
        try {
            if ($stocks->createSurplus($date, $warehouseId, $products)) {
                // Встановити сесійне повідомлення про успіх
                $_SESSION['msg'] = "Оприбуткування надлишків успішно збережено.";
                $_SESSION['msg_type'] = "success";
            } else {
                throw new Exception("Error creating surplus.");
            }
        } catch (Exception $e) {
            $_SESSION['msg'] = $e->getMessage();
            $_SESSION['msg_type'] = "error";
        }
    }
}
}
?>
</body>
</html>