<?php
session_start();
require_once 'func/login.php';
require_once 'func/order.php';
include("include/config.php");
require_once 'func/product.php';
require_once 'func/counterparties.php';
require_once 'func/stocks.php';

$product = new Product($con);
$legal = new Counterparties($con);
$stocks = new Stocks($con);
if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
}
else {
    date_default_timezone_set('Europe/Kiev'); // change according timezone
    $currentTime = date('d-m-Y h:i:s A', time());

    $order = new Order($con);

    if (isset($_POST['submit'])) {
        $number = $_POST['number'];
        $data = $_POST['data'];
        $counterparties = $_POST['counterparties'];
        $warehouse = $_POST['warehouse'];
        $selectedProducts = $_POST['selected_products'];

        if ($order->createOrder($number, $data, $counterparties, $warehouse, $selectedProducts)) {
            $_SESSION['msg'] = "Order inserted successfully!";
        } else {
            $_SESSION['delmsg'] = "Error inserting order or no products selected.";
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Товари</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <style>
        .selected-products-table {
            margin-top: 20px;
        }
        .selected-products-table th,
        .selected-products-table td {
            padding: 8px;
            text-align: left;
        }
        .selected-products-table th {
            background-color: #f2f2f2;
        }
        .remove-product {
            color: red;
            cursor: pointer;
        }
    </style>
    <script>
        $(document).ready(function() {
            $('#select-products').click(function() {
                $('#product-modal').modal('show');
            });

            $('.add-product').click(function() {
                var productId = $(this).data('id');
                var productName = $(this).data('name');

                var existingProduct = $('.selected-product[data-id="' + productId + '"]');

                if (existingProduct.length > 0) {
                    var quantityInput = existingProduct.find('.product-quantity');
                    var currentQuantity = parseInt(quantityInput.val());
                    quantityInput.val(currentQuantity + 1);
                } else {
                    var productHtml = '<tr class="selected-product" data-id="' + productId + '">' +
                        '<td><input type="hidden" name="selected_products[' + productId + '][id]" value="' + productId + '">' + productName + '</td>' +
                        '<td><input type="number" name="selected_products[' + productId + '][quantity]" class="product-quantity" value="1" min="1"></td>' +
                        '<td><input type="number" name="selected_products[' + productId + '][price]" class="product-price" min="0" step="0.01"></td>' +
                        '<td><span class="remove-product" data-id="' + productId + '"><i class="fa fa-trash"></i></span></td>' +
                        '</tr>';

                    $('#selected-products-table tbody').append(productHtml);
                }

                $('#product-modal').modal('hide');
            });

            $(document).on('click', '.remove-product', function() {
                var productId = $(this).data('id');
                $('.selected-product[data-id="' + productId + '"]').remove();
            });
        });
    </script>
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Замовлення постачальнику</h3>
                </div>
                <div class="module-body">
                    <?php if (isset($_SESSION['msg'])) { ?>
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong></strong> <?php echo htmlentities($_SESSION['msg']); $_SESSION['msg'] = ""; ?>
                        </div>
                    <?php } ?>

                    <?php if (isset($_SESSION['delmsg'])) { ?>
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong></strong> <?php echo htmlentities($_SESSION['delmsg']); $_SESSION['delmsg'] = ""; ?>
                        </div>
                    <?php } ?>

                    <form class="form-horizontal row-fluid" name="insertproduct" method="post" enctype="multipart/form-data">
                        <div class="control-group">
                            <label class="control-label" for="basicinput">№</label>
                            <div class="controls">
                                <input type="text" name="number" placeholder="Напишіть номер" class="span8 tip" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Дата</label>
                            <div class="controls">
                                <input type="date" name="data" class="span8 tip" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Постачальник</label>
                            <div class="controls">
                                <select name="counterparties" class="span8 tip" required>
                                    <option value="">Обрати постачальника</option>
                                    <?php
                                    $query = $legal->getAllCounterparties();
                                    while ($row = mysqli_fetch_array($query)) {
                                        ?>
                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['full_name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="basicinput">Склад</label>
                            <div class="controls">
                                <select name="warehouse" class="span8 tip" required>
                                    <option value="">Обрати склад</option>
                                    <?php
                                    $query = $stocks->getAllWarehouses();
                                    while ($row = mysqli_fetch_array($query)) {
                                        ?>
                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <button type="button" id="select-products" class="btn btn-primary">Вибрати товари</button>
                            </div>
                        </div>
                        <table id="selected-products-table" class="selected-products-table">
                            <thead>
                            <tr>
                                <th>Назва товару</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- Selected products will be added here dynamically -->
                            </tbody>
                        </table>
                        <div class="control-group">
                            <div class="controls">
                                <button type="submit" name="submit" class="btn btn-success">Зберегти</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Modal -->
<div class="modal fade" id="product-modal" tabindex="-1" role="dialog" aria-labelledby="product-modal-label" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="product-modal-label">Вибрати товари</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Назва товару</th>
                        <th>Дія</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $query = $product->getAllProductsRaw();
                    while ($row = mysqli_fetch_array($query)) {
                        ?>
                        <tr>
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['productName']; ?></td>
                            <td>
                                <button type="button" class="btn btn-success btn-sm add-product" data-id="<?php echo $row['id']; ?>" data-name="<?php echo $row['productName']; ?>">Додати</button>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>