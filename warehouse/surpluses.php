<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/stocks.php';

$stocks = new Stocks($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}
date_default_timezone_set('Europe/Kiev');
$currentTime = date('Y-m-d H:i:s', time());
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Надлишки</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Оприбуткування надлишків</h3>
                </div>
                <div class="module-body">
                    <div class="control-group">
                        <div class="control-group">
                            <a href="../index.php" class="btn btn-dark">Додати оприбуткування надлишків</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="module">
                <div class="module-head">
                    <h3>Оприбуткування надлишків</h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Склад</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $surpluses = $stocks->getAllSurpluses();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($surpluses)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['date']); ?></td>
                                <td><?php echo htmlentities($row['name']); ?></td>
                            </tr>
                            <?php $cnt=$cnt+1; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="../scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    });
</script>
</body>
</html>