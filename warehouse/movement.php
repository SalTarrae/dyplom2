<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../vendor/autoload.php';
require_once '../func/stocks.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    $stocks = new Stocks($con);

    if (isset($_POST['save'])) {
        $fromWarehouse = $_POST['from_warehouse'];
        $toWarehouse = $_POST['to_warehouse'];
        $products = json_decode($_POST['products'], true);

        foreach ($products as $product) {
            $productId = $product['id'];
            $quantity = $product['quantity'];
            $stockId = $product['stockId'];

            $stockRow = $stocks->getStockById($stockId);
            $stockQuantity = $stockRow['quantity'];

            if ($quantity == $stockQuantity) {
                // Переміщення всієї кількості товару
                $stocks->updateStockWarehouse($stockId, $toWarehouse);
            } else {
                // Переміщення частини товару
                $stocks->updateStockQuantity($stockId, $quantity, 'subtract');
                $stocks->insertStock($productId, $quantity, $stockRow['purchase_price'], $stockRow['margin_price'], $stockRow['id_counterparties'], $stockRow['date_of_receipt'], $stockRow['data_expiry'], $toWarehouse);
            }
        }
    }

    if (isset($_GET['warehouse_id'])) {
        $warehouseId = $_GET['warehouse_id'];
        $products = $stocks->getStockProductsByWarehouse($warehouseId);
        echo json_encode($products);
        exit();
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Переміщення товару</title>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module-head">
                    <h3>Переміщення товару</h3>
                </div>
                <div class="module-body">

                    <form method="post">
                        <div class="form-group">
                            <label for="from_warehouse">З якого складу:</label>
                            <select class="form-control" id="from_warehouse" name="from_warehouse" required>
                                <option value="">Виберіть склад</option>
                                <?php
                                $warehouseResult = $stocks->getAllWarehouses();
                                while ($warehouseRow = mysqli_fetch_assoc($warehouseResult)) {
                                    echo "<option value='" . $warehouseRow['id'] . "'>" . $warehouseRow['name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="to_warehouse">В який склад:</label>
                            <select class="form-control" id="to_warehouse" name="to_warehouse" required>
                                <option value="">Виберіть склад</option>
                                <?php
                                mysqli_data_seek($warehouseResult, 0);
                                while ($warehouseRow = mysqli_fetch_assoc($warehouseResult)) {
                                    echo "<option value='" . $warehouseRow['id'] . "'>" . $warehouseRow['name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProductModal">Додати товар</button>
                        <hr>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Назва товару</th>
                                <th>Кількість</th>
                                <th>Дата надходження</th>
                                <th>Дія</th>
                            </tr>
                            </thead>
                            <tbody id="productTableBody">
                            </tbody>
                        </table>
                        <input type="hidden" id="products" name="products">
                        <button type="submit" class="btn btn-success" name="save">Зберегти</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Product Modal -->
    <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="addProductModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addProductModalLabel">Додати товар</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Назва товару</th>
                            <th>Кількість</th>
                            <th>Дата надходження</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody id="productModalTableBody">
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            var selectedProducts = [];

            $('#from_warehouse').on('change', function () {
                var warehouseId = $(this).val();
                loadProducts(warehouseId);
            });

            function loadProducts(warehouseId) {
                $.ajax({
                    url: '?warehouse_id=' + warehouseId,
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var tableBody = $('#productModalTableBody');
                        tableBody.empty();

                        $.each(response, function (index, product) {
                            var row = '<tr>' +
                                '<td>' + product.productName + '</td>' +
                                '<td><input type="number" class="form-control quantity" value="1" min="1" max="' + product.quantity + '"></td>' +
                                '<td>' + product.date_of_receipt + '</td>' +
                                '<td><button type="button" class="btn btn-primary add-product" data-id="' + product.id + '" data-stock-id="' + product.stockId + '" data-max-quantity="' + product.quantity + '">Додати</button></td>' +
                                '</tr>';
                            tableBody.append(row);
                        });
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            }

            $(document).on('click', '.add-product', function () {
                var productId = $(this).data('id');
                var stockId = $(this).data('stock-id');
                var maxQuantity = $(this).data('max-quantity');
                var quantity = $(this).closest('tr').find('.quantity').val();
                var productName = $(this).closest('tr').find('td:first-child').text();
                var dateOfReceipt = $(this).closest('tr').find('td:nth-child(3)').text();

                if (quantity > maxQuantity) {
                    alert('Кількість не може перевищувати наявну кількість товару на складі.');
                    return;
                }

                var product = {
                    id: productId,
                    stockId: stockId,
                    quantity: quantity
                };

                selectedProducts.push(product);

                var row = '<tr>' +
                    '<td>' + productName + '</td>' +
                    '<td>' + quantity + '</td>' +
                    '<td>' + dateOfReceipt + '</td>' +
                    '<td><button type="button" class="btn btn-danger remove-product" data-id="' + productId + '" data-stock-id="' + stockId + '">Видалити</button></td>' +
                    '</tr>';
                $('#productTableBody').append(row);

                $('#products').val(JSON.stringify(selectedProducts));

                $(this).closest('tr').remove();
            });

            $(document).on('click', '.remove-product', function () {
                var productId = $(this).data('id');
                var stockId = $(this).data('stock-id');

                selectedProducts = selectedProducts.filter(function (product) {
                    return product.id !== productId || product.stockId !== stockId;
                });

                $('#products').val(JSON.stringify(selectedProducts));

                $(this).closest('tr').remove();

                // Додати товар назад до модального вікна
                var productName = $(this).closest('tr').find('td:first-child').text();
                var quantity = $(this).closest('tr').find('td:nth-child(2)').text();
                var dateOfReceipt = $(this).closest('tr').find('td:nth-child(3)').text();

                var row = '<tr>' +
                    '<td>' + productName + '</td>' +
                    '<td><input type="number" class="form-control quantity" value="' + quantity + '" min="1" max="' + quantity + '"></td>' +
                    '<td>' + dateOfReceipt + '</td>' +
                    '<td><button type="button" class="btn btn-primary add-product" data-id="' + productId + '" data-stock-id="' + stockId + '" data-max-quantity="' + quantity + '">Додати</button></td>' +
                    '</tr>';
                $('#productModalTableBody').append(row);
            });
        });
    </script>
    </body>
    </html>
    <?php
}
?>