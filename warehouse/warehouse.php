<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/stocks.php';

$stock = new Stocks($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
} else {

    date_default_timezone_set('Europe/Kiev');
    $currentTime = date( 'd-m-Y h:i:s A', time () );


    if(isset($_POST['submit']))
    {
        $name=$_POST['warehouse_name'];
        $address=$_POST['warehouse_address'];
        if ($stock->createWarehouse($name, $address)) {
            $_SESSION['msg']="Склад створено!";
        } else {
            $_SESSION['error']="Помилка при створенні складу!";
        }

    }

    if(isset($_GET['del']))
    {
        if ($stock->deleteWarehouse($_GET['id'])) {
            $_SESSION['delmsg']="Склад видалено!";
        } else {
            $_SESSION['error']="Помилка при видаленні складу!";
        }
    }
    if(isset($_POST['update']))
    {
        $id = $_POST['warehouse_id'];
        $name = $_POST['warehouse_name'];
        $address = $_POST['warehouse_address'];

        if ($stock->updateWarehouse($id, $name, $address)) {
            $_SESSION['msg'] = "Склад відредаговано!";
        } else {
            $_SESSION['error'] = "Помилка при редагуванні складу!";
        }
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Склади</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Склади</h3>
                </div>

                <div class="module-body">

                    <div class="control-group">

                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#createWarehouseModal">
                            Створити новий склад
                        </button>
                    </div>

                    <div class="control-group">

                        <?php if(isset($_SESSION['msg']))
                        {?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Все добре!</strong>	<?php echo htmlentities($_SESSION['msg']);?><?php echo htmlentities($_SESSION['msg']="");?>
                            </div>
                        <?php } ?>


                        <?php if(isset($_SESSION['delmsg']))
                        {?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong></strong> 	<?php echo htmlentities($_SESSION['delmsg']);?><?php echo htmlentities($_SESSION['delmsg']="");?>
                            </div>
                        <?php } ?>

                        <?php if(isset($_SESSION['error']))
                        {?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Помилка!</strong> 	<?php echo htmlentities($_SESSION['error']);?><?php echo htmlentities($_SESSION['error']="");?>
                            </div>
                        <?php } ?>

                        <br />

                    </div>
                </div>

                <div class="modal fade" id="createWarehouseModal" tabindex="-1" aria-labelledby="createWarehouseModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="createWarehouseModalLabel">Створити новий склад</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form class="form-horizontal row-fluid" name="Category" method="post">
                                <div class="modal-body">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Назва складу</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Назва складу" name="warehouse_name" class="span8 tip" required>
                                        </div>
                                        <label class="control-label" for="basicinput">Адреса складу</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Адреса складу" name="warehouse_address" class="span8 tip" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                                    <button type="submit" name="submit" class="btn btn-dark">Створити</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


            <div class="module">
                <div class="module-head">
                    <h3>Склади</h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped	 display" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Назва складу</th>
                            <th>Адреса</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $warehouses = $stock->getAllWarehouses();
                        $cnt=1;
                        while($row=mysqli_fetch_array($warehouses))
                        {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt);?></td>
                                <td><?php echo htmlentities($row['name']);?></td>
                                <td> <?php echo htmlentities($row['address']);?></td>
                                <td>
                                    <button type="button" class="btn edit-warehouse" data-toggle="modal" data-target="#editWarehouseModal" data-id="<?php echo $row['id']; ?>" data-name="<?php echo $row['name']; ?>" data-address="<?php echo $row['address']; ?>">
                                        <i class="icon-edit"></i>
                                    </button>
                                    <a href="warehouse.php?id=<?php echo $row['id']?>&del=delete" onClick="return confirm('Ви дійсно хочете видалити запис?')"><i class="icon-remove-sign"></i></a></td>
                            </tr>
                            <?php $cnt=$cnt+1; } ?>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editWarehouseModal" tabindex="-1" aria-labelledby="editWarehouseModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editWarehouseModalLabel">Редагувати склад</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal row-fluid" name="editCategory" method="post">
                <input type="hidden" name="warehouse_id" id="edit_warehouse_id">
                <div class="modal-body">
                    <div class="control-group">
                        <label class="control-label" for="basicinput">Назва складу</label>
                        <div class="controls">
                            <input type="text" placeholder="Назва складу" name="warehouse_name" id="edit_warehouse_name" class="span8 tip" required>
                        </div>
                        <label class="control-label" for="basicinput">Адреса складу</label>
                        <div class="controls">
                            <input type="text" placeholder="Адреса складу" name="warehouse_address" id="edit_warehouse_address" class="span8 tip" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" name="update" class="btn btn-primary">Зберегти зміни</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="../scripts/datatables/jquery.dataTables.js"></script>
<script src="../scripts/menu.js"></script>

<script>
    $(document).ready(function() {
        $('.edit-warehouse').click(function() {
            var id = $(this).data('id');
            var name = $(this).data('name');
            var address = $(this).data('address');

            $('#edit_warehouse_id').val(id);
            $('#edit_warehouse_name').val(name);
            $('#edit_warehouse_address').val(address);
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    } );
</script>
</body>
<?php } ?>