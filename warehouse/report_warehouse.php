<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../vendor/autoload.php';
require_once '../func/stocks.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    $stocks = new Stocks($con);

    if (isset($_GET['ajax'])) {
        $warehouseId = isset($_GET['warehouse_id']) ? $_GET['warehouse_id'] : 'all';

        $result = $stocks->getStockAnalysis($warehouseId);

        $tableHtml = '<table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Товар (Назва)</th>
                                <th>Артикул</th>
                                <th>Залишок (кількість)</th>
                                <th>Ціна</th>
                                <th>Собівартість</th>
                            </tr>
                        </thead>
                        <tbody>';

        $totalQuantity = 0;
        $totalCost = 0;

        while ($row = mysqli_fetch_assoc($result)) {
            $totalQuantity = $row['total_quantity'];
            $price = $row['min_price'];
            $cost = $row['cost'];

            $tableHtml .= "<tr>
                            <td>" . $row['productName'] . "</td>
                            <td>" . $row['article'] . "</td>
                            <td>" . $totalQuantity . "</td>
                            <td>" . number_format($price, 2) . "</td>
                            <td>" . number_format($cost, 2) . "</td>
                          </tr>";

            $totalCost += $cost;
        }

        $tableHtml .= '</tbody></table>';

        $totalHtml = '<p><strong>Загальна сума залишків товарів:</strong> ' . $totalQuantity . '</p>';
        $totalHtml .= '<p><strong>Загальна сума собівартості запасів:</strong> ' . number_format($totalCost, 2) . '</p>';

        echo json_encode(['table' => $tableHtml, 'total' => $totalHtml]);
        exit();
    }

    if (isset($_GET['pdf'])) {
        $warehouseId = isset($_GET['warehouse_id']) ? $_GET['warehouse_id'] : 'all';

        $result = $stocks->getStockAnalysis($warehouseId);

        $html = '<html>
                    <head>
                        <style>
                            body { font-family: Arial, sans-serif; }
                            table { border-collapse: collapse; width: 100%; }
                            th, td { border: 1px solid black; padding: 8px; text-align: left; }
                            th { background-color: #f2f2f2; }
                        </style>
                    </head>
                    <body>
                        <h1 style="text-align: center;">Аналіз запасів складу</h1>
                        <p><strong>Дата складання звіту:</strong> ' . date('d.m.Y') . '</p>
                        <p><strong>Період, за який аналізуються дані:</strong> на ' . date('d.m.Y') . '</p>
                        <table>
                            <thead>
                                <tr>
                                    <th>Товар (Назва)</th>
                                    <th>Артикул</th>
                                    <th>Залишок (кількість)</th>
                                    <th>Ціна</th>
                                    <th>Собівартість</th>
                                </tr>
                            </thead>
                            <tbody>';

        $totalQuantity = 0;
        $totalCost = 0;

        while ($row = mysqli_fetch_assoc($result)) {
            $totalQuantity = $row['total_quantity'];
            $price = $row['min_price'];
            $cost = $row['cost'];

            $html .= "<tr>
                        <td>" . $row['productName'] . "</td>
                        <td>" . $row['article'] . "</td>
                        <td>" . $totalQuantity . "</td>
                        <td>" . number_format($price, 2) . "</td>
                        <td>" . number_format($cost, 2) . "</td>
                      </tr>";

            $totalCost += $cost;
        }

        $html .= '</tbody>
                </table>
                <p><strong>Загальна сума залишків товарів:</strong> ' . $totalQuantity . '</p>
                <p><strong>Загальна сума собівартості запасів:</strong> ' . number_format($totalCost, 2) . '</p>
            </body>
        </html>';

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);

        $filename = 'stock_analysis_report.pdf';

        if (isset($_GET['download'])) {
            $mpdf->Output($filename, 'D');
        } else {
            $mpdf->Output($filename, 'I');
        }
        exit();
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Аналіз запасів складу</title>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module-head">
                    <h3>Аналіз запасів складу</h3>
                </div>
                <div class="module-body">
                    <form id="stockForm">
                        <div class="form-group">
                            <label for="warehouse">Склад:</label>
                            <select class="form-control" id="warehouse" name="warehouse_id">
                                <option value="all">Всі склади</option>
                                <?php
                                $warehouseQuery = $stocks->getAllWarehouses();
                                while ($warehouseRow = mysqli_fetch_assoc($warehouseQuery)) {
                                    echo "<option value='" . $warehouseRow['id'] . "'>" . $warehouseRow['name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Показати</button>
                        <button type="button" class="btn btn-secondary" id="printButton">Друк звіту</button>
                        <button type="button" class="btn btn-success" id="downloadButton">Завантажити звіт</button>
                    </form>
                    <hr>
                    <div id="tableContainer"></div>
                    <div id="totalContainer"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#stockForm').on('submit', function (e) {
                e.preventDefault();
                var formData = $(this).serialize();
                $.ajax({
                    url: '?ajax=1',
                    type: 'GET',
                    data: formData,
                    dataType: 'json',
                    success: function (response) {
                        $('#tableContainer').html(response.table);
                        $('#totalContainer').html(response.total);
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $('#printButton').on('click', function () {
                var formData = $('#stockForm').serialize();
                window.open('?pdf=1&' + formData, '_blank');
            });

            $('#downloadButton').on('click', function () {
                var formData = $('#stockForm').serialize();
                window.location.href = '?pdf=1&download=1&' + formData;
            });

            // Trigger form submission on page load
            $('#stockForm').trigger('submit');
        });
    </script>
    </body>
    </html>

    <?php
}
?>