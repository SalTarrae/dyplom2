<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once '../func/stocks.php';
require_once '../func/product.php';

use Mpdf\Mpdf;

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}
date_default_timezone_set('Europe/Kiev');
$currentTime = date('Y-m-d H:i:s', time());

$stocks = new Stocks($con);
$product = new Product($con);

if (isset($_POST['create_write_off'])) {
    $date = $_POST['date'];
    $reason = $_POST['reason'];
    $warehouseId = $_POST['id_warehouse'];
    $products = $_POST['products'];

    try {
        if ($stocks->createWriteOff($date, $warehouseId, $reason, $products)) {
            $_SESSION['msg'] = "Запис успішно створено!";
        } else {
            $_SESSION['msg'] = "Помилка при створенні запису!";
        }
    } catch (Exception $e) {
        $_SESSION['msg'] = "Помилка: " . $e->getMessage();
    }
}
if (isset($_GET['del']) && $_GET['del'] == 'delete') {
    $id = $_GET['id'];

    if ($stocks->deleteWriteOff($id)) {
        $_SESSION['msg'] = "Списання успішно видалено!";
    } else {
        $_SESSION['error'] = "Цей запис неможливо видалити, так як він відноситься до інвентаризації";
    }
}

if (isset($_GET['id']) && $_GET['action'] == 'generate_pdf') {
    $writeOffId = $_GET['id'];
    $writeOffData = $stocks->getWriteOffById($writeOffId);

    $dataStoreQuery = mysqli_query($con, "SELECT * FROM data_store LIMIT 1");
    $dataStore = mysqli_fetch_assoc($dataStoreQuery);

    $html = '
        <html>
        <head>
            <style>
                body {
                    font-family: DejaVu Sans;
                }
                table {
                    border-collapse: collapse;
                    width: 100%;
                }
                th, td {
                    border: 1px solid black;
                    padding: 8px;
                    text-align: left;
                }
            </style>
        </head>
        <body>
            <table>
                <tr>
                    <td colspan="2">
                        <h2>"ЗАТВЕРДЖУЮ"</h2>
                        <p>' . $dataStore['full_name'] . '</p>
                         <p></p>
                          <p>' . $dataStore['persona'] . '</p>
                        <p>(підпис керівника установи)</p>
                        <p>' . date('d.m.Y') . ' р.</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h1>Акт списання № ' . $writeOffData['id'] . ' від ' . $writeOffData['date'] . '</h1>
                        <p>Комісія, призначена наказом по установі (організації) від ______________________ у складі:</p>
                        <ul>
                            <li>Голова комісії: ' . $dataStore['persona'] . '</li>
                            <li>Члени комісії: </li>
                            <li>__________________________</li>
                            <li>__________________________</li>
                            <li>__________________________</li>
                        </ul>
                        <p>здійснила перевірку товарно-матеріальних цінностей, та встановила, що описані нижче цінності непридатні для подальшого використання у діяльності підприємства, підлягають списанню, вилученню з обліку та утилізації:</p>
                    </td>
                </tr>
            </table>
            
            <table>
                <tr>
                    <th>№</th>
                    <th>Артикул</th>
                    <th>Найменування</th>
                    <th>Од. вим.</th>
                    <th>Кількість</th>
                    <th>Ціна</th>
                    <th>Сума</th>
                    <th>Підстава непридатності</th>
                </tr>';

    $products = json_decode($writeOffData['products'], true);
    $cnt = 1;
    $totalQuantity = 0;
    $totalSum = 0;
    foreach ($products as $productData) {
        $productId = $productData['id'];
        $quantity = $productData['quantity'];
        $purchasePrice = $productData['purchasePrice'];
        $sum = $quantity * $purchasePrice;

        // Отримання артикулу та назви продукту з таблиці products
        $productDetails = $product->getProductById($productId);
        $article = $productDetails['article'];
        $productName = $productDetails['productName'];

        // Отримання назви одиниці виміру
        $unitsId = $productDetails['units_id'];
        $unitName = $product->getUnitName($unitsId);

        $html .= '
                <tr>
                    <td>' . $cnt . '</td>
                    <td>' . $article . '</td>
                    <td>' . $productName . '</td>
                    <td>' . $unitName . '</td>
                    <td>' . $quantity . '</td>
                    <td>' . number_format($purchasePrice, 2) . '</td>
                    <td>' . number_format($sum, 2) . '</td>
                    <td>' . $writeOffData['reason'] . '</td>
                </tr>';

        $cnt++;
        $totalQuantity += $quantity;
        $totalSum += $sum;
    }

    $html .= '
            </table>
            
            <p>Усього за цим актом списано товарно-матеріальних цінностей на суму: ' . number_format($totalSum, 2) . ' грн.</p>
            <p>Окремі зауваження комісії: винні особи встановлені: не встановлені (необхідне підкреслити).</p>
            <p>Перелік винних осіб ____________________________________________________________________________</p>
            
            <table>
                <tr>
                    <td>
                        Голова комісії
                    </td>
                    <td>
                        <p>(підпис)</p>
                        <p>' . $dataStore['persona'] . '</p>
                    </td>
                </tr>
                <tr>
                    <td>
                        Члени комісії
                    </td>
                    <td>
                        <p>(підпис)</p>
                        <p>_________________</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p>(підпис)</p>
                    <p>_________________</p>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <p>(підпис)</p>
                        <p>_________________</p>
                    </td>
                </tr>
            </table>
        </body>
        </html>';

    $mpdf = new Mpdf();
    $mpdf->WriteHTML($html);
    $mpdf->Output('act_write_off_' . $writeOffId . '.pdf', 'D');
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Товари</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Списання</h3>
                </div>
                <div class="module-body">
                    <div class="control-group">
                        <div class="control-group">
                            <a href="../index.php" class="btn btn-dark">Додати списання</a>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php if(isset($_POST['create_write_off']) || isset($_GET['action'])) { ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Все добре!</strong>	<?php echo htmlentities($_SESSION['msg']); ?>
                            </div>
                            <?php unset($_SESSION['msg']); ?>
                        <?php } ?>
                    </div>
                    <div class="control-group">
                        <?php if(isset($_SESSION['error'])) { ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Помилка!</strong> <?php echo htmlentities($_SESSION['error']); ?>
                            </div>
                            <?php unset($_SESSION['error']); ?>

                        <?php } ?>
                    </div>
                </div>
            </div>
       <div class="module">
                <div class="module-head">
                    <h3>Списання</h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Причина</th>
                            <th>Склад</th>
                            <th>Дії</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $writeOffs = $stocks->getAllWriteOffs();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($writeOffs)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['date']); ?></td>
                                <td><?php echo htmlentities($row['reason']); ?></td>
                                <td><?php echo htmlentities($row['name']); ?></td>
                                <td>
                                    <a href="?id=<?php echo $row['id']; ?>&action=generate_pdf" class="btn btn-primary btn-sm">Акт списання</a>
                                    <a href="write-off.php?id=<?php echo $row['id']; ?>&del=delete" class="btn btn-danger btn-sm" onclick="return confirm('Ви впевнені, що хочете видалити цей запис?')">Видалити</a>
                                </td>
                            </tr>
                            <?php $cnt=$cnt+1; } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="../scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    });
</script>
</body>
</html>