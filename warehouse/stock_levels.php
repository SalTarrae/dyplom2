<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/stocks.php';
require_once '../func/stock_levels.php';
require_once '../func/product.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
} else {
date_default_timezone_set('Europe/Kiev');
$currentTime = date('d-m-Y h:i:s A', time());

$stocks = new Stocks($con);
$stockLevels = new StockLevels($con);
$product = new Product($con);

if (isset($_POST['create_stock_level'])) {
    $id_product = $_POST['id_product'];
    $quantity = $_POST['quantity'];
    $id_warehouse = $_POST['id_warehouse'];

    $createResult = $stockLevels->createStockLevel($id_product, $quantity, $id_warehouse);
    if ($createResult === false) {
        $_SESSION['msg'] = "Товар вже існує в таблиці норм для вибраного складу!";
    } elseif ($createResult) {
        $_SESSION['msg'] = "Норма виміру успішно створена!";
    } else {
        $_SESSION['msg'] = "Помилка при створенні норми виміру!";
    }
}

if (isset($_POST['update_stock_level'])) {
    $stockLevelId = $_POST['stock_level_id'];
    $idProduct = $_POST['id_product'];
    $quantity = $_POST['quantity'];
    $idWarehouse = $_POST['id_warehouse'];

    $updateResult = $stockLevels->updateStockLevel($stockLevelId, $idProduct, $quantity, $idWarehouse);
    if ($updateResult) {
        $_SESSION['msg'] = "Норма виміру успішно оновлена!";
    } else {
        $_SESSION['msg'] = "Помилка при оновленні норми виміру!";
    }
}

if (isset($_GET['del'])) {
    $stockLevels->deleteStockLevel($_GET['id']);
    $_SESSION['delmsg'] = "Deleted!!";
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Створити норму</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="../css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module">
                <div class="module-head">
                    <h3>Норми запасів</h3>
                </div>
                <div class="module-body">
                    <div class="control-group">
                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#createStockLevelModal">
                            Створити норму
                        </button>
                    </div>
                    <div class="control-group">
                        <?php if (isset($_POST['submit'])) { ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Все добре!</strong> <?php echo htmlentities($_SESSION['msg']); ?><?php echo htmlentities($_SESSION['msg'] = ""); ?>
                            </div>
                        <?php } ?>

                        <?php if (isset($_GET['del'])) { ?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong></strong> <?php echo htmlentities($_SESSION['delmsg']); ?><?php echo htmlentities($_SESSION['delmsg'] = ""); ?>
                            </div>
                        <?php } ?>

                        <br/>

                    </div>
                </div>
            </div>

            <div class="module">
                <div class="module-head">
                    <h3>Склади</h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Товар</th>
                            <th>Норма</th>
                            <th>Одиниця виміру</th>
                            <th>Склад</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $stockLevelQuery = $stockLevels->getStockLevels();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($stockLevelQuery)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['productName']); ?></td>
                                <td><?php echo htmlentities($row['quantity']); ?></td>
                                <td><?php echo htmlentities($row['unit_name']); ?></td>
                                <td><?php echo htmlentities($row['name']); ?></td>
                                <td>
                                    <button type="button" class="btn btn-primary edit-stock-level" data-toggle="modal" data-target="#editStockLevelModal"
                                            data-id="<?php echo $row['id']; ?>"
                                            data-product-id="<?php echo $row['id_product']; ?>"
                                            data-quantity="<?php echo $row['quantity']; ?>"
                                            data-warehouse-id="<?php echo $row['id_warehouse']; ?>">
                                        <i class="icon-edit"></i>
                                    </button>
                                    <a href="stock_levels.php?id=<?php echo $row['id'] ?>&del=delete" onClick="return confirm('Ви дійсно хочете видалити запис?')"><i class="icon-remove-sign"></i></a>
                                </td>
                            </tr>
                            <?php $cnt = $cnt + 1;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createStockLevelModal" tabindex="-1" aria-labelledby="createStockLevelModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createStockLevelModalLabel">Створити нову норму виміру</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal row-fluid" name="createStockLevel" method="post">
                <div class="modal-body">
                    <div class="control-group">
                        <label class="control-label" for="basicinput">Товар</label>
                        <div class="controls">
                            <select name="id_product" class="span8 tip" required>
                                <option value="">Обрати товар</option>
                                <?php
                                $productQuery = $product->getAllProductsRaw();
                                while ($productRow = mysqli_fetch_array($productQuery)) {
                                    echo '<option value="' . $productRow['id'] . '">' . $productRow['productName'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <label class="control-label" for="basicinput">Норма</label>
                        <div class="controls">
                            <input type="number" placeholder="Норма" name="quantity" class="span8 tip" required>
                        </div>
                        <label class="control-label" for="basicinput">Склад</label>
                        <div class="controls">
                            <select name="id_warehouse" class="span8 tip" required>
                                <option value="">Обрати склад</option>
                                <?php
                                $warehouseQuery = $stocks->getAllWarehouses();
                                while ($warehouseRow = mysqli_fetch_array($warehouseQuery)) {
                                    echo '<option value="' . $warehouseRow['id'] . '">' . $warehouseRow['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" name="create_stock_level" class="btn btn-dark">Створити</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editStockLevelModal" tabindex="-1" aria-labelledby="editStockLevelModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="editStockLevelModalLabel">Редагувати норму виміру</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal row-fluid" name="editStockLevel" method="post">
                <input type="hidden" name="stock_level_id" id="edit_stock_level_id">
                <div class="modal-body">
                    <div class="control-group">
                        <label class="control-label" for="basicinput">Товар</label>
                        <div class="controls">
                            <select name="id_product" id="edit_id_product" class="span8 tip" required>
                                <?php
                                $productQuery = $product->getAllProductsRaw();
                                while ($productRow = mysqli_fetch_array($productQuery)) {
                                    echo '<option value="' . $productRow['id'] . '">' . $productRow['productName'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <label class="control-label" for="basicinput">Норма</label>
                        <div class="controls">
                            <input type="number" placeholder="Норма" name="quantity" id="edit_quantity" class="span8 tip" required>
                        </div>
                        <label class="control-label" for="basicinput">Склад</label>
                        <div class="controls">
                            <select name="id_warehouse" id="edit_id_warehouse" class="span8 tip" required>
                                <?php
                                $warehouseQuery = $stocks->getAllWarehouses();
                                while ($warehouseRow = mysqli_fetch_array($warehouseQuery)) {
                                    echo '<option value="' . $warehouseRow['id'] . '">' . $warehouseRow['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="submit" name="update_stock_level" class="btn btn-primary">Зберегти зміни</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="../scripts/datatables/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    });
</script>
<script>
    $(document).ready(function() {
        $('.edit-stock-level').click(function() {
            var stockLevelId = $(this).data('id');
            var productId = $(this).data('product-id');
            var quantity = $(this).data('quantity');
            var warehouseId = $(this).data('warehouse-id');

            $('#edit_stock_level_id').val(stockLevelId);
            $('#edit_id_product').val(productId);
            $('#edit_quantity').val(quantity);
            $('#edit_id_warehouse').val(warehouseId);
        });
    });
</script>
</body>
    </html>

    <?php
}
?>