<?php
session_start();
require_once 'func/login.php';
require_once 'func/sales.php';
require_once 'func/stocks.php';
include("include/config.php");

$sales = new Sales($con);
$stocks = new Stocks($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}

$monthlySalesCount = $sales->getMonthlySalesCount();
$monthlyProductsSold = $sales->getMonthlyProductsSold();
$dailyProductsSold = $sales->getDailyProductsSold();
$popularProduct = $sales->getPopularProduct();
$highestStockProduct = $stocks->getHighestStockProduct();
$lowestStockProduct = $stocks->getLowestStockProduct();

$salesByDay = $sales->getSalesByDay();

$daysInMonth = date('t');
$salesData = array();

for ($day = 1; $day <= $daysInMonth; $day++) {
    $date = date('Y-m-') . sprintf('%02d', $day);
    $count = isset($salesByDay[$date]) ? $salesByDay[$date] : 0;
    $salesData[] = $count;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Dashboard</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <style>
        body {
            background-color: #f4f6f9;
        }
        .card {
            border: none;
            border-radius: 10px;
            box-shadow: 0 4px 6px rgba(0, 0, 0, 0.1);
            transition: all 0.3s ease;
        }
        .card:hover {
            transform: translateY(-5px);
            box-shadow: 0 8px 12px rgba(0, 0, 0, 0.15);
        }
        .card-title {
            font-size: 1.2rem;
            font-weight: bold;
            color: #1f4068;
        }
        .card-text {
            font-size: 2rem;
            font-weight: bold;
            color: #1f4068;
        }
        .bg-pastel-blue {
            background-color: #a8d0e6 !important;
        }
        .bg-pastel-green {
            background-color: #8bc34a !important;
        }
        .bg-pastel-yellow {
            background-color: #f8c291 !important;
        }
        .text-white {
            color: #fff !important;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <h3 class="mb-4" style="color: #1f4068;">Dashboard</h3>
            <div class="row">
                <div class="col-md-4">
                    <div class="card bg-pastel-blue text-white mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Кількість продажів за місяць</h5>
                            <p class="card-text"><?php echo $monthlySalesCount; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-pastel-green text-white mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Кількість проданих товарів за місяць</h5>
                            <p class="card-text"><?php echo $monthlyProductsSold; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-pastel-yellow text-white mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Кількість проданих товарів за цей день</h5>
                            <p class="card-text"><?php echo $dailyProductsSold; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Популярний товар</h5>
                            <p class="card-text"><?php echo $popularProduct; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Товар з найбільшим залишком</h5>
                            <p class="card-text"><?php echo $highestStockProduct; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Товар з найменшим залишком</h5>
                            <p class="card-text"><?php echo $lowestStockProduct; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h5 class="card-title">Графік продажів по дням місяця</h5>
                            <canvas id="salesChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    // Графік продажів по дням місяця
    var ctx = document.getElementById('salesChart').getContext('2d');
    var salesChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: <?php echo json_encode(range(1, $daysInMonth)); ?>,
            datasets: [{
                label: 'Кількість продажів',
                data: <?php echo json_encode($salesData); ?>,
                backgroundColor: 'rgba(31, 64, 104, 0.2)',
                borderColor: 'rgba(31, 64, 104, 1)',
                borderWidth: 2,
                pointBackgroundColor: 'rgba(31, 64, 104, 1)',
                pointRadius: 4,
                pointHoverRadius: 6
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    stepSize: 1
                }
            },
            plugins: {
                legend: {
                    labels: {
                        font: {
                            size: 14
                        }
                    }
                }
            }
        }
    });
</script>

<?php
if (isset($_POST['action']) && $_POST['action'] == 'get_sales_data') {
    $categoryId = $_POST['categoryId'];

    $labels = array();
    $salesData = array();

    $salesQuery = mysqli_query($con, "SELECT DATE(date) AS date, SUM(JSON_LENGTH(products)) AS total_sales
                                       FROM sales
                                       WHERE " . ($categoryId ? "JSON_EXTRACT(products, '$[*].id_product') IN (
                                                                     SELECT id 
                                                                     FROM products
                                                                     WHERE category = $categoryId
                                                                 )" : "1") . "
                                       GROUP BY DATE(date)
                                       ORDER BY DATE(date)");

    while ($salesRow = mysqli_fetch_assoc($salesQuery)) {
        $labels[] = $salesRow['date'];
        $salesData[] = $salesRow['total_sales'];
    }

    $data = array(
        'labels' => $labels,
        'salesData' => $salesData
    );

    echo json_encode($data);
    exit();
}
?>
</body>
</html>