<?php
session_start();
require_once '../func/login.php';
require_once '../func/price.php';
require_once '../func/product.php'; // Додано підключення класу Product
include("../include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    // Перевірка ролі користувача
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    $price = new Price($con);
    $product = new Product($con); // Створення об'єкту класу Product

    // Видалення акцій, у яких date_finish до сьогоднішнього дня
    $price->deleteExpiredDiscounts();

    if (isset($_POST['create_discount'])) {
        $startDate = $_POST['start_date'];
        $endDate = $_POST['end_date'];
        $percentage = $_POST['percentage'];
        $productId = isset($_POST['product_id']) ? $_POST['product_id'] : null;

        if ($productId !== null) {
            // Перевірка, чи вже існує акція на цей товар
            if ($price->isDiscountExists($productId)) {
                $_SESSION['error'] = "Акція на цей товар вже існує!";
            } else {
                $price->createDiscount($startDate, $endDate, $percentage, $productId);
                $_SESSION['msg'] = "Акцію створено!";
            }
        } else {
            $_SESSION['error'] = "Будь ласка, оберіть товар для створення акції!";
        }
    }

    // Функція для отримання старої ціни продукту через AJAX
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['get_old_price'])) {
        $productId = $_POST['product_id'];

        $priceData = $price->getOldPrice($productId);
        $oldPrice = $priceData['margin_price'];
        $pdvPrice = $priceData['pdv_price'];

        echo json_encode(['old_price' => $oldPrice, 'pdv_price' => $pdvPrice]);
        exit();
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Знижки</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Створити акцію</h3>
                    </div>
                    <div class="module-body">
                        <?php if (isset($_SESSION['msg'])) { ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Все добре!</strong> <?php echo $_SESSION['msg']; ?>
                            </div>
                            <?php unset($_SESSION['msg']); ?>
                        <?php } ?>
                        <?php if (isset($_SESSION['error'])) { ?>
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Помилка!</strong> <?php echo $_SESSION['error']; ?>
                            </div>
                            <?php unset($_SESSION['error']); ?>
                        <?php } ?>

                        <form method="post" action="">
                            <div class="form-group">
                                <label for="start_date">Дата початку:</label>
                                <input type="date" class="form-control" id="start_date" name="start_date" value="<?php echo date('Y-m-d'); ?>" required>
                            </div>

                            <div class="form-group">
                                <label for="end_date">Дата завершення:</label>
                                <input type="date" class="form-control" id="end_date" name="end_date" required>
                            </div>

                            <div class="form-group">
                                <label for="percentage">Відсоток знижки:</label>
                                <select class="form-control" id="percentage" name="percentage" required>
                                    <option value="10">10%</option>
                                    <option value="15">15%</option>
                                    <option value="20">20%</option>
                                    <option value="25">25%</option>
                                    <option value="30">30%</option>
                                </select>
                            </div>

                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProductModal">
                                Додати товар
                            </button>

                            <div id="selected_product">
                                <!-- Тут буде відображатися вибраний товар -->
                            </div>

                            <button type="submit" name="create_discount" class="btn btn-success">Створити акцію</button>
                        </form>

                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#discountsModal">
                            Перегляд акцій
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Модальне вікно для додавання товару -->
    <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="addProductModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addProductModalLabel">Додати товар</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Назва товару</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        // Отримати унікальні товари з таблиці stocks
                        $productsWithDiscounts = $product->getProductsWithDiscounts();
                        while ($row = mysqli_fetch_assoc($productsWithDiscounts)) {
                            echo "<tr>
                            <td>" . $row['productName'] . "</td>
                            <td><button class='btn btn-primary add-product-btn' data-product-id='" . $row['id'] . "'>Додати</button></td>
                          </tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Модальне вікно для перегляду акцій -->
    <div class="modal fade" id="discountsModal" tabindex="-1" role="dialog" aria-labelledby="discountsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="discountsModalLabel">Перегляд акцій</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Товар</th>
                            <th>Стара ціна</th>
                            <th>Нова ціна</th>
                            <th>Відсоток знижки</th>
                            <th>Дата початку</th>
                            <th>Дата завершення</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $discounts = $price->getAllDiscounts();
                        foreach ($discounts as $discount) {
                            echo "<tr>
                            <td>" . $discount['productName'] . "</td>
                            <td>" . $discount['old_price'] . "</td>
                            <td>" . $discount['new_price'] . "</td>
                            <td>" . $discount['percentage'] . "%</td>
                            <td>" . $discount['date_start'] . "</td>
                            <td>" . $discount['date_finish'] . "</td>
                          </tr>";
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            // Обробник події для кнопки "Додати товар"
            $('.add-product-btn').click(function () {
                var productId = $(this).data('product-id');
                var productName = $(this).closest('tr').find('td:first-child').text();

                // Отримати стару ціну з таблиці price
                $.ajax({
                    url: '<?php echo $_SERVER['PHP_SELF']; ?>',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        product_id: productId,
                        get_old_price: true
                    },
                    success: function (data) {
                        var oldPrice = data.old_price;
                        var pdvPrice = data.pdv_price;

                        // Отримати відсоток знижки
                        var percentage = $('#percentage').val();

                        // Обчислити нову ціну зі знижкою та ПДВ
                        var newPrice = oldPrice * (1 - percentage / 100) * 1.2;

                        // Додати вибраний товар до списку
                        var productHtml = '<div class="form-group">' +
                            '<label>' + productName + '</label>' +
                            '<div class="input-group">' +
                            '<div class="input-group-prepend">' +
                            '<span class="input-group-text">Стара ціна з ПДВ:</span>' +
                            '</div>' +
                            '<input type="text" class="form-control" value="' + pdvPrice + '" disabled>' +
                            '<div class="input-group-append">' +
                            '<span class="input-group-text">Нова ціна:</span>' +
                            '</div>' +
                            '<input type="text" class="form-control" value="' + newPrice.toFixed(2) + '" disabled>' +
                            '<input type="hidden" name="product_id" value="' + productId + '">' +
                            '</div>' +
                            '</div>';

                        $('#selected_product').html(productHtml);
                        $('#addProductModal').modal('hide');
                    },
                    error: function () {
                        alert('Помилка при отриманні старої ціни');
                    }
                });
            });

            // Оновлення нової ціни при зміні відсотка знижки
            $('#percentage').change(function () {
                var oldPrice = parseFloat($('#selected_product input:hidden[name="product_id"]').data('old-price'));
                var percentage = parseFloat($(this).val());

                if (!isNaN(oldPrice)) {
                    var newPrice = oldPrice * (1 - percentage / 100) * 1.2;
                    $('#selected_product input[type="text"]:last').val(newPrice.toFixed(2));
                }
            });
        });
    </script>
    </body>
    </html>
<?php } ?>