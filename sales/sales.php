<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once '../func/sales.php';
require_once '../func/product.php';

use Mpdf\Mpdf;

$sales = new Sales($con);
$product = new Product($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    if (isset($_POST['add_sale'])) {
        $date = $currentTime;
        $products = $_POST['products'];
        $payment = $_POST['payment'];

        $sales->addSale($date, json_decode($products, true), $payment);
        $sales->updateStocks(json_decode($products, true));
        $_SESSION['msg'] = "Продаж додано!";
    }

    if (isset($_GET['del'])) {
        $sales->deleteSale($_GET['id']);
        $_SESSION['delmsg'] = "Продаж видалено!";
    }

    if (isset($_GET['generate_receipt'])) {
        $saleId = $_GET['id'];

        // Отримання даних про продаж
        $sale = $sales->getSale($saleId);

        // Отримання даних про магазин
        $query = mysqli_query($con, "SELECT * FROM data_store");
        $store = mysqli_fetch_assoc($query);

        // Розпакування JSON з продуктами
        $products = json_decode($sale['products'], true);

        // Створення об'єкту mPDF
        $mpdf = new Mpdf();

        // Генерація HTML для чеку
        $html = '<h2>' . $store['full_name'] . '</h2>';
        $html .= '<p>Адреса: ' . $store['location_address'] . '</p>';
        $html .= '<hr>';

        $total = 0;
        $totalDiscount = 0;
        $totalPdv = 0;

        foreach ($products as $item) {
            $productId = $item['id_product'];
            $productData = $product->getProductById($productId);

            $quantity = $item['quantity'];
            $price = $item['price'];
            $pdv = $item['pdv'];
            $unitsId = $productData['units_id'];

            $html .= '<p>' . $productData['productName'] . ' - Кількість: ' . $quantity . ', Ціна: ' . $price . ' грн/' . $product->getUnitName($unitsId);

            if (isset($item['discount_price']) && !empty($item['discount_price'])) {
                $discountPrice = $item['discount_price'];
                $discount = ($price - $discountPrice) * $quantity;
                $html .= ', Знижка: ' . $discount . ' грн.';
                $totalDiscount += $discount;
                $total += ($discountPrice * $quantity);
            } else {
                $total += ($price * $quantity);
            }

            $html .= '</p>';

            $totalPdv += $pdv;
        }

        $html .= '<hr>';
        $html .= '<p>Загальна знижка: ' . $totalDiscount . ' грн.</p>';
        $html .= '<p>Загальна сума: ' . $total . ' грн.</p>';
        $html .= '<p>ПДВ А 20%: ' . $totalPdv . ' грн.</p>';
        $html .= '<p>Спосіб оплати: ' . $sale['payment'] . ', Сума: ' . $total . ' грн.</p>';

        // Генерація PDF
        $mpdf->WriteHTML($html);
        $mpdf->Output('receipt.pdf', 'I');
        exit();
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Продажі</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Продажі</h3>
                    </div>

                    <div class="module-body">

                        <div class="control-group">
                            <a href="../index.php" class="btn btn-dark">Додати продаж</a>
                        </div>


                        <div class="control-group">
                        <?php if (isset($_POST['add_sale'])) { ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Все добре!</strong> <?php echo htmlentities($_SESSION['msg']); ?><?php echo htmlentities($_SESSION['msg'] = ""); ?>
                            </div>
                            <?php unset($_SESSION['msg']); ?>
                        <?php } ?>

                        <?php if (isset($_GET['del'])) { ?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong></strong> <?php echo htmlentities($_SESSION['delmsg']); ?><?php echo htmlentities($_SESSION['delmsg'] = ""); ?>
                            </div>
                            <?php unset($_SESSION['delmsg']); ?>
                        <?php } ?>

                        <br/>

                    </div>
                </div>

                <div class="modal fade" id="addSaleModal" tabindex="-1" aria-labelledby="addSaleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="addSaleModalLabel">Додати продаж</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <form class="form-horizontal row-fluid" name="addSale" method="post">
                                <div class="modal-body">
                                    <div class="control-group">
                                        <label class="control-label" for="basicinput">Продукти</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Продукти" name="products"
                                                   class="span8 tip" required>
                                        </div>
                                        <label class="control-label" for="basicinput">Оплата</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Оплата" name="payment" class="span8 tip"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Закрити
                                    </button>
                                    <button type="submit" name="add_sale" class="btn btn-dark">Додати</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>


            <div class="module">
                <div class="module-head">
                    <h3>Продажі</h3>
                </div>
                <div class="module-body table">
                    <table cellpadding="0" cellspacing="0" border="0"
                           class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Дата</th>
                            <th>Оплата</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        $query = $sales->getAllSales();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['date']); ?></td>
                                <td><?php echo htmlentities($row['payment']); ?></td>
                                <td>
                                    <a href="sales.php?id=<?php echo $row['id'] ?>&del=delete"
                                       onClick="return confirm('Ви дійсно хочете видалити цей продаж?')"><i
                                                class="icon-remove-sign"></i></a>
                                    <a href="sales.php?id=<?php echo $row['id'] ?>&generate_receipt=true" target="_blank"
                                       class="btn btn-primary btn-sm">Чек</a>
                                </td>
                            </tr>
                            <?php $cnt = $cnt + 1;
                        } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../scripts/datatables/jquery.dataTables.js"></script>
    <script src="../scripts/menu.js"></script>
    <script>
        $(document).ready(function () {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        });
    </script>
    </body>
    </html>
<?php } ?>