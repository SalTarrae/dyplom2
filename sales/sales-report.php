<?php
session_start();
require_once dirname(__DIR__) . '/func/login.php';
include("../include/config.php");
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once '../func/sales.php';
require_once '../func/product.php';
require_once '../func/price.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentDate = date('Y-m-d');

    // Якщо форма не була відправлена, встановлюємо дату за замовчуванням
    if (!isset($_POST['submit'])) {
        $_POST['date'] = $currentDate;
    }

    $date = $_POST['date'];
    $salesObj = new Sales($con);
    $productObj = new Product($con);
    $priceObj = new Price($con);

    // Отримання даних про продані товари за вибраною датою
    $result = $salesObj->getSalesReport($date);

    // Групування товарів та розрахунок сумарних значень
    $groupedProducts = array();

    while ($row = mysqli_fetch_assoc($result)) {
        $article = $row['article'];
        $productName = $row['productName'];
        $unitsId = $row['units_id'];
        $productsJson = $row['products'];
        $products = json_decode($productsJson, true);

        $unitsName = $productObj->getUnitName($unitsId);

        foreach ($products as $product) {
            $productId = $product['id_product'];
            $productQuantity = $product['quantity'];
            $price = $product['price'];
            $discountPrice = $product['discount_price'];
            $sale = $product['sale'];

            if (!isset($groupedProducts[$productId])) {
                $groupedProducts[$productId] = array(
                    'article' => $article,
                    'productName' => $productName,
                    'unitsName' => $unitsName,
                    'quantity' => 0,
                    'totalAmount' => 0,
                    'totalDiscount' => 0,
                    'totalAmountWithDiscount' => 0,
                    'totalCost' => 0
                );
            }

            $groupedProducts[$productId]['quantity'] += $productQuantity;

            if ($discountPrice !== "") {
                $groupedProducts[$productId]['totalAmount'] += $price * $productQuantity;
                $groupedProducts[$productId]['totalDiscount'] += $sale * $productQuantity;
                $groupedProducts[$productId]['totalAmountWithDiscount'] += $discountPrice * $productQuantity;
            } else {
                $groupedProducts[$productId]['totalAmount'] += $price * $productQuantity;
                $groupedProducts[$productId]['totalAmountWithDiscount'] += $price * $productQuantity;
            }


            // Отримання собівартості товару
            $costData = $priceObj->getProductCostPrice($productId);
            $costPrice = $costData['cost_price'];
            $marginPrice = $costData['margin_price'];


            $groupedProducts[$productId]['totalCost'] += $costPrice * $productQuantity;
            $groupedProducts[$productId]['marginPrice'] = $marginPrice;
        }
    }

    // Створення HTML-таблиці зі звітом
    $html = '<html>
                <head>
                    <style>
                        table {
                            border-collapse: collapse;
                            width: 100%;
                        }
                        th, td {
                            border: 1px solid black;
                            padding: 8px;
                            text-align: left;
                        }
                    </style>
                </head>
                <body>
                    <h2>Звіт по продажам за ' . $date . '</h2>
                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                            <tr>
                                <th>Артикул</th>
                                <th>Назва товару</th>
                                <th>Одиниця виміру</th>
                                <th>Продана кількість</th>
                                <th>Сума продажу</th>
                                <th>Надана знижка</th>
                                <th>Сума продажу з урахуванням знижки</th>
                                <th>Собівартість проданого товару</th>
                                <th>Маржинальний дохід</th>
                                <th>Маржинальний дохід з ПДВ</th>
                            </tr>
                        </thead>
                        <tbody>';

    foreach ($groupedProducts as $productId => $product) {
        $article = $product['article'];
        $productName = $product['productName'];
        $unitsName = $product['unitsName'];
        $quantity = $product['quantity'];
        $totalAmount = $product['totalAmount'];
        $totalDiscount = $product['totalDiscount'];
        $totalAmountWithDiscount = $product['totalAmountWithDiscount'];
        $totalCost = $product['totalCost'];
        $marginPrice = $product['marginPrice'];

        // Розрахунок маржинального доходу
        $pdv = $marginPrice * 0.2;
        $marginProfit = $totalAmountWithDiscount - $totalCost - $pdv;
        $marginProfitWithPDV = $totalAmountWithDiscount - $totalCost + $pdv;

        $html .= '<tr>
                    <td>' . $article . '</td>
                    <td>' . $productName . '</td>
                    <td>' . $unitsName . '</td>
                    <td>' . $quantity . '</td>
                    <td>' . number_format($totalAmount, 2) . '</td>
                    <td>' . number_format($totalDiscount, 2) . '</td>
                    <td>' . number_format($totalAmountWithDiscount, 2) . '</td>
                    <td>' . number_format($totalCost, 2) . '</td>
                    <td>' . number_format($marginProfit, 2) . '</td>
                    <td>' . number_format($marginProfitWithPDV, 2) . '</td>
                  </tr>';
    }

    $html .= '</tbody>
            </table>
        </body>
    </html>';

    // Генерація PDF-файлу за допомогою MPDF
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->WriteHTML($html);
    $pdfContent = $mpdf->Output('', 'S');

    // Збереження HTML та PDF у сесії
    $_SESSION['salesReportHtml'] = $html;
    $_SESSION['salesReportPdf'] = $pdfContent;
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Звіт по продажам</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Звіт по продажам</h3>
                    </div>

                    <div class="module-body">
                        <form class="form-horizontal row-fluid" name="SalesReport" method="post">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Дата</label>
                                <div class="controls">
                                    <input type="date" name="date" class="span8 tip" value="<?php echo $date; ?>" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" name="submit" class="btn btn-dark">Показати звіт</button>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <?php echo $_SESSION['salesReportHtml']; ?>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <a href="../index.php" target="_blank" class="btn btn-dark">Завантажити звіт</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php
}
?>