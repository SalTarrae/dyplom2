<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/counterparties.php';
require_once '../func/price.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());
    $counterpartiesObj = new Counterparties($con);
    $priceObj = new Price($con);

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['showPriceList'])) {
        $counterpartyId = $_POST['counterpartyView'];
        $priceList = $priceObj->getPriceListByCounterparty($counterpartyId);
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES['jsonFile'])) {
        $counterpartyId = $_POST['counterparty'];
        $jsonFile = $_FILES['jsonFile']['tmp_name'];
        $jsonData = file_get_contents($jsonFile);
        $error = false;
        $errorMessage = '';

        $data = json_decode($jsonData, true);
        if ($data === null) {
            $error = true;
            $errorMessage = "Некоректний формат JSON-файлу.";
        }

        if (!$error) {
            $duplicateErrors = $priceObj->processPriceListData($data, $counterpartyId);
            if (!empty($duplicateErrors)) {
                $_SESSION['duplicate_error'] = $duplicateErrors;
            }
        } else {
            $_SESSION['error'] = $errorMessage;
        }
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Надходження</title>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
              rel='stylesheet'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <?php if (!$user->hasRole('manager')) { ?>
                    <div class="module">
                        <div class="module-head">
                            <h3>Додавання прайс-лісту</h3>
                        </div>
                        <div class="module-body">
                            <form method="post" enctype="multipart/form-data" class="form-horizontal row-fluid">
                                <div class="control-group">
                                    <label class="control-label" for="counterparty">Оберіть постачальника:</label>
                                    <div class="controls">
                                        <select class="span8" id="counterparty" name="counterparty">
                                            <?php
                                            $counterpartiesQuery = $counterpartiesObj->getAllCounterparties();
                                            while ($row = mysqli_fetch_assoc($counterpartiesQuery)) {
                                                echo "<option value='" . $row['id'] . "'>" . $row['full_name'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="jsonFile">Завантажте JSON-файл:</label>
                                    <div class="controls">
                                        <input type="file" class="span8" id="jsonFile" name="jsonFile" accept=".json">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <div class="controls">
                                        <button type="submit" class="btn btn-dark">Додати прайс-лист</button>
                                    </div>
                                </div>
                            </form>
                            <?php if (isset($_SESSION['msg'])) { ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>Все добре!</strong> <?php echo htmlentities($_SESSION['msg']); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php unset($_SESSION['msg']); } ?>
                            <?php if (isset($_SESSION['error'])) { ?>
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>Помилка!</strong> <?php echo htmlentities($_SESSION['error']); ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <?php unset($_SESSION['error']); } ?>
                            <?php
                            if (isset($_SESSION['duplicate_error'])) {
                                foreach ($_SESSION['duplicate_error'] as $error) {
                                    echo "<div class='alert alert-warning alert-dismissible fade show' role='alert'>
                                  <strong>Увага!</strong> $error
                                  <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                      <span aria-hidden='true'>&times;</span>
                                  </button>
                              </div>";
                                }
                                unset($_SESSION['duplicate_error']);
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="module">
                    <div class="module-head">
                        <h3>Перегляд прайс-лісту</h3>
                    </div>
                    <div class="module-body">
                        <form method="post">
                            <div class="control-group">
                                <label class="control-label" for="counterpartyView">Оберіть постачальника:</label>
                                <div class="controls">
                                    <select class="span8" id="counterpartyView" name="counterpartyView">
                                        <option value="">-- Оберіть постачальника --</option>
                                        <?php
                                        $counterpartiesQuery = $counterpartiesObj->getAllCounterparties();
                                        while ($row = mysqli_fetch_assoc($counterpartiesQuery)) {
                                            echo "<option value='" . $row['id'] . "'>" . $row['full_name'] . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" name="showPriceList" class="btn btn-primary">Показати таблицю</button>
                                </div>
                            </div>
                        </form>
                        <div id="priceListContainer">
                            <?php if (isset($priceList) && !empty($priceList)) { ?>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Назва товару</th>
                                        <th>Ціна</th>
                                        <th>Одиниця виміру</th>
                                        <th>Постачальник</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $cnt = 1;
                                    foreach ($priceList as $row) {
                                        echo "<tr>";
                                        echo "<td>" . $cnt . "</td>";
                                        echo "<td>" . $row['name_product'] . "</td>";
                                        echo "<td>" . $row['price'] . "</td>";
                                        echo "<td>" . $row['units'] . "</td>";
                                        echo "<td>" . $row['counterparty_name'] . "</td>";
                                        echo "</tr>";
                                        $cnt++;
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            <?php } elseif (isset($priceList) && empty($priceList)) { ?>
                                <p>Немає даних для відображення.</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php
}
?>