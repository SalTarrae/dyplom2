<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../vendor/autoload.php';
require_once '../func/order.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());
    $orderObj = new Order($con);

    if (isset($_GET['ajax'])) {
        $period = isset($_GET['period']) ? $_GET['period'] : 'all';
        $year = isset($_GET['year']) ? $_GET['year'] : null;
        $month = isset($_GET['month']) ? $_GET['month'] : null;

        $orders = $orderObj->getOrders($period, $year, $month);
        $tableHtml = $orderObj->generateOrdersTable($orders);
        $totalHtml = $orderObj->generateTotalHtml($orders);

        echo json_encode(['table' => $tableHtml, 'total' => $totalHtml]);
        exit();
    }

    if (isset($_GET['pdf'])) {
        $period = isset($_GET['period']) ? $_GET['period'] : 'all';
        $year = isset($_GET['year']) ? $_GET['year'] : null;
        $month = isset($_GET['month']) ? $_GET['month'] : null;

        $orders = $orderObj->getOrders($period, $year, $month);
        $html = $orderObj->generatePdfContent($orders);

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);

        $filename = 'delivery_control_report.pdf';

        if (isset($_GET['download'])) {
            $mpdf->Output($filename, 'D');
        } else {
            $mpdf->Output($filename, 'I');
        }
        exit();
    }
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Контроль поставок</title>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600' rel='stylesheet'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module-head">
                    <h3>Контроль поставок</h3>
                </div>
                <div class="module-body">
                    <form id="deliveryForm">
                        <div class="form-group">
                            <label for="period">Період:</label>
                            <select class="form-control" id="period" name="period">
                                <option value="all">Весь період</option>
                                <option value="year">Рік</option>
                                <option value="month">Місяць</option>
                                <option value="week">Тиждень</option>
                                <option value="day">День</option>
                            </select>
                        </div>
                        <div class="form-group" id="yearSelect" style="display: none;">
                            <label for="year">Рік:</label>
                            <select class="form-control" id="year" name="year">
                                <?php
                                $currentYear = date('Y');
                                for ($year = $currentYear; $year >= 2000; $year--) {
                                    echo "<option value='$year'>$year</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group" id="monthSelect" style="display: none;">
                            <label for="month">Місяць:</label>
                            <select class="form-control" id="month" name="month">
                                <?php
                                $currentMonth = date('m');
                                for ($month = 1; $month <= 12; $month++) {
                                    $selected = ($month == $currentMonth) ? 'selected' : '';
                                    echo "<option value='$month' $selected>" . date('F', mktime(0, 0, 0, $month, 1)) . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Показати</button>
                        <button type="button" class="btn btn-secondary" id="printButton">Друк звіту</button>
                        <button type="button" class="btn btn-success" id="downloadButton">Завантажити звіт</button>
                    </form>
                    <hr>
                    <div id="tableContainer"></div>
                    <div id="totalContainer"></div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#period').on('change', function () {
                var selectedPeriod = $(this).val();
                if (selectedPeriod === 'year') {
                    $('#yearSelect').show();
                    $('#monthSelect').hide();
                } else if (selectedPeriod === 'month') {
                    $('#yearSelect').show();
                    $('#monthSelect').show();
                } else {
                    $('#yearSelect').hide();
                    $('#monthSelect').hide();
                }
            });

            $('#deliveryForm').on('submit', function (e) {
                e.preventDefault();
                var formData = $(this).serialize();
                $.ajax({
                    url: '?ajax=1',
                    type: 'GET',
                    data: formData,
                    dataType: 'json',
                    success: function (response) {
                        $('#tableContainer').html(response.table);
                        $('#totalContainer').html(response.total);
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $('#printButton').on('click', function () {
                var formData = $('#deliveryForm').serialize();
                window.open('?pdf=1&' + formData, '_blank');
            });

            $('#downloadButton').on('click', function () {
                var formData = $('#deliveryForm').serialize();
                window.location.href = '?pdf=1&download=1&' + formData;
            });

            // Trigger form submission on page load
            $('#deliveryForm').trigger('submit');
        });
    </script>
    </body>
    </html>

    <?php
}
?>