<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/receipts.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev'); // change according timezone
    $currentTime = date('d-m-Y h:i:s A', time());
    $receiptsObj = new Receipts($con);

    if (isset($_GET['del'])) {
        $receiptId = $_GET['del'];
        if ($receiptsObj->deleteReceipt($receiptId)) {
            $_SESSION['delmsg'] = "Надходження товару успішно видалено";
        } else {
            $_SESSION['delmsg'] = "Помилка при видаленні надходження товару";
        }
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Надходження</title>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link type="text/css" href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600'
              rel='stylesheet'>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Надходження</h3>
                    </div>
                    <div class="module-body">
                        <div class="control-group">
                            <div class="controls">
                                <a href="../index.php" class="btn btn-dark">Додати надходження</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="module-body table">
                    <?php if (isset($_SESSION['delmsg'])) { ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Oh snap!</strong> <?php echo htmlentities($_SESSION['delmsg']); ?><?php $_SESSION['delmsg'] = ""; ?>
                        </div>
                    <?php } ?>

                    <br/>

                    <table cellpadding="0" cellspacing="0" border="0"
                           class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Номер надходження</th>
                            <th>Замовлення</th>
                            <th>Склад</th>
                            <th>Дата</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $receiptsQuery = $receiptsObj->getReceipts();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($receiptsQuery)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td>Надходження №<?php echo htmlentities($row['number']); ?></td>
                                <td>Замовлення №<?php echo htmlentities($row['order_number']); ?></td>
                                <td><?php echo htmlentities($row['warehouse_name']); ?></td>
                                <td><?php echo htmlentities($row['date']); ?></td>
                                <td>
                                    <a href="all_goods_receipt.php?del=<?php echo $row['id'] ?>"
                                       class="btn btn-danger btn-sm"
                                       onClick="return confirm('Are you sure you want to delete?')">
                                        <i class="bi bi-trash"></i> Видалити
                                    </a>
                                    <a href="../generate_goods_receipt_report.php?id=<?php echo $row['id'] ?>"
                                       target="_blank"
                                       class="btn btn-primary btn-sm">
                                        <i class="bi bi-file-earmark-text"></i> Документ
                                    </a>
                                </td>
                            </tr>

                            <?php $cnt = $cnt + 1;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        });
    </script>
    </body>
    </html>
<?php } ?>