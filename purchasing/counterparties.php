<?php
session_start();
require_once '../func/login.php';
include("../include/config.php");
require_once '../func/counterparties.php';

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} elseif ($user->hasRole('manager')) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());
    $counterpartiesObj = new Counterparties($con);

    if (isset($_GET['del'])) {
        $counterpartyId = $_GET['id'];
        if ($counterpartiesObj->deleteCounterparty($counterpartyId)) {
            $_SESSION['delmsg'] = "Контрагента видалено";
        } else {
            $_SESSION['delmsg'] = "Помилка при видаленні контрагента";
        }
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["update_counterparty"])) {
        $id = $_POST["id"];
        $fullName = $_POST["full_name"];
        $mobilePhone = $_POST["mobile_phone"];
        $email = $_POST["email"];
        $other = $_POST["other"];
        $note = $_POST["note"];
        $country = $_POST["country"];
        $registerCode = $_POST["register_code"];
        $payerCode = $_POST["payer_code"];
        $legalAddress = $_POST["legal_address"];
        $postalAddress = $_POST["postal_address"];
        $locationAddress = $_POST["location_address"];

        if ($counterpartiesObj->updateCounterparty($id, $fullName, $mobilePhone, $email, $other, $note, $country, $registerCode, $payerCode, $legalAddress, $postalAddress, $locationAddress)) {
            echo "Зміни збережено успішно";
        } else {
            echo "Помилка при збереженні змін: " . mysqli_error($con);
        }

        exit();
    }

    if (isset($_POST['get_counterparty_data'])) {
        $counterpartyId = $_POST['counterparty_id'];
        $counterparty = $counterpartiesObj->getCounterpartyById($counterpartyId);
        echo json_encode($counterparty);
        exit();
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Постачальники</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="../css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="../images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Контрагенти</h3>
                    </div>
                    <div class="module-body">
                        <div class="control-group">
                            <div class="controls">
                                <a href="../index.php" class="btn btn-dark">Створити нового контрагента</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="module-body table">
                    <?php if (isset($_GET['del'])) { ?>
                        <div class="alert alert-error">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>Oh snap!</strong> <?php echo htmlentities($_SESSION['delmsg']); ?><?php echo htmlentities($_SESSION['delmsg'] = ""); ?>
                        </div>
                    <?php } ?>

                    <br />

                    <table cellpadding="0" cellspacing="0" border="0" class="datatable-1 table table-bordered table-striped display" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Назва/Ім'я</th>
                            <th>Мобільний телефон</th>
                            <th>Email</th>
                            <th>Інше</th>
                            <th>Помітки</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $counterpartiesQuery = $counterpartiesObj->getAllCounterparties();
                        $cnt = 1;
                        while ($row = mysqli_fetch_array($counterpartiesQuery)) {
                            ?>
                            <tr>
                                <td><?php echo htmlentities($cnt); ?></td>
                                <td><?php echo htmlentities($row['full_name']); ?></td>
                                <td><?php echo htmlentities($row['mobile_phone']); ?></td>
                                <td><?php echo htmlentities($row['email']); ?></td>
                                <td><?php echo htmlentities($row['other']); ?></td>
                                <td><?php echo htmlentities($row['note']); ?></td>
                                <td>
                                    <a href="#" class="edit-counterparty" data-toggle="modal" data-target="#editCounterpartyModal" data-counterparty-id="<?php echo $row['id'] ?>">
                                        <i class="icon-edit"></i>
                                    </a>
                                    <a href="counterparties.php?id=<?php echo $row['id'] ?>&del=delete" onClick="return confirm('Are you sure you want to delete?')">
                                        <i class="icon-remove-sign"></i>
                                    </a>
                                </td>
                            </tr>
                            <?php $cnt = $cnt + 1;
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editCounterpartyModal" tabindex="-1" role="dialog" aria-labelledby="editCounterpartyModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="editCounterpartyModalLabel">Редагувати контрагента</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="editCounterpartyForm">
                        <input type="hidden" id="editCounterpartyId" name="id">
                        <div class="form-group">
                            <label for="editFullName">Назва/Ім'я</label>
                            <input type="text" class="form-control" id="editFullName" name="full_name">
                        </div>
                        <div class="form-group">
                            <label for="editMobilePhone">Мобільний телефон</label>
                            <input type="text" class="form-control" id="editMobilePhone" name="mobile_phone">
                        </div>
                        <div class="form-group">
                            <label for="editEmail">Email</label>
                            <input type="email" class="form-control" id="editEmail" name="email">
                        </div>
                        <div class="form-group">
                            <label for="editOther">Інше</label>
                            <input type="text" class="form-control" id="editOther" name="other">
                        </div>
                        <div class="form-group">
                            <label for="editNote">Помітки</label>
                            <textarea class="form-control" id="editNote" name="note"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="editCountry">Країна</label>
                            <input type="text" class="form-control" id="editCountry" name="country">
                        </div>
                        <div class="form-group">
                            <label for="editRegisterCode">Реєстраційний код</label>
                            <input type="text" class="form-control" id="editRegisterCode" name="register_code">
                        </div>
                        <div class="form-group">
                            <label for="editPayerCode">Код платника</label>
                            <input type="text" class="form-control" id="editPayerCode" name="payer_code">
                        </div>
                        <div class="form-group">
                            <label for="editLegalAddress">Юридична адреса</label>
                            <input type="text" class="form-control" id="editLegalAddress" name="legal_address">
                        </div>
                        <div class="form-group">
                            <label for="editPostalAddress">Поштова адреса</label>
                            <input type="text" class="form-control" id="editPostalAddress" name="postal_address">
                        </div>
                        <div class="form-group">
                            <label for="editLocationAddress">Адреса знаходження</label>
                            <input type="text" class="form-control" id="editLocationAddress" name="location_address">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                    <button type="button" class="btn btn-primary" id="saveChangesBtn">Зберегти зміни</button>
                </div>
            </div>
        </div>
    </div>

    <script src="../scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../scripts/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="../scripts/datatables/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {
            $('.datatable-1').dataTable();
            $('.dataTables_paginate').addClass("btn-group datatable-pagination");
            $('.dataTables_paginate > a').wrapInner('<span />');
            $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
            $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
        });
    </script>

    <script>
        $(document).ready(function() {
            $('.edit-counterparty').on('click', function() {
                var counterpartyId = $(this).data('counterparty-id');

                $.ajax({
                    url: '',
                    method: 'POST',
                    data: { get_counterparty_data: true, counterparty_id: counterpartyId },
                    dataType: 'json',
                    success: function(response) {
                        $('#editCounterpartyId').val(response.id);
                        $('#editFullName').val(response.full_name);
                        $('#editMobilePhone').val(response.mobile_phone);
                        $('#editEmail').val(response.email);
                        $('#editOther').val(response.other);
                        $('#editNote').val(response.note);
                        $('#editCountry').val(response.country);
                        $('#editRegisterCode').val(response.register_code);
                        $('#editPayerCode').val(response.payer_code);
                        $('#editLegalAddress').val(response.legal_address);
                        $('#editPostalAddress').val(response.postal_address);
                        $('#editLocationAddress').val(response.location_address);
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });

            $('#saveChangesBtn').on('click', function() {
                var form = $('#editCounterpartyForm');
                var formData = form.serialize();

                $.ajax({
                    url: '',
                    method: 'POST',
                    data: formData + '&update_counterparty=true',
                    success: function(response) {
                        $('#editCounterpartyModal').modal('hide');
                        location.reload();
                    },
                    error: function(xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            });
        });
    </script>
    </body>
    </html>
<?php } ?>