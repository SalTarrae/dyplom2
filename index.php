<?php
session_start();
require_once './func/login.php';
include("./include/config.php");

if ($user->isLoggedIn()) {
    header("Location: dashboard.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Адмін-панель</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            background-color: #f8f9fa;
        }
        .login-container {
            margin-top: 100px;
        }
        .login-form {
            background-color: #fff;
            padding: 30px;
            border-radius: 5px;
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
        }
        .login-form h3 {
            text-align: center;
            margin-bottom: 30px;
            color: #333;
        }
        .login-form .form-control {
            border-radius: 3px;
        }
        .login-form .btn-primary {
            width: 100%;
            border-radius: 3px;
            padding: 12px;
            font-size: 16px;
        }
        .login-form .error-message {
            color: #dc3545;
            margin-bottom: 15px;
        }
    </style>
</head>
<body>

<div class="container login-container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="login-form">
                <h3>Вхід в систему</h3>
                <form method="post">
                    <?php if (isset($_SESSION['errmsg'])): ?>
                        <div class="error-message"><?php echo htmlentities($_SESSION['errmsg']); ?></div>
                        <?php unset($_SESSION['errmsg']); ?>
                    <?php endif; ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Пароль" required>
                    </div>
                    <button type="submit" class="btn btn-primary" name="submitLogin">Увійти</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>