<?php
session_start();
require_once 'func/login.php';
require_once 'func/receipts.php';
require_once 'func/order.php';
require_once 'func/price.php';
include("include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev'); // change according timezone
    $currentTime = date('d-m-Y h:i:s A', time());
    $receipts = new Receipts($con);
    $order = new Order($con);
    $price = new Price($con);

    if (isset($_POST['submit'])) {
        $date = $_POST['date'];
        $orderId = $_POST['order'];
        $counterpartyId = $_POST['counterparty_id'];
        $warehouseId = $_POST['warehouse_id'];

        $products = array();
        foreach ($_POST['quantity'] as $productId => $quantity) {
            $price = $_POST['price'][$productId];
            $expiryDate = $_POST['expiry_date'][$productId];

            $products[] = array(
                'id' => $productId,
                'quantity' => $quantity,
                'price' => $price,
                'expiry_date' => $expiryDate
            );
        }

        $goodsReceiptId = $receipts->addReceipt($date, $orderId, $warehouseId, $counterpartyId, $products);

        if ($goodsReceiptId) {
            header("Location: goods_receipt.php?success=1");
            exit();
        } else {
            echo "Error inserting goods receipt: " . mysqli_error($con);
            exit();
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Надходження товару</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="module-head">
                <h3>Надходження товару</h3>
            </div>
            <div class="module-body">
                <form method="post" action="">
                    <div class="form-group">
                        <label for="date">Дата:</label>
                        <input type="date" class="form-control" id="date" name="date" required>
                    </div>
                    <div class="form-group">
                        <label for="order">Замовлення:</label>
                        <select class="form-control" id="order" name="order" required>
                            <option value="">Виберіть замовлення</option>
                            <?php
                            $orderQuery = $order->getAllOrders();
                            while ($orderRow = mysqli_fetch_array($orderQuery)) {
                                ?>
                                <option value="<?php echo $orderRow['id']; ?>"><?php echo "Замовлення №" . $orderRow['number'] . " від " . $orderRow['date']; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div id="orderDetails">
                        <!-- Order details will be loaded here -->
                    </div>
                    <button type="submit" class="btn btn-primary" name="submit">Зберегти</button>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#order').on('change', function () {
            var orderId = $(this).val();
            if (orderId) {
                $.ajax({
                    url: 'get_order_details.php',
                    type: 'POST',
                    data: {orderId: orderId},
                    success: function (response) {
                        $('#orderDetails').html(response);
                    },
                    error: function (xhr, status, error) {
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('#orderDetails').html('');
            }
        });
    });
</script>
</body>
</html>