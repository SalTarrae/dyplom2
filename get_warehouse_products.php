<?php

include("include/config.php");

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $id_warehouse = $_POST['id_warehouse'];

    $query = "SELECT p.productName, s.quantity
              FROM stock s
              JOIN products p ON s.id_product = p.id
              WHERE s.id_warehouse = '$id_warehouse'";
    $result = mysqli_query($con, $query);

    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            echo '<div class="product-item">
                    <span>' . $row['productName'] . '</span>
                    <span>Кількість: ' . $row['quantity'] . '</span>
                    <button type="button" class="btn btn-sm btn-primary add-product">Додати</button>
                  </div>';
        }
    } else {
        echo 'Немає доступних товарів на цьому складі.';
    }
}
