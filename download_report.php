<?php
session_start();

if (isset($_SESSION['salesReportPdf'])) {
    $pdfContent = $_SESSION['salesReportPdf'];

    // Відправлення PDF-файлу як відповіді на запит завантаження
    header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename="sales_report.pdf"');
    header('Content-Length: ' . strlen($pdfContent));

    echo $pdfContent;
    exit();
}
?>
