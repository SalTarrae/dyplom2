<?php
session_start();
require_once 'func/login.php';
require_once 'func/product.php';
include("include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
}

$pid = intval($_GET['id']); // product id
$product = new Product($con);

if (isset($_POST['submit'])) {
    $category = $_POST['category'];
    $article = $_POST['article'];
    $subcat = $_POST['subcategory'];
    $productname = $_POST['productName'];
    $productdescription = $_POST['productDescription'];
    $trade_mark = $_POST['trade_mark'];
    $units = $_POST['units'];

    if ($product->updateProduct($pid, $category, $subcat, $article, $productname, $trade_mark, $units, $productdescription)) {
        $_SESSION['msg'] = "Product Updated Successfully !!";
        header("Location: manage-products.php");
        exit();
    } else {
        $_SESSION['error'] = "Failed to Update Product !!";
    }
}

$productDetails = $product->getProductById($pid);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>StockUp | Редагування товару</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
    <link type="text/css" href="css/theme.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
</head>
<body>
<div class="container-fluid">
    <div class="row flex-nowrap sticky-lg-top">
        <?php include('include/side.php'); ?>
        <div class="col py-3">
            <div class="span9">
                <div class="content">
                    <div class="module">
                        <div class="module-head">
                            <h3>Редагувати товар</h3>
                        </div>
                        <div class="module-body">
                            <?php if (isset($_SESSION['error'])) { ?>
                                <div class="alert alert-error">
                                    <button type="button" class="close" data-dismiss="alert">×</button>
                                    <strong>Oh snap!</strong> <?php echo htmlentities($_SESSION['error']); ?>
                                    <?php unset($_SESSION['error']); ?>
                                </div>
                            <?php } ?>

                            <br />

                            <form class="form-horizontal row-fluid" name="insertproduct" method="post" enctype="multipart/form-data">
                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Категорія</label>
                                    <div class="controls">
                                        <select name="category" class="span8 tip" onChange="getSubcat(this.value);" required>
                                            <?php
                                            $categoryQuery = $product->getAllCategories();
                                            while ($row = mysqli_fetch_array($categoryQuery)) {
                                                $selected = ($row['id'] == $productDetails['category']) ? 'selected' : '';
                                                echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['categoryName'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Підкатегорія</label>
                                    <div class="controls">
                                        <select name="subcategory" id="subcategory" class="span8 tip" required>
                                            <?php
                                            $subcategoryQuery = $product->getSubcategoriesByCategoryId($productDetails['category']);
                                            while ($row = mysqli_fetch_array($subcategoryQuery)) {
                                                $selected = ($row['id'] == $productDetails['subCategory']) ? 'selected' : '';
                                                echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['subcategory'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Назва</label>
                                    <div class="controls">
                                        <input type="text" name="productName" placeholder="Enter Product Name" value="<?php echo $productDetails['productName']; ?>" class="span8 tip" required>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Артікул</label>
                                    <div class="controls">
                                        <input type="text" name="article" placeholder="Enter Article" value="<?php echo $productDetails['article']; ?>" class="span8 tip" required>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Опис</label>
                                    <div class="controls">
                                        <textarea name="productDescription" placeholder="Enter Product Description" rows="6" class="span8 tip" required><?php echo $productDetails['productDescription']; ?></textarea>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Торгова марка</label>
                                    <div class="controls">
                                        <select name="trade_mark" class="span8 tip" required>
                                            <?php
                                            $tradeMarkQuery = $product->getAllTradeMarks();
                                            while ($row = mysqli_fetch_array($tradeMarkQuery)) {
                                                $selected = ($row['id'] == $productDetails['trade_mark']) ? 'selected' : '';
                                                echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['trade_name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label" for="basicinput">Одиниці виміру</label>
                                    <div class="controls">
                                        <select name="units" class="span8 tip" required>
                                            <?php
                                            $unitsQuery =  $product->getAllUnits();
                                            while ($row = mysqli_fetch_array($unitsQuery)) {
                                                $selected = ($row['id'] == $productDetails['units_id']) ? 'selected' : '';
                                                echo '<option value="' . $row['id'] . '" ' . $selected . '>' . $row['name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <div class="controls">
                                        <button type="submit" name="submit" class="btn">Оновити</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
<script src="scripts/datatables/jquery.dataTables.js"></script>
<script>
    function getSubcat(val) {
        $.ajax({
            type: "POST",
            url: "get_subcat.php",
            data:'cat_id='+val,
            success: function(data){
                $("#subcategory").html(data);
            }
        });
    }
</script>
<script>
    $(document).ready(function() {
        $('.datatable-1').dataTable();
        $('.dataTables_paginate').addClass("btn-group datatable-pagination");
        $('.dataTables_paginate > a').wrapInner('<span />');
        $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
        $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
    });
</script>
</body>
</html>