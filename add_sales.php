<?php
session_start();
require_once 'func/login.php';
include("include/config.php");
require_once 'func/sales.php';
require_once 'func/product.php';
require_once 'func/price.php';

$sales = new Sales($con);
$product = new Product($con);
$price = new Price($con);

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    // Видалення акцій, у яких date_finish є до сьогоднішнього дня
    $price->deleteExpiredDiscounts();

    if (isset($_POST['submit'])) {
        $date = $_POST['date'];
        $payment = $_POST['payment'];

        $products = array();

        if (isset($_POST['products']) && is_array($_POST['products'])) {
            foreach ($_POST['products'] as $product) {
                $productId = $product['id'];
                $quantity = $product['quantity'];

                $productData = $price->getProductPriceData($productId);
                $price = $productData['price'];
                $marginPrice = $productData['margin_price'];
                $discountPrice = $productData['discount_price'];
                $discountPercentage = $productData['percentage'];

                if ($discountPercentage !== null) {
                    $pdv = ($price - ($price * ($discountPercentage / 100))) * 0.2;
                    $sale = $marginPrice * ($discountPercentage / 100);
                    $finalPrice = $discountPrice;
                } else {
                    $pdv = $price * 0.2;
                    $discountPrice = "";
                    $sale = "";
                    $finalPrice = $price;
                }

                $products[] = array(
                    'id_product' => $productId,
                    'price' => $price,
                    'quantity' => $quantity,
                    'discount_price' => $discountPrice,
                    'final_price' => $finalPrice,
                    'pdv' => $pdv
                );
            }
        }

        if (!empty($products)) {
            $sales->addSale($date, $products, $payment);
            $_SESSION['msg'] = "Продаж додано!";

            $sales->updateStocks($products);
        } else {
            $_SESSION['error'] = "Будь ласка, виберіть хоча б один товар для додавання продажу.";
        }
    }
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Додавання продажу</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Додавання продажу</h3>
                    </div>

                    <div class="module-body">
                        <form class="form-horizontal row-fluid" name="Sale" method="post">
                            <div class="control-group">
                                <label class="control-label" for="basicinput">Дата</label>
                                <div class="controls">
                                    <input type="date" name="date" class="span8 tip" value="<?php echo date('Y-m-d'); ?>" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Товари</label>
                                <div class="controls">
                                    <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#addProductModal">Додати товар</button>
                                    <table class="table table-bordered table-striped" id="selected-products-table">
                                        <thead>
                                        <tr>
                                            <th>Назва товару</th>
                                            <th>Кількість</th>
                                            <th>Ціна</th>
                                            <th>Дія</th>
                                        </tr>
                                        </thead>
                                        <tbody id="selected-products"></tbody>
                                    </table>
                                    <div id="total-price"></div>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Вид оплати</label>
                                <div class="controls">
                                    <select name="payment" class="span8 tip" required>
                                        <option value="Готівка">Готівка</option>
                                        <option value="Картка">Картка</option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" name="submit" class="btn btn-dark">Додати продаж</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addProductModal" tabindex="-1" aria-labelledby="addProductModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addProductModalLabel">Додати товар</h5>
                    <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Назва товару</th>
                            <th>Ціна</th>
                            <th>Дія</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $addedProducts = array();
                        $productsQuery = $product->getProductsWithDiscounts();
                        while ($row = mysqli_fetch_array($productsQuery)) {
                            if (!in_array($row['id'], $addedProducts)) {
                                $addedProducts[] = $row['id'];
                                $productPriceData = $price->getProductPriceData($row['id']);
                                $productPrice = $productPriceData['discount_price'] !== null ? $productPriceData['discount_price'] : $productPriceData['price'];
                                ?>
                                <tr>
                                    <td><?php echo $row['productName']; ?></td>
                                    <td><?php echo $productPrice; ?></td>
                                    <td>
                                        <button type="button" class="btn btn-dark add-product" data-id="<?php echo $row['id']; ?>" data-name="<?php echo $row['productName']; ?>" data-price="<?php echo $productPrice; ?>">Додати</button>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            var selectedProducts = [];

            $('.add-product').click(function() {
                var productId = $(this).data('id');
                var productName = $(this).data('name');
                var productPrice = $(this).data('price');

                var product = {
                    id: productId,
                    name: productName,
                    price: productPrice,
                    quantity: 1
                };

                var existingProduct = selectedProducts.find(function(p) {
                    return p.id == productId;
                });

                if (existingProduct) {
                    existingProduct.quantity++;
                } else {
                    selectedProducts.push(product);
                }

                updateSelectedProducts();

                $('#addProductModal').modal('hide');
            });

            $(document).on('input', '.product-quantity', function() {
                var productId = $(this).data('id');
                var quantity = parseInt($(this).val());

                for (var i = 0; i < selectedProducts.length; i++) {
                    if (selectedProducts[i].id == productId) {
                        selectedProducts[i].quantity = quantity;
                        break;
                    }
                }

                updateSelectedProducts();
            });

            $(document).on('click', '.remove-product', function() {
                var productId = $(this).data('id');

                selectedProducts = selectedProducts.filter(function(p) {
                    return p.id != productId;
                });

                updateSelectedProducts();
            });

            function updateSelectedProducts() {
                var selectedProductsHtml = '';
                var totalPrice = 0;

                for (var i = 0; i < selectedProducts.length; i++) {
                    var product = selectedProducts[i];
                    selectedProductsHtml += '<tr>';
                    selectedProductsHtml += '<td>' + product.name + '</td>';
                    selectedProductsHtml += '<td><input type="number" class="form-control product-quantity" data-id="' + product.id + '" value="' + product.quantity + '" min="1"></td>';
                    selectedProductsHtml += '<td>' + product.price + '</td>';
                    selectedProductsHtml += '<td><button type="button" class="btn btn-danger remove-product" data-id="' + product.id + '">Видалити</button></td>';
                    selectedProductsHtml += '<input type="hidden" name="products[' + i + '][id]" value="' + product.id + '">';
                    selectedProductsHtml += '<input type="hidden" name="products[' + i + '][quantity]" value="' + product.quantity + '">';
                    selectedProductsHtml += '</tr>';

                    totalPrice += product.price * product.quantity;
                }

                $('#selected-products').html(selectedProductsHtml);
                $('#total-price').html('<p>Загальна сума: ' + totalPrice + '</p>');
            }
        });
    </script>
    </body>
    </html>
    <?php
}
?>