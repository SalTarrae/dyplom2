<?php
session_start();
require_once 'func/login.php';
require_once 'func/product.php';
include("include/config.php");

if (!$user->isLoggedIn()) {
    header("Location: index.php");
    exit();
} else {
    date_default_timezone_set('Europe/Kiev');
    $currentTime = date('d-m-Y h:i:s A', time());

    $product = new Product($con);

    if (isset($_POST['submit'])) {
        $category = $_POST['category'];
        $subcat = $_POST['subcategory'];
        $articul = $_POST['articul'];
        $productname = $_POST['productName'];
        $trade_mark = $_POST['trade_mark'];
        $units = $_POST['units'];
        $productdescription = $_POST['productDescription'];
        $productimage1 = ''; // Додайте логіку обробки завантаження зображення

        if ($product->insertProduct($category, $subcat, $articul, $productname, $trade_mark, $units, $productdescription, $productimage1)) {
            $_SESSION['msg'] = "Product Inserted Successfully !!";
        } else {
            $_SESSION['msg'] = "Error inserting product";
        }
    }

    $categories = $product->getAllCategories();
    $tradeMarks = $product->getAllTradeMarks();
    $units = $product->getAllUnits();
    ?>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>StockUp | Товари</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.2/font/bootstrap-icons.css">
        <link type="text/css" href="css/theme.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link type="text/css" href="images/icons/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
        <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
        <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
        <script>
            function getSubcat(val) {
                $.ajax({
                    type: "POST",
                    url: "get_subcat.php",
                    data:'cat_id='+val,
                    success: function(data){
                        $("#subcategory").html(data);
                    }
                });
            }
            function selectCountry(val) {
                $("#search-box").val(val);
                $("#suggesstion-box").hide();
            }
        </script>
    </head>
    <body>
    <div class="container-fluid">
        <div class="row flex-nowrap sticky-lg-top">
            <?php include('include/side.php'); ?>
            <div class="col py-3">
                <div class="module">
                    <div class="module-head">
                        <h3>Вставити товар</h3>
                    </div>
                    <div class="module-body">

                        <?php if(isset($_POST['submit'])) { ?>
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>Все добре!</strong>	<?php echo htmlentities($_SESSION['msg']);?><?php echo htmlentities($_SESSION['msg']="");?>
                            </div>
                        <?php } ?>

                        <?php if(isset($_GET['del'])) { ?>
                            <div class="alert alert-error">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>О ні!</strong> 	<?php echo htmlentities($_SESSION['delmsg']);?><?php echo htmlentities($_SESSION['delmsg']="");?>
                            </div>
                        <?php } ?>

                        <br />

                        <form class="form-horizontal row-fluid" name="insertproduct" method="post" enctype="multipart/form-data">

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Категорія</label>
                                <div class="controls">
                                    <select name="category" class="span8 tip" onChange="getSubcat(this.value);" required>
                                        <option value="">Обрати категорію</option>
                                        <?php while ($row = mysqli_fetch_array($categories)) { ?>
                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['categoryName']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Підкатегорія</label>
                                <div class="controls">
                                    <select name="subcategory" id="subcategory" class="span8 tip" required>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Артикул</label>
                                <div class="controls">
                                    <input type="text" name="articul" placeholder="Напишіть артикул" class="span8 tip" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Назва</label>
                                <div class="controls">
                                    <input type="text" name="productName" placeholder="Напишіть назву" class="span8 tip" required>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Торгова марка</label>
                                <div class="controls">
                                    <select name="trade_mark" class="span8 tip" required>
                                        <option value="">Обрати торгову марку</option>
                                        <?php while ($row = mysqli_fetch_array($tradeMarks)) { ?>
                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['trade_name']; ?>, <?php echo $row['country']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Одиниці виміру</label>
                                <div class="controls">
                                    <select name="units" class="span8 tip" required>
                                        <option value="">Обрати одиниці виміру</option>
                                        <?php while ($row = mysqli_fetch_array($units)) { ?>
                                            <option value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="basicinput">Опис</label>
                                <div class="controls">
                                    <textarea name="productDescription" placeholder="Напишіть опис" rows="6" class="span8 tip"></textarea>
                                </div>
                            </div>

                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" name="submit" class="btn btn-dark">Вставити</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div><!--/.content-->
        </div><!--/.span9-->

        <script src="scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="scripts/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
        <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="scripts/flot/jquery.flot.js" type="text/javascript"></script>
        <script src="scripts/datatables/jquery.dataTables.js"></script>
        <script>
            $(document).ready(function() {
                $('.datatable-1').dataTable();
                $('.dataTables_paginate').addClass("btn-group datatable-pagination");
                $('.dataTables_paginate > a').wrapInner('<span />');
                $('.dataTables_paginate > a:first-child').append('<i class="icon-chevron-left shaded"></i>');
                $('.dataTables_paginate > a:last-child').append('<i class="icon-chevron-right shaded"></i>');
            });
        </script>
    </body>
    </html>
<?php } ?>